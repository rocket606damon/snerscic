<?php get_template_part('templates/page', 'header'); ?>
<?php include (dirname(__FILE__)."/lib/ssep/getSearchResults.php"); ?>

<!-- Banner Starts Here -->
<?php use Roots\Sage\Titles; ?>
<section class="inner-banner" style="background-image: url(<?php bloginfo('url'); ?>/media/contact-banner.jpg); <?php if(!have_posts()){ echo 'display: none;'; } ?>">
  <div class="tbl">
    <div class="tbl-cell-bottom">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 col-sm-12">
            <h1><?= Titles\title(); ?></h1>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Banner Ends Here -->
<section class="mid-container" style="">
  <div class="container">
    <div class="row">
      <div class="contact-content">
		<?php echo doSearch($_REQUEST['s']); ?>
		</div>
    </div>
  </div>
</section>

<?php the_posts_navigation(); ?>
