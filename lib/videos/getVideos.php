<?php

define ('WP_LOADFILE_DIR', '../../../../../');
define( 'WP_USE_THEMES', false );
require_once( WP_LOADFILE_DIR . 'wp-load.php' );

$g_videos = array();
$filterColumns = array(
	'video_index',
);

function wp_getVideos(){
	global $g_videos;
	global $wpdb;
		$rows = $wpdb->get_results("SELECT post_id FROM " . $wpdb->postmeta . " " .
			"WHERE meta_key LIKE 'videos_%select'", OBJECT
	);
	foreach($rows as $r){
		$videos = $wpdb->get_results("SELECT * FROM ".$wpdb->postmeta." " .
			"WHERE post_id='" . $r->post_id . "' " .
			"AND meta_key LIKE 'videos_%'", OBJECT
		);
		foreach($videos as $video){
			index_video($video);
		}
	}
	return filter_videos();
}

function index_video($video){
	global $filterColumns;
	global $g_videos;

	$kp = explode('_', $video->meta_key);
	$index = $kp[1];
	$prefix = "videos_" . $index . "_";
	$short_key = str_replace($prefix, '', $video->meta_key);
	$g_videos[$index][$short_key] = $video->meta_value;
	$g_videos[$index]['video_index'] = $index +1;
}

function filter_videos(){
	global $g_videos;

	$finalVideos = array();
	foreach($g_videos as $id => $video){
			$finalVideos[] = $video;
	}
	return $finalVideos;
}

$return = wp_getVideos();
if (!is_array($return)){
	echo $return;
}else{
	$videos = $return;
	//print_r($videos);
	echo (json_encode($videos));
}
