jQuery.noConflict();

function snChangeBackgroundImage(page, subpage){
	var bgClass = page + '-' + subpage + "-content-panel".replace(' ', '-');
	console.log(bgClass);
	jQuery(bgLocation).css('background-image', '');
	jQuery(bgLocation).css('background-size', 'cover');
	jQuery(bgLocation).addClass(bgClass);
}

function snContainsMember( haystack, needle ) {
   for (var i in haystack) {
      if (haystack[i] == needle) return true;
   }
   return false;
}

function snContainsAttr(targetObjname, keyname) {
	var keynames = Object.keys(targetObjname);
	for (var i = 0; i < Object.keys(targetObjname).length; i++){
		if (Object.keys(targetObjname)[i] == keyname){
			return true;
		}
	}
	return false;
}
function snReplaceAll (targetString, search, replacement) {
    return targetString.replace(new RegExp(search, 'g'), replacement);
}

function snParseUrl (url) {
	var urlInfo = {};
	var urlPts = url.split("/");
	console.log("snParseUrl urlPts : " +  urlPts);
	urlInfo.page = urlPts[1];
	urlInfo.subpage = (urlPts.length > 2) ? urlPts[2] : '';
	console.log("snParseUrl urlInfo: " +  urlInfo.page + '/' + urlInfo.subpage);
    return urlInfo;
}

function resetActiveTopButton(index){
	jQuery(".li-clicker").removeClass('active');
	var activeTab = "#li-tabs-" + index;
	jQuery(activeTab).addClass('active');
	jQuery( "#tabs" ).tabs({ active: index - 1});
}

function setHeaderContent(index){
	
	var subpage = snTabs[index - 1];
	console.log(subpage);
	snChangeBackgroundImage(snUrlInfo.page, subpage);

	// reset active top button
	resetActiveTopButton(index);

}

var activeClasses = [
	'current-page-ancestor',
	'current-menu-ancestor',
	'current-menu-parent',
	'current-page-parent',
	'current_page_ancestor',
	'active'
];
var snActiveClassList = activeClasses.join(' ');
var snRelvantPages = [
	'about',
	'research',
	'sci-education',
	'resources'
];
var currentIndex = 0;
var snPagename = window.location.pathname;
console.log("snPagename:" + snPagename);
var snUrlInfo = snParseUrl(snPagename);
var snTabs = [];
var snTabsByOffset = {};
var bgLocation = '#tab-container section';

jQuery(document).ready(function() {

	// jQuery('.nav-tabs a').on('show.bs.tab', function(){
 //        alert('The new tab is about to be shown.');
 //    });
 //    jQuery('.nav-tabs a').on('shown.bs.tab', function(){
 //        alert('The new tab is now fully shown.');
 //    });

	console.log("checking page pagename:" + snUrlInfo.page);
	if (!snContainsMember(snRelvantPages, snUrlInfo.page)){
		console.log("pagename is NOT relevant:" + snUrlInfo.page);
	}else{
		console.log("pagename is relevant:" + snUrlInfo.page);
		var i = 1;
		var index = 1;
		jQuery('.tab-pane').each(function(){
			var panelName = jQuery(this).attr('id');
			console.log("panelName:" + panelName);
			snTabs.push(panelName);
			snTabsByOffset[panelName] = i++;
		});

		// list of background classes
		var lhash = window.location.hash;
		if (lhash != ''){
			lhash = lhash.replace('#', '');
			index = snContainsAttr(snTabsByOffset, lhash) ? snTabsByOffset[lhash] : 1;
		}else{
			console.log("looking up subpage:" + snUrlInfo.subpage);
			lhash = snUrlInfo.subpage;
			console.log("subpage:" + snUrlInfo.subpage);
			index = snContainsAttr(snTabsByOffset, lhash) ? snTabsByOffset[lhash] : 1;
			console.log("subpage index:" + index);
		}
		console.log("path:" + snUrlInfo.page+ " / " + snUrlInfo.subpage + " / " + index);
		jQuery( "#tabs" ).tabs({active: index-1});

		snChangeBackgroundImage(snUrlInfo.page, snUrlInfo.subpage);

		// jQuery('.tab-clicker').click(function(){
		// 	var id = jQuery(this).prop('id');
		// 	var index = id.replace('ui-id-');
		// 	var subpage = snTabs[index];
		// 	snChangeBackgroundImage(snUrlInfo.page, snTabs[index]);
		// });

		// handler for bottom menu tab clicks...
		jQuery(".li-clicker a.tab-clicker").on("click", function(event){
			var id = jQuery(this).attr('id');
			var index = id.substr(-1, 1);
			jQuery(bgLocation).removeClass();

			console.log(index);

			// set new header/tab
			setHeaderContent(index);
			event.preventDefault();
		});
	}
});