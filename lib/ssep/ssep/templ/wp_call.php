<?php

$search_domain = "snerscic.org";
define('DOMAIN', $search_domain);
define('SSEP_CACHE', $cache_dir. preg_replace(['/^www\./i', '/[^a-z0-9_]+/i'], ['', '_'], DOMAIN) .'/');

$searchTerm = "exercise";
$server_name = $_SERVER['SERVER_NAME'];
$results = doSearch($search_domain, $server_name, $searchTerms);

function doSearch($search_domain, $server_name, $searchTerms){
	include(dirname(__FILE__).'/php/config.php');
	include(dirname(__FILE__).'/'php/sitesearch.php');

	unset($_SESSION['src_dom_id']);

	if(!isset($_SESSION['src_dom_id']))
		$_SESSION['src_dom_id'] = getDomainId($obsql, DOMAIN);
	if(!isset($_SESSION['ssep_sets']))
		$_SESSION['ssep_sets'] = getSettings($obsql, $_SESSION['src_dom_id']);

	$sets = array_merge($sets0, $_SESSION['ssep_sets']);    // get settings from database
	$obsrc = new SiteSearch($obsql);
	$obsrc->use_ajax = 0;

	// template items
	$tpl = [
		'lang'=> $lang,
		'base'=> $server_name .
				rtrim(dirname(preg_replace('@/[^/]*$@i', '/', $_SERVER['REQUEST_URI']) .
				'index.php'), '/'). '/',
		'title'=> getTL('ssep_title'). DOMAIN,
		'description'=> getTL('ssep_description'),
		'keywords'=> '',
		'ssep_pg'=>$obsrc->ssep_pg,
		'nr_suggest'=>$sets['src_suggest'],
		'home_page'=> $server_name,
		'search'=>getTL('search'),
		'msg_ssep_inp'=>getTL('msg_ssep_inp'),
		'nav_menu'=> getTL('nav_menu'),
		'last_searches'=> getTL('last_searches'),
		'last_src'=> $obsrc->getListSrc($sets['last_src']),
		'top_searches'=> getTL('top_searches'),
		'top_src' => $obsrc->getListSrc($sets['top_src'], 'top'),
		'search_results'=> getTL('ssep_results'),
		'ssep_results_for'=> getTL('ssep_results_for'),
		'pgi_type'=> $sets['pgi_type']
	];

	// define properties
	$obsrc->score1 = $sets['score1'];
	$obsrc->score2 = $sets['score2'];
	$obsrc->rowsperpage = intval($sets['rowsperpage']);
	$obsrc->src_suggest = intval($sets['src_suggest']);

	// get array with the words added in text file (separated by comma), to be removed from search
	if(file_exists(dirname(__FILE__).'/php/stop_words.txt')) {
		$stop_words = trim(file_get_contents(dirname(__FILE__).'/php/stop_words.txt'));
		$obsrc->stop_words = array_map('trim', explode(',', $stop_words));
	}
	$tpl['search_results'] = $obsrc->getSearch($searchTerms);    // get search results

	// data to be added in $tl for template
	$tpl['title'] = $obsrc->pg_data['title'];
	$tpl['description'] = $obsrc->pg_data['description'];
	$tpl['keywords'] = $obsrc->pg_data['keywords'];

	return template(file_get_contents(SSEP_TEMPL .'search.htm'), $tpl);
}
