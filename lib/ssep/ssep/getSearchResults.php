<?php

function doSearch1($keywords){
	include('php/config.php');
	include('php/sitesearch.php');

	define('DOMAIN', $search_domain);
	define('SSEP_CACHE', $cache_dir. preg_replace(['/^www\./i', '/[^a-z0-9_]+/i'], ['', '_'], DOMAIN) .'/');    // folder with cache files of current domain

	unset($_SESSION['src_dom_id']);
	// gets $_SESSION['src_dom_id'] from database, and settings from session
	if(!isset($_SESSION['src_dom_id'])) $_SESSION['src_dom_id'] = getDomainId($obsql, DOMAIN);
	if(!isset($_SESSION['ssep_sets'])) $_SESSION['ssep_sets'] = getSettings($obsql, $_SESSION['src_dom_id']);
	$sets = array_merge($sets0, $_SESSION['ssep_sets']);    // get settings from database

	$obsrc = new SiteSearch($obsql);
	$obsrc->use_ajax = $sets['use_ajax'];
	$obsrc->score1 = $sets['score1'];
	$obsrc->score2 = $sets['score2'];
	$obsrc->rowsperpage = intval($sets['rowsperpage']);
	$obsrc->src_suggest = intval($sets['src_suggest']);

	if(file_exists('php/stop_words.txt')) {
		$stop_words = trim(file_get_contents('php/stop_words.txt'));
		$obsrc->stop_words = array_map('trim', explode(',', $stop_words));
	}
	$gs = $obsrc->getSearch($keywords);
	//var_dump($obsrc);
	//var_dump($gs);
	return (print_r($gs));
}

function doSearch($keywords){
	include('php/config.php');
	include('php/sitesearch.php');

	define('DOMAIN', $search_domain);
	define('SSEP_CACHE', $cache_dir. preg_replace(['/^www\./i', '/[^a-z0-9_]+/i'], ['', '_'], DOMAIN) .'/');    // folder with cache files of current domain

	unset($_SESSION['src_dom_id']);
	// gets $_SESSION['src_dom_id'] from database, and settings from session
	//	if(!isset($_SESSION['src_dom_id'])) $_SESSION['src_dom_id'] = getDomainId($obsql, DOMAIN);
	//	if(!isset($_SESSION['ssep_sets'])) $_SESSION['ssep_sets'] = getSettings($obsql, $_SESSION['src_dom_id']);
	//	$sets = array_merge($sets0, $_SESSION['ssep_sets']);    // get settings from database

	$obsrc = new SiteSearch($obsql);
	$obsrc->use_ajax = $sets['use_ajax'];
	$obsrc->score1 = $sets['score1'];
	$obsrc->score2 = $sets['score2'];
	$obsrc->rowsperpage = intval($sets['rowsperpage']);
	$obsrc->src_suggest = intval($sets['src_suggest']);

	if(file_exists('php/stop_words.txt')) {
		$stop_words = trim(file_get_contents('php/stop_words.txt'));
		$obsrc->stop_words = array_map('trim', explode(',', $stop_words));
	}
	$gs = $obsrc->getSearch($keywords);
        echo "search terms:$keywords";
	//var_dump($obsrc);
	//var_dump($gs);
	return (print_r($gs));
	// return ($obsrc->getSearch($keywords));
}

