<?php

$search_domain = "snerscic.org";
define('DOMAIN', $search_domain);
define('SSEP_CACHE', $cache_dir. preg_replace(['/^www\./i', '/[^a-z0-9_]+/i'], ['', '_'], DOMAIN) .'/');

$searchTerm = "exercise";
$server_name = $_SERVER['SERVER_NAME'];

$results = doSearch($search_domain, $server_name, $searchTerm);

function doSearch($search_domain, $server_name, $searchTerms){
	include(dirname(__FILE__)."/php/config.php");
	include(dirname(__FILE__)."/php/sitesearch.php");
	unset($_SESSION['src_dom_id']);

	if(!isset($_SESSION['src_dom_id']))
		$_SESSION['src_dom_id'] = getDomainId($obsql, DOMAIN);
	if(!isset($_SESSION['ssep_sets']))
		$_SESSION['ssep_sets'] = getSettings($obsql, $_SESSION['src_dom_id']);

	$sets = array_merge($sets0, $_SESSION['ssep_sets']);    // get settings from database
	$obsrc = new SiteSearch($obsql);
	$obsrc->use_ajax = 0;

	// define properties
	$obsrc->score1 = $sets['score1'];
	$obsrc->score2 = $sets['score2'];
	$obsrc->rowsperpage = intval($sets['rowsperpage']);
	$obsrc->src_suggest = intval($sets['src_suggest']);

	// get array with the words added in text file (separated by comma), to be removed from search
	if(file_exists(dirname(__FILE__).'/php/stop_words.txt')) {
		$stop_words = trim(file_get_contents(dirname(__FILE__).'/php/stop_words.txt'));
		$obsrc->stop_words = array_map('trim', explode(',', $stop_words));
	}

	var_dump($obsrc);

	$results = $obsrc->getSearch($searchTerms);    // get search results
	echo "<h3>" . print_r($results) . "</h3>\n";
	exit;
	return $results;
}
