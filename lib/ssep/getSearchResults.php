<?php

// SSEP - Site Search Engine PHP-Ajax - http://coursesweb.net/

//print_r(doSearch(isset($_REQUEST['sp']) ? $_REQUEST['sp'] : 'spinal'));
//echo doSearch(isset($_REQUEST['sp']) ? $_REQUEST['sp'] : 'spinal');

function doSearch($sp){
  global $search_domain;
  include('php/config.php');
  include('php/sitesearch.php');

  // set domain in which to search (set in config.php)
  if($search_domain == 'auto') $search_domain = isset($_SESSION['ssep_domain']) ? $_SESSION['ssep_domain'] : $_SERVER['SERVER_NAME'];

  define('DOMAIN', $search_domain); 
  define('SSEP_CACHE', $cache_dir. preg_replace(['/^www\./i', '/[^a-z0-9_]+/i'], ['', '_'], DOMAIN) .'/');    // folder with cache files of current domain
  unset($_SESSION['src_dom_id']);
 
  // gets $_SESSION['src_dom_id'] from database, and settings from session
  if(!isset($_SESSION['src_dom_id'])) $_SESSION['src_dom_id'] = getDomainId($obsql, DOMAIN);
  if(!isset($_SESSION['ssep_sets'])) $_SESSION['ssep_sets'] = getSettings($obsql, $_SESSION['src_dom_id']);
  $sets = array_merge($sets0, $_SESSION['ssep_sets']);    // get settings from database  
 
  $obsrc = new SiteSearch($obsql);
  $obsrc->use_ajax = $sets['use_ajax'];

  // define properties
  $obsrc->score1 = $sets['score1'];
  $obsrc->score2 = $sets['score2'];
  $obsrc->rowsperpage = intval($sets['rowsperpage']);
  $obsrc->src_suggest = intval($sets['src_suggest']);

  // get array with the words added in text file (separated by comma), to be removed from search
  if(file_exists('php/stop_words.txt')) {
    $stop_words = trim(file_get_contents('php/stop_words.txt'));
    $obsrc->stop_words = array_map('trim', explode(',', $stop_words));
  }
  return $obsrc->getSearch($sp);
}
