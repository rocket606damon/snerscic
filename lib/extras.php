<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Setup;

/**
 * Add <body> classes
 */
function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  // Add class if sidebar is active
  if (Setup\display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');

/**
* My Pagename
**/
function my_pagename(){
  $url =  trim($_SERVER['REQUEST_URI'], "/");
  if ($url == "about" || $url == "research" || $url == "sci-education" || $url == "resources"){
    return $url;
  }else{
    $pts = explode("/", $url);
    if ($pts[0] == "about" || $pts[0] == "research" || $pts[0] == "sci-education" || $pts[0] == "resources"){
      //echo "<!-- final url:". $pts[0] . " -->\n";
      return $pts[0];
    }
  }
  return $url ;
}

/**
 * Change default media folder
 */
update_option('uploads_use_yearmonth_folders', 0);
update_option('upload_path', 'media');

/**
 * Custom Slug
 */
function custom_slug($slug) {
  $slug = strtolower($slug);
  $slug = preg_replace("/[^a-z0-9_\s-]/", "", $slug);
  $slug = preg_replace("/[\s-]+/", " ", $slug);
  $slug = preg_replace("/[\s_]/", "-", $slug);
  return $slug;
}

/**
 * Addition of jQuery UI from WP Core
 */
function wpdocs_theme_name_scripts() {
  //Enqueue date picker UI from WP core:
  wp_enqueue_script("jquery-ui-core");
  wp_enqueue_script("jquery-ui-tabs");
}
add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\\wpdocs_theme_name_scripts');

/**
 * Gravity Forms - Hide Labels
 */
add_filter( 'gform_enable_field_label_visibility_settings',  '__return_true' );

/**
 * Gravity Forms - Init Scripts in the Footer
 */
add_filter( 'gform_init_scripts_footer',  '__return_true' );
add_filter( 'gform_cdata_open', __NAMESPACE__ . '\\wrap_gform_cdata_open', 1 );
function wrap_gform_cdata_open( $content = '' ) {
if ( ( defined('DOING_AJAX') && DOING_AJAX ) || isset( $_POST['gform_ajax'] ) ) {
return $content;
}
$content = 'document.addEventListener( "DOMContentLoaded", function() { ';
return $content;
}
add_filter( 'gform_cdata_close', __NAMESPACE__ . '\\wrap_gform_cdata_close', 99 );
function wrap_gform_cdata_close( $content = '' ) {
if ( ( defined('DOING_AJAX') && DOING_AJAX ) || isset( $_POST['gform_ajax'] ) ) {
return $content;
}
$content = ' }, false );';
return $content;
}

/**
 * Register ACF Options Page
 */ 
if( function_exists('acf_add_options_page') ) {
  
  acf_add_options_page(array(
    'page_title'  => 'General Settings',
    'menu_title'  => 'General Settings',
    'menu_slug'   => 'general-settings',
    'capability'  => 'edit_posts',
    'redirect'    => false
  ));
  
  acf_add_options_sub_page(array(
    'page_title'  => 'Header Settings',
    'menu_title'  => 'Header Settings',
    'parent_slug' => 'general-settings',
  ));
  
  acf_add_options_sub_page(array(
    'page_title'  => 'Footer Settings',
    'menu_title'  => 'Footer Settings',
    'parent_slug' => 'general-settings',
  ));

  acf_add_options_sub_page(array(
    'page_title'  => 'Contact Settings',
    'menu_title'  => 'Contact Settings',
    'parent_slug' => 'general-settings',
  ));
  
}

/**
 * Removal of Query Strings From Static Resources
 */
function _remove_script_version( $src ){ 
  $parts = explode( '?', $src ); 
  return $parts[0]; 
} 
add_filter( 'script_loader_src',  __NAMESPACE__ . '\\_remove_script_version', 15, 1 ); 
add_filter( 'style_loader_src',  __NAMESPACE__ . '\\_remove_script_version', 15, 1 );

/**
 * Special Loading for Videos
 */

function video_load(){ ?>
  <script type="text/javascript">

    jQuery.noConflict();

    var currentOffset = 0;
    var currentVideos = [];
    jQuery(document).ready(function(){
      /*jshint ignore:start */
      function nextSegment(segmentLength){
        if (currentOffset == 0 || currentOffset + segmentLength <= currentVideos.length){
          var start = currentOffset;
          var finish = currentOffset + segmentLength;
          if (finish > currentVideos.length) finish = currentVideos.length;
          for(var i = start; i < finish; i++){
            var video = currentVideos[i];
            jQuery('.video-'+video.video_index).slideDown();
            currentOffset++;
          }
        }
      }
      /*jshint ignore:end */

      function videoSelect(){
        var base_url = "<?php echo get_stylesheet_directory_uri() ; ?>";
        var url = base_url + "/lib/getVideos.php";
      jQuery.ajax({
        url: url,
        dataType:'json',
        method:'GET',
      }).done(function(data) {
          currentOffset = 0;
          currentVideos = data.videos;
          jQuery('.video').hide();
          nextSegment(2);
        });
      }

      videoSelect();

      jQuery('.lazy').lazy({
        youtubeLoader: function(element) {
          var frame = jQuery('<iframe />')

          frame.addClass('embed-responsive-item')
          frame.attr('src', element.data("video"));

          element.append(frame).load();
        }
      });

      jQuery("#loadMoreEducation1").on("click", function(e){
        e.preventDefault();
        nextSegment(4);
        jQuery('html,body').animate({
          scrollTop: jQuery(this).offset().top
        }, 1500);
      });
    });  
  </script>
<?php 
}
if(my_pagename() == 'sci-education'){
  add_action('wp_footer',  __NAMESPACE__ . '\\video_load', 20);
}

/**
 * Special Loading for Presentations
 */

function presentations_animation() {
    ?><script type="text/javascript">

    jQuery.noConflict();

    var vl = {
    urlSuffix: "/lib/getPresentations.php",
    urlBase: "<?php echo get_stylesheet_directory_uri() ; ?>",
    //segmentLength: 2,
    //preloadLength: 4,
    //loadTag: 'src',
    //preLoadTag: 'data-src',
    downloading: false,
    category: 1,
    offset: 0,
    //videoCount: 0,
    list: {},
    template:
      '                      <div class="col-sm-12 video video-{{index}}">' + "\n" +
      '                        <div class="col-sm-5">' + "\n" +
      '                          <div class="embed-responsive embed-responsive-16by9">' + "\n" +
      '                            <iframe class="embed-responsive-item" {{pre_tag}}="" {{post_tag}}="{{embedVideoUrl}}" allowfullscreen></iframe>' + "\n" +
      '                          </div>' + "\n" +
      '                        </div>' + "\n" +
      '                        <div class="col-sm-7">' + "\n" +
      '                          <h3><a href="{{bottomVideoUrl}}" target="_blank">{{title}}</a></h3>' + "\n" +
      '                          <p class="video-description">{{description}}</p>' + "\n" +
      '                        </div>' + "\n" +
      '                      </div>' + "\n",
    // urlFormat: { vimeo:{
    //  prefix:'//player.vimeo.com/video/', suffix: '?title=0&amp;byline=0&amp;portrait=0',
    //    bottomPrefix:'https://vimeo.com/'},
    //  youtube:{prefix:'//www.youtube.com/embed/', suffix:'',
    //    bottomPrefix:'//www.youtube.com/watch?v='},
    // },
    // removals:["http:", "https:", "www.", "vimeo.com", "//", "/",
    //    "watch?v=", "youtube.com", "youtu.be", "embed"],
    tValues: {
      'Media':1,
      'Presentations and Courses':2,
      'Posters':3,
      'Awards':4,
      'All':5
    },
    classValues: {
      1:'media',
      2:'pres-course',
      3:'poster',
      4:'award',
      5:'all'
    }

  };

    jQuery(document).ready(function(){

    // trigger download of presentations...
    //presToggle(1);

    jQuery("#pres-dropdown-menu li a").click(function(e){
      e.preventDefault();
      var tValues = vl.tValues;
      var selText = jQuery(this).text();
          jQuery(this).parents('.btn-group').find('.dropdown-toggle').html(selText+' <span class="caret"></span>').attr("value", selText);
      var toggleValue = jQuery(this).parents('.btn-group').find('.dropdown-toggle').html();
      toggleValue = toggleValue.replace(' <span class="caret"></span>', '');
      var toggleid = vl.tValues[toggleValue];
      presToggle(toggleid);
    });

    // function preloadPresentations(start){
    //   var finish = (start + vl.preloadLength > vl.videoCount) ?
    //       vl.videoCount :
    //       vl.offset + vl.preloadLength;
    //   for(var index = start; index <= finish; index++){
    //     var displayIndex = index + 1;
    //     var video = vl.list[vl.category[index]];

    //     var sel = '.video-'+displayIndex+' iframe';
    //     var src = jQuery(sel).attr('src');
    //     if (typeof(src) === 'undefined'){
    //       var src = "";
    //     }
    //     if (src.length == 0){
    //       jQuery(sel).attr('src', jQuery(sel).data('src'));
    //     }
    //   }
    // }

    // function renderPresentations(){
    //   var html = '';
    //   jQuery.each(vl.list[vl.category], function(index, presentation){
    //     var vars = {};
    //     var videoUrl = '';

    //     var yttype = typeof(video.youtube);
    //     var vtype = typeof(video.vimeo);

    //     if (video.video_type == 'youtube' &&  yttype == 'string'){
    //       videoUrl = video.youtube;
    //     }else if (video.video_type == 'vimeo' && vtype == 'string'){
    //       videoUrl = video.vimeo;
    //     }else if (typeof(video.video_url) !== 'undefined'){
    //       videoUrl = video.video_url;
    //     }

    //     if (videoUrl){
    //       jQuery.each(vl.removals, function(i, str){
    //         videoUrl = videoUrl.replace(str, '');
    //       });
    //       var videoId = videoUrl;
    //       vars.index = index + 1;
    //       vars.embedVideoUrl = vl.urlFormat[video.video_type].prefix +
    //           videoId + vl.urlFormat[video.video_type].suffix;
    //       vars.bottomVideoUrl = vl.urlFormat[video.video_type].bottomPrefix + videoId;
    //       vars.description = video.video_description;
    //       vars.title = video.video_title;

    //       vars.post_tag = (vars.index <= vl.preloadLength) ? "src" : "data-src";
    //       vars.pre_tag = (vars.index <= vl.preloadLength) ? "data-src" : "src";

    //       var videoItem =  vl.template;
    //       jQuery.each(vars, function(name, value){
    //         videoItem = videoItem.replace('{{'+name+'}}', value);
    //       });
    //       html = html + videoItem;
    //     }
    //   });
    //   jQuery('.video-list').html(html);
    // }

    function presToggle(toggleid){
      jQuery('.presentation').hide();
      if (vl.downloading){
        vl.category = toggleid;
        return;
      }

      vl.category = toggleid;
      vl.category == '5' ? jQuery('.presentation').show('fast') :loadPresentations();

    }

    function loadPresentations(){
      vl.downloading = true;
      var url = vl.urlBase + vl.urlSuffix;
      jQuery.ajax({
        url: url,
        dataType:'json',
        method:'GET'
      }).done(function(presentations) {
        vl.list = presentations;
        vl.downloading = false;
        //console.log(vl.list[vl.category]);
        var classShown = vl.classValues[vl.category];
        //console.log(classShown);
        //vl.presentationCount = vl.list[vl.category].length;
        jQuery("." + classShown).show('fast');
      })
      .fail(function(jqXHR, textStatus, errorThrown){
        console.log("status: " + textStatus + ";" + errorThrown);
      });
    }

    });
    </script>
  <?php
}
// Do not load unless this is the resources page
if(my_pagename() == 'research'){
  add_action('wp_footer',  __NAMESPACE__ . '\\presentations_animation', 20);
}

/**
 * Special Loading for Resource List
 */

function resource_load(){ ?>
  <script type="text/javascript">

    jQuery.noConflict();

    var currentOffset = 0;
    var currentResources = [];
    jQuery(document).ready(function(){

      function nextSegment(segmentLength){
        if (currentOffset == 0 || currentOffset + segmentLength <= currentResources.length){
          var start = currentOffset;
          var finish = currentOffset + segmentLength;
          if (finish > currentResources.length) finish = currentResources.length;
          for(var i = start; i < finish; i++){
            var resource = currentResources[i];
            jQuery('.resource-'+resource.resource_index).slideDown();
            currentOffset++;
          }
        }
      }

      function resourceSelect(){
        var base_url = "<?php echo get_stylesheet_directory_uri() ; ?>";
        var url = base_url + "/lib/getResources.php";
      jQuery.ajax({
        url: url,
        dataType:'json',
        method:'GET',
      }).done(function(data) {
          currentOffset = 0;
          currentResources = data.resources;
          jQuery('.item').hide();
          nextSegment(3);
        });
      }

      resourceSelect();

      jQuery("#loadMoreResources").on("click", function(e){
        e.preventDefault();
        nextSegment(3);
        jQuery('html,body').animate({
          scrollTop: jQuery(this).offset().top
        }, 1500);
      });
    });  
  </script>
<?php 
}
if(my_pagename() == 'resources'){
  add_action('wp_footer',  __NAMESPACE__ . '\\resource_load', 20);
}