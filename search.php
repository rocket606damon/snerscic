<?php get_template_part('templates/page', 'header'); ?>

<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <div class="search-form sorry-result">
  	<?php get_search_form(); ?>
	</div>
<?php endif; ?>

<!-- Banner Starts Here -->
<?php use Roots\Sage\Titles; ?>
<section class="inner-banner" style="background-image: url(<?php bloginfo('url'); ?>/media/contact-banner.jpg); <?php if(!have_posts()){ echo 'display: none;'; } ?>">
  <div class="tbl">
    <div class="tbl-cell-bottom">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 col-sm-12">
            <h1><?= Titles\title(); ?></h1>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Banner Ends Here -->
<section style="<?php if(!have_posts()){ echo 'display: none;'; } ?>">
  <div class="container">
    <div class="row">
      <div class="contact-content">
				<?php while (have_posts()) : the_post(); ?>
				  <?php get_template_part('templates/content', 'search'); ?>
				<?php endwhile; ?>
			</div>
    </div>
  </div>
</section>

<?php the_posts_navigation(); ?>
