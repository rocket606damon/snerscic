/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages

        function getSize() {
          size = $("p").css( "font-size" );
          size = $("h1").css( "font-size" );
          size = $("a").css( "font-size" );
          size = $("span").css( "font-size" );
          size = parseInt(size, 10);
          $( "#font-size" ).text(  size  );
        }

        $(document).ready(function(){
          $('.banner-main img').each(function() {
            $(this).addClass('bannerImage');
            var imgSrc = $(this).attr('src');
            imgSrc = "url(" + imgSrc + ")";
            $(this).parent('.banner-main').css("background-image", imgSrc);
          });

          $( "#increase-font" ).on( "click", function() {
            // parse font size, if less than 50 increase font size
            if ((size + 2) <= 50) {
              $("p").css( "font-size", "+=2" );
              $("h1").css( "font-size", "+=2" );
              $("a").css( "font-size", "+=2" );
              $("span").css( "font-size", "+=2" );
              $( "#font-size" ).text(  size += 2 );
            }
          });

          $( "#decrease-font" ).on( "click", function() {
            if ((size - 2) >= 12) {
              $("p").css( "font-size", "-=2" );
              $("h1").css( "font-size", "-=2" );
              $("a").css( "font-size", "-=2" );
              $("span").css( "font-size", "-=2" );
              $( "#font-size" ).text(  size -= 2  );
            }
          });

          // $('#tabs').tabs({
          //   activate: function(event, ui){
          //     var active = $('#tabs').tabs('option', 'active', currentIndex);
          //   }
          // });

          $(window).scroll(function(){
            if ($(this).scrollTop() > 50) {
              $('#backToTop').fadeIn('slow');
            } else {
              $('#backToTop').fadeOut('slow');
            }
          });

          $('#backToTop').click(function(){
            $("html,body").animate({ scrollTop: 0 }, 700);
            return false;
          });

          getSize();

          $(".lazy").lazy();

        });
      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
        $(document).ready(function(){
          $('.banner-slider').slick({
            arrows: false,
            dots: true,
            autoplay: true,
            speed: 4500,
            autoplaySpeed: 7000,
            pauseOnFocus: false,
            pauseOnHover: false
          });
        });
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about': {
      init: function() {
        // JavaScript to be fired on the about us page
        function equalheight() {
          var $height = 0;
          $(".about-boxes").each(function() {
            $(this).css("height", "auto");
            if (($(this).outerHeight()) > $height) {
              $height = $(this).outerHeight();
            }
          });
          $(".about-boxes").each(function() {
            $(this).css("height", $height);
          });
        }

        $(document).ready(function(){
          equalheight();
          $(window).resize(function () {
            equalheight();
          });
        });
      }
    },
    // SCI Education Page
    'sci_education': {
      init: function() {
        // JavaScript to be fired on the about us page
        
      }
    },
    // Research page
    'research': {
      init: function() {
        // JavaScript to be fired on the about us page
    
      }
    },
    // Resources Page
    'resources': {
      init: function() {
        // JavaScript to be fired on the about us page
        $(document).ready(function(){
          $("div[id^='largeModal']").each(function(){
            var currentModal = $(this);
            //click next
            currentModal.find('.btn-next').click(function(){
              currentModal.modal('hide');
              currentModal.closest("div[id^='largeModal']").nextAll("div[id^='largeModal']").first().modal('show');
            });
            //click prev
            currentModal.find('.btn-prev').click(function(){
              currentModal.modal('hide');
              currentModal.closest("div[id^='largeModal']").prevAll("div[id^='largeModal']").first().modal('show');
            });
          });

          $('#list').click(function(event){
            event.preventDefault();
            $('#products .item').removeClass('grid-group-item');
            $('.links-number').removeClass('hide');
            $('.caption-words').removeClass('caption-grid');
            $('#products .item').addClass('list-group-item');
            $('.list-group-image').addClass('hide');
            $('.caption-words').addClass('caption');
            $('.loader').addClass('hide');
          });

          $('#grid').click(function(event){
            event.preventDefault();
            $('.caption-words').removeClass('caption');
            $('#products .item').removeClass('list-group-item');
            $('.list-group-image').removeClass('hide');
            $('.links-number').removeClass('hide');
            $('.loader').removeClass('hide');
            $('#products .item').addClass('grid-group-item');
            $('.links-number').addClass('hide');
            $('.caption-words').addClass('caption-grid');    
          });

          $("#products .item").addClass('grid-group-item');
          $(".grid-group-item .thumbnail").slice(0,6).show();
          $("#loadMoreResources").on('click', function(e){
            e.preventDefault();
            $(".grid-group-item .thumbnail:hidden").slice(0,6).slideDown();
            if($(".grid-group-item .thumbnail:hidden").length === 0){
              $("#load").fadeOut('slow');
            }
            $('html,body').animate({
              scrollTop: $(this).offset().top
            }, 1500);
          });
        });  
      }  
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
