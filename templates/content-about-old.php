<!-- Banner Starts Here -->
<section class="inner-banner about-banner" style="background-image: url('<?php bloginfo('url'); ?>/media/about-banner-1.jpg')">
  <div class="tbl">
    <div class="tbl-cell-bottom">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 col-sm-12">
            <h1>About</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Banner Ends Here -->

<!-- Mid Content Starts Here -->
<section class="mid-content">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
        <h2><em>The Spaulding New England Regional Spinal Cord Injury Center (SNERSCIC)</em> is committed to promoting quality of life for individuals with spinal cord injury throughout their lives, through exemplary standards of care and rehabilitation advanced through research, education, advocacy, and ongoing support.</h2>
      </div>
    </div>
  </div>
</section>
<!-- Mid Content Starts Here -->

<!--Start of Tab-->
<div id="tabs">
<!-- Page Links Starts Here -->
<section class="page-links about-nav" id="about-nav">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
        <ul class="links-listing">

          <li class="about-tabs about-tabs-1 li-clicker">
            <a href="#tabs-1" class="tab-clicker" title="About">
              <div class="tbl">
                <div class="tbl-cell">
                  <span>About</span>
                </div>
              </div>
            </a>
          </li>

          <li class="about-tabs about-tabs-2 li-clicker">
            <a href="#tabs-2" class="tab-clicker" title="Our Team">
              <div class="tbl">
                <div class="tbl-cell">
                  <span>Our Team</span>
                </div>
              </div>
            </a>
          </li>

          <li class="about-tabs about-tabs-3 li-clicker">
            <a href="#tabs-3" class="tab-clicker" title="Spinal Cord Injury Model Systems Program">
              <div class="tbl">
                <div class="tbl-cell">
                  <span>Spinal Cord Injury Model Systems Program</span>
                </div>
              </div>
            </a>
          </li>

          <li class="about-tabs about-tabs-4 li-clicker">
            <a href="#tabs-4" class="tab-clicker" title="SNERSCIC Network Sites">
              <div class="tbl">
                <div class="tbl-cell">
                  <span>SNERSCIC Network Sites</span>
                </div>
              </div>
            </a>
          </li>

          <li class="about-tabs about-tabs-5 li-clicker">
            <a href="#tabs-5" class="tab-clicker" title="Snerscic Community Advisory Boards">
              <div class="tbl">
                <div class="tbl-cell">
                  <span>Snerscic Community Advisory Boards</span>
                </div>
              </div>
            </a>
          </li>

        </ul>
      </div>
    </div>
  </div>
</section>
<!-- Page Links Ends Here -->

<!-- Mid Container Starts Here -->
<div id="tab-container" class="tab-container about-page">
  <section>
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="tab-content">

            <!--Start Tab 1-->
            <div id="tabs-1">
              <div class="tab-pane" id="about">
                <div class="about-snerscic-content">
                  <div class="row">
                    <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1 text-center">

                      <img src="<?php bloginfo('url'); ?>/media/about-snerscic-img.jpg" alt="" />

                      <div class="about-inside">
                        <p>We are pleased to announce the formation of the <strong>The Spaulding New England Regional Spinal Cord Injury Center (SNERSCIC)</strong>
                        , a merger between the Spaulding-Harvard Spinal Cord Injury Model System and the New England Regional Spinal Cord Injury Center Model System.</p>
                        <p>SNERSCIC has a long and distinguished history in working with the SCI community across New England.</p>
                        <p><strong>Our mission</strong> is to promote quality of life for individuals with SCI throughout their lives, through exemplary standards of care and rehabilitation advanced through research, education, advocacy, and ongoing support.</p>
                        <p><span class="up_bold">Our purpose is to:</span></p>
                        <ul>
                          <li>Research many different aspects of SCI through onsite and collaborative studies and data collection.</li>
                          <li>Inform people with SCI, their families, the general public and medical professionals about SCI - including rehabilitation, the latest resources, adaptive technology, and research.</li>
                          <li>Provide services for people with SCI during their initial hospitalization and after discharge from a SNERSCIC site.</li>
                          <li>Advocate and support individuals with disabilities to live as fully as possible as active members of their communities</li>
                          <li>Provide individuals with SCI the opportunity to be involved in research, whenever appropriate.</li>
                        </ul>
                        <p><span class="up_bold">In Massachusetts:</span></p>
                        <ul>
                          <li><a href="http://www.massgeneral.org/" target="_blank">Massachusetts General Hospital (MGH)</a></li>
                          <li><a href="http://www.brighamandwomens.org/" target="_blank">Brigham and Women’s Hospital</a></li>
                          <li><a href="http://spauldingrehab.org/" target="_blank">Spaulding Rehabilitation Network</a></li>
                          <li><a href="https://hms.harvard.edu/" target="_blank">Harvard Medical School</a></li>
                          <li><a href="https://www.mghihp.edu/" target="_blank">MGH Institute for Health Professions (MGH IHP)</a></li>
                        </ul>
                        <p><span class="up_bold">In Connecticut:</span></p>
                        <ul>
                          <li><a href="https://www.gaylord.org/" target="_blank">Gaylord Rehabilitation Hospital</a></li>
                          <li><a href="https://www.ynhh.org/" target="_blank">Yale-New Haven Hospital</a></li>
                        </ul>
                        <p>SNERSCIC is funded by grant # 90S1503 from the National Institute on Disability, Independent Living, and Rehabilitation Research as one of 14 SCI Model Systems in the United States, working together to optimize clinical care, research, education, and services related to the impact of SCI on individuals and their communities.</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!--End of Tab 1-->

            <!--Start of Tab 2-->
            <div id="tabs-2">
              <div class="tab-pane" id="our-team">
                <div class="about-snerscic-content">
                  <div class="row">

                  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="about-boxes">
                      <img src="<?php bloginfo('url'); ?>/media/zafonte.png" alt="" />
                      <h3>Dr. Ross Zafonte, DO</h3>
                      <p data-mh="team">Administrative Co-Director</p>
                      <a href="#" data-toggle="modal" data-target="#largeModal1" title="View Bio" class="btn blue-btn">View Bio</a>
                    </div>
                  </div>
                  <!--Modal 1-->
                  <div class="modal fade" id="largeModal1" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                          <h4 id="largeModal-t2-1" class="modal-title" >Dr. Ross Zafonte, DO - Administrative Co-Director</h4>
                        </div>
                        <div class="modal-body">
                          <div class="thumbnail left-align">
                            <img src="<?php bloginfo('url'); ?>/media/zafonte.png" alt="" />
                          </div>
                          <p>Dr. Ross D. Zafonte is Earle P. and Ida S. Charlton Professor and Chairman of the Department of Physical Medicine and Rehabilitation at Harvard Medical School. He also serves as chief of Physical Medicine and Rehabilitation at Massachusetts General Hospital as well as Vice President Medical Affairs Research and Education at Spaulding Rehabilitation Hospital.Dr. Zafonte also previously served on the Board of Governors of the American Congress of Rehabilitation Medicine. He is the author of more than 300 peer review journal articles, abstracts and book chapters. In 2012, Dr. Zafonte received the William Caveness award. In May 2013, he received the Joel DeLisa Prize from the Kessler Foundation. In 2014, Dr. Zafonte received the Robert L Moody Prize the University of Texas, Galveston. </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="about-boxes">
                        <img src="<?php bloginfo('url'); ?>/media/taylor.png" alt="" />
                      <h3>Dr. J. Andrew Taylor, PhD</h3>
                      <p data-mh="team">Co-Director</p>
                      <a href="#" data-toggle="modal" data-target="#largeModal2" title="View Bio" class="btn blue-btn">View Bio</a>
                    </div>
                  </div>
                  <!--Modal 2-->
                  <div class="modal fade" id="largeModal2" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                          <h4 class="modal-title" >Dr. J. Andrew Taylor, PhD - Co-Director</h4>
                        </div>
                        <div class="modal-body">
                          <div class="thumbnail left-align">
                            <img src="<?php bloginfo('url'); ?>/media/taylor.png" alt="" />
                          </div>
                          <p>Dr. Taylor is Associate Professor at Harvard Medical School and Associate Chair for Research in the Department of PM&amp;R. He has pursued research into integrative human physiology for 30 years and has a strong understanding of both cardiovascular and exercise physiologies. He is currently funded by the NIH to study the cardiovascular benefits of a hybrid functional electrical stimulation exercise for those with SCI. He created the ExPD program that provides access to physical activity for a broad range of those with neuromuscular disabilities who otherwise have limited access to appropriate exercise. He recently edited a book for the American Physiological Society series on physiology in health and disease entitled, The Physiology of Exercise in Spinal Cord Injury.</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="about-boxes">
                      <img src="<?php bloginfo('url'); ?>/media/jette.png" alt="" />
                      <h3>Dr. Alan Jette, PhD</h3>
                      <p data-mh="team">Director of Research</p>
                      <a href="#" data-toggle="modal" data-target="#largeModal3" title="View Bio" class="btn blue-btn">View Bio</a>
                    </div>
                  </div>
                  <!--Modal 3-->
                  <div class="modal fade" id="largeModal3" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                          <h4 class="modal-title" >Dr. Alan Jette, PhD - Director of Research</h4>
                        </div>
                        <div class="modal-body">
                          <div class="thumbnail left-align">
                            <img src="<?php bloginfo('url'); ?>/media/jette.png" alt="" />
                          </div>
                          <p>Dr. Jette is a physical therapist and an internationally recognized expert in the measurement of function and disability. He has developed numerous instruments that assess function and disability and has published numerous articles on these topics in the rehabilitation, geriatrics, and public health literature. He was elected to the Institute of Medicine in October 2013. Over the past 30 years, Dr. Jette has received a total of 54 grants and fellowships.From 2005-2007, Dr. Jette chaired the Institute of Medicine's (IOM) Project, The Future of Disability in America. Professional recognition of Dr. Jette includes his being named an American Physical Therapy Association (APTA) Catherine Worthingham Fellow in 2005, and the 2012 Mary McMillan Lecturer; and the 2010 American Academy of Physical Medicine and Rehabilitation Recognition Award for Distinguished Public Service, as well as the American Congress of Rehabilitation Medicine’s  2016 John Stanley Coulter Award, for his pioneering contributions to the field of disability rehabilitation research and to people with disabilities.</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="about-boxes">
                        <img src="<?php bloginfo('url'); ?>/media/houlihan.png" alt="" />
                      <h3>Bethlyn Houlihan, MSW, MPH</h3>
                      <p data-mh="team">Co-Director of Dissemination/Knowledge Translation</p>
                      <a href="#" data-toggle="modal" data-target="#largeModal4" title="View Bio" class="btn blue-btn">View Bio</a>
                    </div>
                  </div>
                  <!--Modal 4-->
                  <div class="modal fade" id="largeModal4" tabindex="-1" role="dialog" aria-hidden="true">
                      <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                              <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                      <h4 class="modal-title" >Bethlyn Houlihan, MSW, MPH - Co-Director of Dissemination/Knowledge Translation</h4>
                              </div>
                              <div class="modal-body">
                                <div class="thumbnail left-align">
                                  <img src="<?php bloginfo('url'); ?>/media/houlihan.png" alt="" />
                                </div>
                                <p>Ms. Houlihan has been on the SCIMS research team for over 15 years. Her main research interests focus on developing and testing consumer-led community interventions to promote health-related quality of life through better self-management and access to resources.  Ms. Houlihan acted as Project Director on My Care My Call, a peer-led phone coaching intervention that improved health self-management, participation, and access to resources for people with SCI in a randomized control trial.  She was also Content Developer/Project Manager for the SCI Virtual Coach to Promote Self-Care in Pressure Ulcer Prevention.  Ms. Houlihan has served as a board member for the Greater Boston Chapter of United Spinal since 2004. She isco-chair for the Health &amp; Disability Partnership, which brings together state agencies and community non-profits to address the healthcare access needs of people with disabilities.</p>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="about-boxes">
                        <img src="<?php bloginfo('url'); ?>/media/blauwet.png" alt="" />
                      <h3>Dr. Cheri Blauwet, MD</h3>
                      <p data-mh="team">Co-Director of Dissemination/Knowledge Translation</p>
                      <a href="#" data-toggle="modal" data-target="#largeModal5" title="View Bio" class="btn blue-btn">View Bio</a>
                    </div>
                  </div>
                  <!--Modal 5-->
                  <div class="modal fade" id="largeModal5" tabindex="-1" role="dialog" aria-hidden="true">
                      <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                              <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                      <h4 class="modal-title" >Dr. Cheri Blauwet, MD - Co-Director of Dissemination/Knowledge Translation</h4>
                              </div>
                              <div class="modal-body">
                                <div class="thumbnail left-align">
                                  <img src="<?php bloginfo('url'); ?>/media/blauwet.png" alt="" />
                                </div>
                                <p>Dr. Blauwet is an Instructor in PM&amp;R at Harvard Medical School, and also serves as the Disability Access and Awareness Director for Spaulding Rehabilitation Network. Dr. Blauwet is a former Paralympic athlete in wheelchair racing, competing for the US team in three Paralympic Games and bringing home a total of seven medals. Dr. Blauwet now serves as Chair of the International Paralympic Committee’s Medical Commission and on the Board of Directors for the US Anti-Doping Agency. Dr. Blauwet currently oversees the Kelley Adaptive Sports Research Institute. She is a global advocate for sport and physical activity to promote healthy lifestyles for all those with disabilities – exemplified by speaking on the floor of the UN for the UN International Day of Sport for Development and Peace. She has given many talks throughout the world and was keynote speaker at the Boston celebration of the 25th Anniversary of the Americans with Disabilities Act.</p>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="about-boxes">
                        <img src="<?php bloginfo('url'); ?>/media/connor.png" alt="" />
                      <h3>Dr. Kevin O'Connor, MD</h3>
                      <p data-mh="team">Director of Clinical Care,<br> Spaulding Rehabilitation Network</p>
                      <a href="#" data-toggle="modal" data-target="#largeModal6" title="View Bio" class="btn blue-btn">View Bio</a>
                    </div>
                  </div>
                  <!--Modal 6-->
                  <div class="modal fade" id="largeModal6" tabindex="-1" role="dialog" aria-hidden="true">
                      <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                              <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                      <h4 class="modal-title" >Dr. Kevin O'Connor, MD - Director of Clinical Care, Spaulding Rehabilitation Network</h4>
                              </div>
                              <div class="modal-body">
                                <div class="thumbnail left-align">
                                  <img src="<?php bloginfo('url'); ?>/media/connor.png" alt="" />
                                </div>
                                <p>Dr. O’Connor specializes in pain management in SCI and has extensive clinical expertise in SCI rehabilitation. He is board certified in PM&R, as well as subspecialty certified in Spinal Cord Medicine, a clinical associate at Massachusetts General Hospital, and an Assistant Professor of PM&amp;R at Harvard Medical School.  Since 2002, he has been the Medical Director of the SCI program at Spaulding Rehabilitation Hospital and has helped advance the program through clinical care, advocacy, and education.  Dr. O’Connor has been nominated for the MGH McGovern award for most outstanding physician each of the last 2 years. He directs a team of 6 board certified SCI medicine physicians and co-directs our SCI fellowship in collaboration with the VA Boston. </p>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="about-boxes">
                        <img src="<?php bloginfo('url'); ?>/media/rosenblum.png" alt="" />
                      <h3>Dr. David Rosenblum, MD</h3>
                      <p data-mh="team">Director of Clinical Care, Gaylord Specialty Healthcare</p>
                      <a href="#" data-toggle="modal" data-target="#largeModal7" title="View Bio" class="btn blue-btn">View Bio</a>
                    </div>
                  </div>
                  <!--Modal 7-->
                  <div class="modal fade" id="largeModal7" tabindex="-1" role="dialog" aria-hidden="true">
                      <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                              <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                      <h4 class="modal-title" >Dr. David Rosenblum, MD - Director of Clinical Care, Gaylord Specialty Healthcare</h4>
                              </div>
                              <div class="modal-body">
                                <div class="thumbnail left-align">
                                  <img src="<?php bloginfo('url'); ?>/media/rosenblum.png"  alt="" />
                                </div>
                                <p>Dr. Rosenblum acts as Medical Director of the Gaylord Hospital SCI Program in Wallingford, CT, and is the Medical Director of Outpatient Services at Gaylord. He is an Associate Clinical Professor of Orthopedics and Rehabilitation, Yale University School of Medicine, and is board certified in PM&R and the subspecialties of Spinal Cord Medicine and Brain Injury Medicine. Dr. Rosenblum also provides PM&R consultations to patients at Yale New Haven Hospital. In addition to his administrative and research activities, Dr. Rosenblum has an active inpatient and outpatient SCI practice. Under Dr. Rosenblum’s leadership, Gaylord’s SCI program provides excellent acute and life-long care. He has been named a Top Doc by Connecticut magazine 9 consecutive times and was named Health Care Hero of the Year, Physician in 2016 by the Hartford Business Journal, and Physician of the Year, and was awarded the Healthcare Heroes Award in 2014 by Business New Haven. Dr. Rosenblum has published and presented extensively on topics related to SCI and Multiple Sclerosis.</p>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="about-boxes">
                        <img src="<?php bloginfo('url'); ?>/media/estrada.png" alt="" />
                      <h3>Dave Estrada, JD</h3>
                      <p data-mh="team">Program Manager</p>
                      <a href="#" data-toggle="modal" data-target="#largeModal8" title="View Bio" class="btn blue-btn">View Bio</a>
                    </div>
                  </div>
                  <!--Modal 8-->
                  <div class="modal fade" id="largeModal8" tabindex="-1" role="dialog" aria-hidden="true">
                      <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                              <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                      <h4 class="modal-title" >Dave Estrada, JD - Program Manager</h4>
                              </div>
                              <div class="modal-body">
                                <div class="thumbnail left-align">
                                  <img src="<?php bloginfo('url'); ?>/media/estrada.png" alt="" />
                                </div>
                                <p>Mr. Estrada has been a part of various SCIMS research projects over his 20-plus years of living with SCI. His main interests focus on consumer-led community advocacy both locally and nationally to promote health-related quality of life through better self-management and access to resources.  Mr. Estrada is a trained SCI peer mentor, was the Executive Director of the Greater Boston Chapter of the National Spinal Cord Injury Association for 8 years, and was the Athletes with Disabilities Coordinator for the Boston Marathon from 2008 to 2016.  He also works as the Research and Development Coordinator in the Exercise for Persons with Disabilities Program (ExPD) at Spaulding Rehabilitation Hospital.</p>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="about-boxes">
                        <img src="<?php bloginfo('url'); ?>/media/pernigotti.png" alt="" />
                      <h3>Diana Pernigotti, MSG</h3>
                      <p data-mh="team">Senior Project Coordinator</p>
                      <a href="#" data-toggle="modal" data-target="#largeModal9" title="View Bio" class="btn blue-btn">View Bio</a>
                    </div>
                  </div>
                  <!--Modal 9-->
                  <div class="modal fade" id="largeModal9" tabindex="-1" role="dialog" aria-hidden="true">
                      <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                              <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                      <h4 class="modal-title" >Diana Pernigotti, MSG - Senior Project Coordinator</h4>
                              </div>
                              <div class="modal-body">
                                <div class="thumbnail left-align">
                                  <img src="<?php bloginfo('url'); ?>/media/pernigotti.png" alt="" />
                                </div>
                                <p>Ms. Pernigotti has worked at Gaylord Hospital in Wallingford, Connecticut since 2009. She is responsible for managing the recruitment and implementation of multiple SCI research projects at Gaylord Hospital and throughout the SNERSCIC network. She organizes the New England SCI Toolkit and serves on the Institutional Review Board at Gaylord. As an Executive Board Member of the SCI Association of CT, a Chapter of United Spinal, she engages people at every level from inpatients to professional members of the community. Consumer education, research opportunities and all programs related to improving the lives of people with and affected by SCI are priorities of both roles she fulfills as a Senior Project Coordinator and SCIACT Board Member.</p>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="about-boxes">
                        <img src="<?php bloginfo('url'); ?>/media/slocum.png" alt="" />
                      <h3>Dr. Chloe Slocum, MD, MPH</h3>
                      <p data-mh="team">Spaulding SCI Physician<br> and Co-Investigator</p>
                      <a href="#" data-toggle="modal" data-target="#largeModal10" title="View Bio" class="btn blue-btn">View Bio</a>
                    </div>
                  </div>
                  <!--Modal 10-->
                  <div class="modal fade" id="largeModal10" tabindex="-1" role="dialog" aria-hidden="true">
                      <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                              <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                      <h4 class="modal-title" >Dr. Chloe Slocum, MD, MPH - Spaulding SCI Physician and Co-Investigator</h4>
                              </div>
                              <div class="modal-body">
                                <div class="thumbnail left-align">
                                  <img src="<?php bloginfo('url'); ?>/media/slocum.png" alt="" />
                                </div>
                                <p>Dr. Slocum is a member of the Department of Physical Medicine and Rehabilitation at Harvard Medical School, where she is an SCI physician in the Division of Spinal Cord Medicine. Dr. Slocum has the unique perspective of a physician as well as advocate for patients and families through her work as a leader on numerous health policy initiatives at the state, local and national levels. She completed her Master of Public Health (MPH) in Health Policy as a Commonwealth Fund Mongan Fellow in Health Policy from 2016-2017 at the Harvard School of Public Health and has presented nationally and been invited to speak internationally on health issues faced by persons with SCI and health equity for people with disabilities. Dr. Slocum’s work has been published in medical journals and she lectures regularly to health professionals and physicians-in-training on health promotion in SCI.  When she’s not working, she is an avid reader and adaptive sports enthusiast.</p>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="about-boxes">
                        <img src="<?php bloginfo('url'); ?>/media/min.png" alt="" />
                      <h3>Jenny Min</h3>
                      <p data-mh="team">Research Assistant</p>
                      <a href="#" data-toggle="modal" data-target="#largeModal11" title="View Bio" class="btn blue-btn">View Bio</a>
                    </div>
                  </div>
                  <!--Modal 11-->
                  <div class="modal fade" id="largeModal11" tabindex="-1" role="dialog" aria-hidden="true">
                      <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                              <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                      <h4 class="modal-title" >Jenny Min - Research Assistant</h4>
                              </div>
                              <div class="modal-body">
                                <div class="thumbnail left-align">
                                  <img src="<?php bloginfo('url'); ?>/media/min.png" alt="" />
                                </div>
                                <p>Jenny joined Spaulding’s SCI Model System in 2015. As a research assistant, she is involved in the data-gathering of spinal cord injury-related studies and dissemination of information for SCI consumers, their families and clinicians.</p>
                              </div>
                          </div>
                      </div>
                  </div>

                  </div>
                </div>
              </div>
            </div>
            <!--End of Tab 2-->

            <!--Start of Tab 3-->
            <div id="tabs-3">
              <div class="tab-pane" id="spinal-cord-injury-model-systems-program">
                <div class="about-snerscic-content">
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="about-sci-systems-top">
                        <p>The Spinal Cord Injury Model Systems (SCIMS) are funded by the National Institute on Disability, Independent Living, and Rehabilitation Research (NIDILRR). The Spinal Cord Injury Model Systems are called "model systems" because they are national leaders in SCI-related care and research.</p>
                        <p>Currently, there are 14 SCIMS centers across the United States; each center provides the highest level of comprehensive and multidisciplinary care from point of injury through rehabilitation and full community re-entry. In addition to these 14 centers, the Spinal Cord Injury Model Systems Data Center directs the collection, management, and analysis of a longitudinal national SCI database.</p>
                      </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                      <div class="about-boxes">
                        <figure>
                          <img src="<?php bloginfo('url'); ?>/media/nscisc.png" alt="" />
                        </figure>
                        <h3>National Spinal Cord Injury Statistical Center</h3>
                        <p>(NSCISC)</p>
                        <a href="#" data-toggle="modal" data-target="#national1" class="btn blue-btn">More Info</a>
                      </div>
                    </div>
                    <!--MODAL#1 BEGINS-->
                    <div class="modal fade" id="national1" tabindex="-1" role="dialog" aria-labelledby="largeModal-t3-1" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 id="largeModal-t3-1" class="modal-title" >National Spinal Cord Injury Statistical Center (NSCISC)</h4>
                                </div>
                                <div class="modal-body">
                                  <p>NSCISC, located at UAB, supports and directs the collection, management and analysis of data
                                    contributed from SCIMS Centers.  The Model Systems database is the world’s largest and longest
                                    SCI research database.  Findings have had significant impact on the delivery and nature of SCI
                                    rehabilitation services.
                                  </p>
                                    <ul>
                                      <li><a href="http://www.msktc.org/lib/docs/Data_Sheets_/SCIMS_Facts_and_Figures_2017_August_FINAL.pdf" target="_blank">SCI Facts and Figures at a Glance</a></li>
                                      <li><a href="http://www.bu.edu/nerscic/files/2015/10/SCI_Facts_and_Figures_at_a_Glance_Spanish-2015.pdf" target="_blank">Spanish SCI Facts and Figures</a></li>
                                      <li><a href="https://www.nscisc.uab.edu/PublicDocuments/fact_sheets/Recent%20trends%20in%20causes%20of%20SCI.pdf" target="_blank">Recent Trends in Causes of SCI</a></li>
                                      <li><a href="https://www.nscisc.uab.edu/public/2016%20Annual%20Report%20-%20Complete%20Public%20Version.pdf" target="_blank">2016 NSCISC Annual Statistical Report- Complete Public Version</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                      <div class="about-boxes">
                        <figure>
                          <img src="<?php bloginfo('url'); ?>/media/msktc.png" alt="" />
                        </figure>
                        <h3>Model System Knowledge Translation Center</h3>
                        <p>(MSKTC)</p>
                    <a href="#" data-toggle="modal" data-target="#national2" class="btn blue-btn">More Info</a>
                  </div>
                </div>
                <!--MODAL#2 BEGINS-->
                <div class="modal fade" id="national2" tabindex="-1" role="dialog" aria-labelledby="largeModal-t3-2" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 id="largeModal-t3-2" class="modal-title" >Model System Knowledge Translation Center (MSKTC)</h4>
                            </div>
                            <div class="modal-body">
                              <p>MSKTC is a national Center that provides resources about the Model Systems to consumers, clinicians
                                and researchers.  Resources include SCI Fact Sheets, information about SCIMS centers and their studies,
                                a research publications database for spinal cord injuries, and quick review summaries of Model Systems
                                studies for consumers. 
                              </p>
                                <ul>
                                  <li><a href="http://www.bu.edu/nerscic/files/2013/08/MSKTC_SCIMS_Program_Factsheet_Brochure_.pdf" target="_blank">SCI Model Systems Program Brochure</a></li>
                                  <!-- Waiting for phase 2 for this button to direct to SCI EDUCATION-consumer factshets
                                  <li><a href="http://www.bu.edu/nerscic/consumer-education/consumer-fact-sheets/" target="_blank">SCI Consumer Fact Sheets</a></li>-->
                                  <li><a href="http://www.msktc.org/sci/Quick-Reviews" target="_blank">Quick Reviews of Model Systems Research</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>


                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                      <div class="about-boxes">
                        <figure>
                          <img src="<?php bloginfo('url'); ?>/media/uab-logo.png" alt="" />
                        </figure>
                        <h3>UAB Model Spinal Cord Injury Care System</h3>
                        <p>University of<br> Alabama/Birmingham</p>
                        <a href="http://www.uab.edu/sci/" target="_blank" title="Visit Site" class="btn blue-btn">Visit Site</a>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                      <div class="about-boxes">
                        <figure>
                          <img src="<?php bloginfo('url'); ?>/media/rancho-logo.png" alt="" />
                        </figure>
                        <h3>Southern California Spinal Cord Injury Model System</h3>
                        <p>Los Amigos Rehabilitation and Education Institute, Downey, CA</p>
                        <a href="http://www.larei.org/" target="_blank" title="Visit Site" class="btn blue-btn">Visit Site</a>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                      <div class="about-boxes">
                        <figure>
                          <img src="<?php bloginfo('url'); ?>/media/craig-logo.png" alt="" />
                        </figure>
                        <h3>Rocky Mountain Regional Spinal Injury System</h3>
                        <p>Craig Hospital, Englewood, CO</p>
                        <a href="https://craighospital.org/programs/research" target="_blank" title="Visit Site" class="btn blue-btn">Visit Site</a>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                      <div class="about-boxes">
                        <figure>
                          <img src="<?php bloginfo('url'); ?>/media/health-logo.png" alt="" />
                        </figure>
                        <h3>South Florida Regional Spinal Cord Injury Model System</h3>
                        <p>University of Miami, Miami, FL</p>
                        <a href="http://scimiami.med.miami.edu/" target="_blank" title="Visit Site" class="btn blue-btn">Visit Site</a>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                      <div class="about-boxes">
                        <figure>
                          <img src="<?php bloginfo('url'); ?>/media/shepherd-center-logo.png" alt="" />
                        </figure>
                        <h3>Southeastern Regional Spinal Cord Injury Model System</h3>
                        <p>Shepherd Center, Inc., Atlanta, GA</p>
                        <a href="http://www.shepherd.org/research/model-system-of-care" target="_blank" title="Visit Site" class="btn blue-btn">Visit Site</a>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                      <div class="about-boxes">
                        <figure>
                          <img src="<?php bloginfo('url'); ?>/media/shirley-ryan-abilitylab-logo.png" alt="" />
                        </figure>
                        <h3>Midwest Regional Spinal Cord Injury Care System (MRSCIS)</h3>
                        <p>Rehabilitation Institute of Chicago, Chicago, IL</p>
                        <a href="https://www.sralab.org/conditions/spinal-cord-injury" target="_blank" title="Visit Site" class="btn blue-btn">Visit Site</a>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                      <div class="about-boxes">
                        <figure>
                          <img src="<?php bloginfo('url'); ?>/media/spaulding-logo.png" alt="" />
                        </figure>
                        <h3>New England Regional Spinal Cord Injury Center</h3>
                        <p>Gaylord Hospital, Wallingford, CT<br> Spaulding Rehabilitation Hospital, Boston, MA</p>
                        <a href="<?php echo home_url(); ?>" title="Visit Site" class="btn blue-btn">Visit Site</a>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                      <div class="about-boxes">
                        <figure>
                          <img src="<?php bloginfo('url'); ?>/media/kessler-foundation-logo.png" alt="" />
                        </figure>
                        <h3>Northern New Jersey Spinal Cord Injury System</h3>
                        <p>Kessler Medical Rehabilitation<br> Research and Education Corporation (KMRREC), West Orange, NJ</p>
                        <a href="http://www.kessler-rehab.com/programs/spinal-cord-injury-rehab/Default.aspx" target="_blank" title="Visit Site" class="btn blue-btn">Visit Site</a>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                      <div class="about-boxes">
                        <figure>
                          <img src="<?php bloginfo('url'); ?>/media/mount-sinai.png" alt="" />
                        </figure>
                        <h3>Mount Sinai Hospital Spinal Cord Injury Model System</h3>
                        <p>Mount Sinai Hospital, New York, NY</p>
                        <a href="http://www.mssm.edu/research/programs/mount-sinai-spinal-cord-injury-model-system/about-us" target="_blank" title="Visit Site" class="btn blue-btn">Visit Site</a>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                      <div class="about-boxes">
                        <figure>
                          <img src="<?php bloginfo('url'); ?>/media/ohio.png" alt="" />
                        </figure>
                        <h3>Ohio Regional Spinal Cord Injury Model System</h3>
                        <p>Ohio State University Wexner Medical Center, Columbus, Ohio</p>
                        <a href="#" target="_blank" title="Visit Site" class="btn blue-btn">Visit Site</a>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                      <div class="about-boxes">
                        <figure>
                          <img src="<?php bloginfo('url'); ?>/media/northeast-ohio.png" alt="" />
                        </figure>
                        <h3>Northeast Ohio Regional Spinal Cord Injury System</h3>
                        <p>Case Western Reserve University, Cleveland, OH</p>
                        <a href="http://www.metrohealth.org/rehab" target="_blank" title="Visit Site" class="btn blue-btn">Visit Site</a>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                      <div class="about-boxes">
                        <figure>
                          <img src="<?php bloginfo('url'); ?>/media/jefferson-logo.png" alt="" />
                        </figure>
                        <h3>Regional Spinal Cord Injury Center of the Delaware Valley</h3>
                        <p>Thomas Jefferson University, Philadelphia, PA</p>
                        <a href="http://www.spinalcordcenter.org/" target="_blank" title="Visit Site" class="btn blue-btn">Visit Site</a>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                      <div class="about-boxes">
                        <figure>
                          <img src="<?php bloginfo('url'); ?>/media/university-pittsburgh-logo.png" alt="" />
                        </figure>
                        <h3>University of Pittsburgh Model Center on Spinal Cord Injury</h3>
                        <p>Pittsburgh, PA</p>
                        <a href="http://www.upmc-sci.pitt.edu/" target="_blank" title="Visit Site" class="btn blue-btn">Visit Site</a>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                      <div class="about-boxes">
                        <figure>
                          <img src="<?php bloginfo('url'); ?>/media/texas.png" alt="" />
                        </figure>
                        <h3>Texas Model Spinal Cord Injury System at TIRR</h3>
                        <p>Memorial Hermann, Houston, TX </p>
                        <a href="http://www.memorialhermann.org/locations/TIRR/research.aspx" target="_blank" title="Visit Site" class="btn blue-btn">Visit Site</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!--End of Tab 3-->

            <!--Start of Tab 4-->
            <div id="tabs-4">
              <div class="tab-pane" id="snerscic-network-sites">
                <div class="about-snerscic-content">
                  <div class="row">
                    <div class="col-lg-offset-2 col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="about-boxes">
                      <figure>
                        <img src="<?php bloginfo('url'); ?>/media/network-spaulding.png" alt="" />
                      </figure>
                      <h3>Spaulding Rehabilitation Network</h3>
                      <p>(throughout MA)</p>
                      <a href="#" data-toggle="modal" data-target="#networkSites1" class="btn blue-btn">More Info</a>
                    </div>
                  </div>
                  <!--MODAL#1 BEGINS-->
                  <div class="modal fade" id="networkSites1" tabindex="-1" role="dialog" aria-hidden="true">
                      <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                              <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                      <h4 class="modal-title" >Spaulding Rehabilitation Network</h4>
                              </div>
                              <div class="modal-body">
                                <img src="<?php bloginfo('url'); ?>/media/modal-spaulding.jpg" alt="" />
                                <p>The Spaulding Rehabilitation Network Spinal Cord Injury programs provide an unparalleled level of care and support for individuals recovering from SCI. We treat patients who have suffered a SCI as a result of traumatic injury or as a result of illnesses such as polio, spina bifida or multiple sclerosis. Our comprehensive rehabilitation program focuses on maximizing neurorecovery while helping patients develop strategies for independent living. We do this in a comfortable, supportive environment in which patients—with the help and support of their families—can attain the skills they need to return to their community. We work with Massachusetts General Hospital (MGH) and Brigham and Women’s Hospital (BWH) for acute care, and MGH Institute for Health Professions (IHP) for research and education support.</p>
                                <a href="http://spauldingrehab.org/conditions-and-treatments/spinal-cord-injury-rehabilitation" target="_blank"><p>Learn more about receiving SCI care at Spaulding</p></a>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="about-boxes">
                      <figure>
                        <img src="<?php bloginfo('url'); ?>/media/network-gaylord.png" alt="" />
                      </figure>
                      <h3>Gaylord Specialty Healthcare</h3>
                      <p>(Wallingford, CT)</p>
                      <a href="#" data-toggle="modal" data-target="#networkSites2" class="btn blue-btn">More Info</a>
                    </div>
                  </div>
                  <!--MODAL#2 BEGINS-->
                  <div class="modal fade" id="networkSites2" tabindex="-1" role="dialog" aria-hidden="true">
                      <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                              <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                      <h4 class="modal-title" >Gaylord Specialty Healthcare</h4>
                              </div>
                              <div class="modal-body">
                                <img src="<?php bloginfo('url'); ?>/media/modal-gaylord.jpg" alt="" />
                                <p>Gaylord Specialty Healthcare is a prominent Long-Term Acute Care Hospital (LATCH) collaborating with Yale New Haven Hospital that serves a higher proportion of patients with medically complex conditions in a suburban/rural locale.  Dr. David Rosenblum, SCI-certified physiatrist, is SNERSCIC’s Director of Clinical Care, and Medical Director of Rehabilitation Services and of Outpatient Medical Services at Gaylord, directing Gaylord’s SCI program.   Gaylord Specialty Healthcare is a 137-bed non-profit specialty hospital specializing in medical management and rehabilitation for patients who have experienced an acute illness or a traumatic accident. Gaylord is staffed and equipped to handle the needs of the acutely ill or chronically disabled patients who require a hospital level of care, and has expertise in treating individuals who need care and rehabilitation for illness or injury related to the  spinal cord, brain, and many other conditions . Gaylord offers both inpatient and outpatient rehabilitation to meet the patient’s needs throughout their life. </p>
                                <a href="https://www.gaylord.org/" target="_blank"><p>Learn more about receiving SCI care at Gaylord</p></a>
                              </div>
                          </div>
                      </div>
                  </div>
                  </div>
                </div>
              </div>
            </div>
            <!--End of Tab 4-->

          </div>
        </div>
      </div>
    </div>
  </div>
</section>
</div><!--End of Tab Container-->
<!-- Mid Container Ends Here -->
</div><!--End of Tabs-->

