<!-- Start of Homepage Slider -->
<section class="banner">
  <div class="banner-slider">
    <!--Start Slide 1-->
    <div>
      <div class="banner-main">
        <img src="<?php bloginfo('url'); ?>/media/banner-image.jpg" alt="" />
        <div class="banner-content">
          <div class="tbl">
            <div class="tbl-cell">
              <div class="container">
                <div class="row">
                  <div class="col-lg-8 col-md-10 col-sm-12">
                    <h2>Interested In Participating In One of Our Upcoming Studies?</h2>
                    <a href="<?php echo home_url(); ?>/research/currently-recruiting/" title="Learn More" class="btn" target="_blank">Learn More</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--End Slide 1-->

    <!--Start Slide 2-->
    <div>
      <div class="banner-main">
        <img src="<?php bloginfo('url'); ?>/media/banner-image2.jpg" alt="" />
        <div class="banner-content">
          <div class="tbl">
            <div class="tbl-cell">
              <div class="container">
                <div class="row">
                  <div class="col-lg-8 col-md-10 col-sm-12">
                    <h2>Get the Resources You Need When You Need Them</h2>
                    <a href="<?php echo home_url(); ?>/resources/resource-list/" title="Learn More" class="btn" target="_blank">Learn More</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--End Slide 2-->

    <!--Start Slide 3-->
    <div>
      <div class="banner-main">
        <img src="<?php bloginfo('url'); ?>/media/banner-image3.jpg" alt="" />
        <div class="banner-content">
          <div class="tbl">
            <div class="tbl-cell">
              <div class="container">
                <div class="row">
                  <div class="col-lg-8 col-md-10 col-sm-12">
                    <h2>Hear from the Experts on the Latest, Cutting-Edge Research in SCI</h2>
                    <a href="<?php echo home_url(); ?>/sci-education/kim-lecture-with-live-webcast/" title="Learn More" class="btn" target="_blank">Learn More</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--End Slide 3-->

  </div>
</section>
<!-- End of Homepage Slider -->