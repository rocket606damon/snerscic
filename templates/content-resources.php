<!-- Banner Starts Here -->
<section class="inner-banner lazy" data-src="<?php bloginfo('url'); ?>/media/resources-banner.jpg">
  <div class="tbl">
    <div class="tbl-cell-bottom">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 col-sm-12">
            <h1>Resources</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Banner Ends Here -->

<!-- Mid Content Starts Here -->
<section class="mid-content mid-content-blue">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
        <?php the_content(); ?>
      </div>
    </div>
  </div>
</section>

<!-- Mid Page Links Starts Here -->
<div id="tabs" class="resources-page">
<section class="page-links">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
        <ul class="links-listing">

          <li class="resources-tabs-1" class="li-clicker">
            <div class="tbl">
              <div class="tbl-cell">
                <a href="#tabs-1" class="tab-clicker" title="New England SCI Toolkit">
                  <i class="icon icon-toolkit"></i>
                  <span>New England sci Toolkit (NESCIT)</span>
                </a>
              </div>
            </div>
          </li>

          <li class="resources-tabs-2" class="li-clicker">
            <div class="tbl">
              <div class="tbl-cell">
                <a href="#tabs-2" class="tab-clicker" title="Resource Referrals">
                  <i class="icon icon-referral"></i>
                  <span>Resource Referral</span>
                </a>
              </div>
            </div>
          </li>

          <li class="resources-tabs-3" class="li-clicker">
            <div class="tbl">
              <div class="tbl-cell">
                <a href="#tabs-3" class="tab-clicker" title="Resource List">
                  <i class="icon icon-list"></i>
                  <span>Resource List</span>
                </a>
              </div>
            </div>
          </li>

          <li class="resources-tabs-4" class="li-clicker">
            <div class="tbl">
              <div class="tbl-cell">
                <a href="#tabs-4" class="tab-clicker" title="Upcoming Events">
                  <i class="icon icon-news"></i>
                  <span>Community News &amp; Events</span>
                </a>
              </div>
            </div>
          </li>

        </ul>
      </div>
    </div>
  </div>
</section>
<!-- Mid Page Links Ends Here -->


<!-- Mid Container Starts Here -->
<div id="tab-container" class="tab-container about-page">
  <section>
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="tab-content">

            <!--Start Tab 1-->
            <div id="tabs-1">
              <div class="tab-pane" id="new-england-sci-toolkit-nescit">
                <div class="resource-snerscic-content">
                  <div class="row">

                    <!--Top Section-->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                      <div class="mid-banner-referral-content">
                        <?php the_field('sci_toolkit_topsection', 125); ?>
                      </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" id="toolkit-list">
                      <div class="resources-content">

                      <?php if(have_rows('toolkit_resource', 125)): $i = 0;
                      while (have_rows('toolkit_resource', 125)) : $i++; the_row();

                        $modal = get_sub_field('title');
                        $modal = preg_replace("/[^A-Za-z0-9 ]/", '', $modal);
                        $modal = str_replace(" ", "-", $modal);
                        $modal = strtolower($modal);
                        
                        $toolkit_image = get_sub_field('image');
                      ?>  

                      <!--Block 1-->
                      <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="spacer">
                        <div class="toolkit-icon">
                          <img class="lazy" src="<?php bloginfo('url'); ?>/media/spinner.gif" data-src="<?php echo $toolkit_image['url']; ?>" alt="<?php echo $toolkit_image['alt']; ?>">
                        </div>
                        <div class="toolkit-text" data-mh="toolkit-text">
                          <h3><?php the_sub_field('title'); ?></h3>
                          <p><?php the_sub_field('description', false, false); ?></p>
                        </div>
                        <a href="#" data-toggle="modal" data-target="#<?php echo $modal; ?>">Learn More</a>
                      </div>
                      </div>
                      <!--MODAL#1 BEGINS-->
                      <div class="modal fade" id="<?php echo $modal; ?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title" id="myModalLabel"><?php echo $i . "."?>&nbsp;<?php the_sub_field('title'); ?></h4>
                            </div>

                            <div class="modal-body">
                              <p><a href="<?php the_sub_field('resource_link'); ?>" target="_blank">[Print Friendly Version]</a></p>
                              <?php the_sub_field('modal_body_content'); ?>
                            </div>

                            <div class="modal-footer">
                              <button type="button" class="btn-prev">&lt; Prev</button>
                              <button type="button" class="btn-next">Next ></button>
                              <button type="button" class="btn-default" data-dismiss="modal">Close</button>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!--MODAL#1 ENDS-->

                      <?php endwhile; endif; ?>

                      </div><!--End of Resource Content-->
                    </div><!--End of Toolkit DIV-->

                  </div>
                </div>
              </div>
            </div>
            <!--End Tab 1-->

            <!--Start Tab 2-->
            <div id="tabs-2">
              <div class="tab-pane" id="resource-referral">
                <div class="resource-snerscic-content">
                  <div class="row">

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                      <div class="mid-banner-referral-content">
                        <?php the_field('resource_referral_content', 125, false, false); ?>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
            <!--End Tab 2-->


          <div id="tabs-3">
              <div class="tab-pane" id="resource-list">
                <div class="resource-snerscic-content">

                  <div class="row">
                    <div class="col-lg-12">
                    <div class="resource-list-top-text">
                      <?php the_field('resource_list_top_text', 125, false, false); ?>
                    </div>

                    <section class="snerscic-network-resources">
                      <h3>SNERSCIC Network Site Resources</h3>
                      <?php $g = 0; while (have_rows('in_network', 125)) : the_row();
                      ?>
                        <p class="network-title"><?php the_sub_field('header_title'); ?></p>
                        <ul class="clearfix">
                      <?php $count = ''; $i = 1; while (have_rows('network_links', 125)) : the_row(); ?>
                      <?php
                        $my_count = count(get_sub_field('link_list'));
                        $count = $my_count;
                      ?>
                        
                        <li class="new-resource-type">
                          <p class="left-link"><?php the_sub_field('section_title'); ?></p>
                          <a data-toggle="collapse" data-target="#link<?php echo $g . "-" . $i; ?>" class="right-link"><?php echo $count; ?><i class="fa fa-link"></i></a>
                          <div id="link<?php echo $g . "-" . $i; ?>" class="collapse">
                            <div class="internal">
                              <ul class="clearfix links-list">
                                <?php $k = 0; while (have_rows('link_list', 125)) : the_row(); ?>
                                <li class="list<?php echo $k; ?>"><a href="<?php the_sub_field('link'); ?>" target="_blank"><?php the_sub_field('title'); ?></a></li>
                                <?php $k++; endwhile; ?>
                              </ul>
                            </div>
                          </div>
                        </li> 
                        
                      <?php $i++; endwhile; ?>
                      </ul>
                      <?php $g++; endwhile; ?>
                      </section>

                    <section class="snerscic-network-resources">
                      <h3>General Resources</h3>
                    </section>

                    <div class="btn-group">
                      <a href="#" id="list"><i class="fa fa-bars" aria-hidden="true"></i>List</a>
                      <a href="#" id="grid"><i class="fa fa-th-large" aria-hidden="true"></i>Grid</a>
                    </div>

                    <div id="products" class="row list-group">

                      <?php if(have_rows('resource_list', 125)): $i = 0;
                      while (have_rows('resource_list', 125)) : $i++; the_row();

                        $modal = get_sub_field('title');
                        $modal = preg_replace("/[^A-Za-z0-9 ]/", '', $modal);
                        $modal = str_replace(" ", "-", $modal);
                        $modal = strtolower($modal);
                        
                        $grid_image = get_sub_field('image');
                      ?> 

                      <!--Item 1-->
                      <div class="item col-xs-12 col-md-6 col-lg-4 resource-<?php echo $i; ?>">
                        <div class="thumbnail">
                          <a href="#" data-toggle="modal" data-target="#<?php echo $modal; ?>">
                            <img class="group list-group-image lazy" src="<?php bloginfo('url'); ?>/media/spinner.gif" alt="<?php the_sub_field('title'); ?>" data-src="<?php echo $grid_image['url']; ?>" />
                            <div class="caption-grid caption-words">
                              <h4 class="group inner list-group-item-heading"><?php the_sub_field('title'); ?></h4>
                              <span class="links-number hide"><?php the_sub_field('link_number'); ?> <i class="fa fa-link" aria-hidden="true"></i></span>
                            </div>
                          </a>
                        </div>
                      </div>
                      <!--MODAL#1 BEGINS-->
                      <div class="modal fade" id="<?php echo $modal; ?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title" id="myModalLabel"><?php the_sub_field('title'); ?></h4>
                            </div>
                            <div class="modal-body">
                              <?php the_sub_field('modal_content'); ?>
                            </div>
                          </div>
                        </div>
                      </div>

                      <?php endwhile; endif; ?>

                    </div><!--End of Products div-->

                    <div class="loader"><a href="#" id="loadMoreResources">Load More</a></div>

                  </div>
                </div>
                </div>
              </div>
            </div>
            <!--End Tab 3-->

            <!--Start Tab 4-->
            <div id="tabs-4">
              <div class="tab-pane" id="upcoming-events">
                <div class="resource-snerscic-content">
                  <div class="row">
                  	<div class="col-lg-12">
                  	  <?php $g = 0; while (have_rows('upcoming_events', 125)) : the_row();
                      ?>
                      <h3><?php the_sub_field('header_title'); ?></h3>
                      <?php $i = 0; while (have_rows('upcoming_list', 125)) : the_row();

                        $modal = get_sub_field('modal_title');
                        $modal = preg_replace("/[^A-Za-z0-9 ]/", '', $modal);
                        $modal = str_replace(" ", "-", $modal);
                        $modal = strtolower($modal);

                      ?>
                      <aside class="event-section">
                        <?php if(get_sub_field('modal_event')): ?>
                          <p class="event-title"><a href="#" data-toggle="modal" data-target="#<?php echo $modal; ?>"><?php the_sub_field('modal_title'); ?></a></p>
                        <?php else: ?>  
                        <?php the_sub_field('event_title'); ?>
                        <?php endif ?>
                        <?php if(get_sub_field('event_date')): ?><p class="event-date"><?php the_sub_field('event_date'); ?></p><?php endif; ?>
                        <?php if(get_sub_field('event_standard')): ?><?php the_sub_field('event_standard'); ?><?php endif; ?>
                        <?php if(get_sub_field('event_extra')): ?><?php the_sub_field('event_extra'); ?><?php endif; ?>
                      </aside>

                      <?php if(get_sub_field('modal_event')): ?>
                      <div class="modal fade" id="<?php echo $modal; ?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title" id="myModalLabel"><?php the_sub_field('modal_title'); ?></h4>
                            </div>
                            <div class="modal-body">
                              <?php the_sub_field('modal_content'); ?>
                            </div>
                          </div>
                        </div>
                      </div>
                      <?php endif; ?>

                      <?php $i++; endwhile; ?>
                      <?php $g++; endwhile; ?>
                  	  <h3>Also Check Your Local Independent Living Center:</h3>
                  	  <svg id="New-England" viewBox="0 0 337.39 560.42">
                  	    <defs>
                  	      <style>
                  	      #New-England {width:50%; margin:0 auto; display: block; padding-top: 30px;}
                  	      .state:hover{fill:#87ceeb;cursor:pointer;}
                  	      .state{fill:#008ca8;stroke:#000;transition: fill .3s;}
                  	      .state-name{fill:#fff;}
                  	      </style>
                  	    </defs>
                  	    <a href="http://www.ilru.org/projects/cil-net/cil-center-and-association-directory-results/RI" target="_blank"><path id="RI" class="state" d="M151,508.68l-1.68-14.91-2.82-15.49-5.92-20.94,20-5.44,5.63,4L178,471.4l10.14,15.77L178,492.63l-4.5-.58-3.95,6.31-8.45,6.89Z"/></a>
                  	    <a href="http://www.ilru.org/projects/cil-net/cil-center-and-association-directory-results/ct" target="_blank"><path id="CT" class="state" d="M152.66,508.68l-3.37-14.91-2.82-15.49-5.63-21.22-18,4L46.75,478,49,489.75l5.07,25.81v28.68l-3.95,8,6.39,7.48,17.27-12.06,12.39-11.48,6.76-7.45,2.82,2.29,9.58-5.16,18-4Z"/></a>
                  	    <a href="http://www.ilru.org/projects/cil-net/cil-center-and-association-directory-results/MA" target="_blank"><path id="MA" class="state" d="M241.26,487.5l7.56-2.44,1.6-6.08,3.58.4,3.59,8.12-4.38,1.62-13.55.4Zm-32.67,2.84,8-9.34h5.58l6.37,5.28-8.37,3.65-7.57,3.65Zm-121.27-78,60.84-14.91,7.89-2.29,7.32-11.47,13-5.9,10.07,15.65L178,411.75l-1.12,5.17,6.76,9.17,3.94-2.86h6.2l7.88,9.17,13.53,21.23,12.39,1.72,7.89-3.45,6.19-6.3-2.81-9.75-7.33-5.74L226.46,433l-3.38-4.59,1.69-1.72,7.33-.57,6.19,2.86,6.77,8.61,3.37,10.32,1.13,8.6-14.65,5.16-13.52,6.88L207.87,484.6l-6.76,5.16v-3.44l8.45-5.17,1.69-6.3L208.44,464l-10.15,5.16-2.81,5.16,1.69,8-9.58,5.16L178,471.4l-11.83-15.49-5.64-4-20,5.44-17.74,3.74L46.75,478l-3.38-20.36,2.26-37.56,18-3.16,23.67-4.59"/></a>
                  	    <a href="http://www.ilru.org/projects/cil-net/cil-center-and-association-directory-results/me" target="_blank"><path id="ME" class="state" d="M322.24,145.06l6.76,7.45,7.89,13.2v6.88l-7.33,16.63-6.76,2.29L311,202.41l-16.9,19.5h-4.51c-2.25,0-3.38-7.46-3.38-7.46L280,215l-3.39,5.16-8.45,5.16-3.38,5.17,5.64,5.15-1.69,2.3L267,247.72l-6.76-.57v-5.74l-1.13-4.59L254.07,238l-6.2-11.47-7.32,4.59,4.51,5.16,1.12,4-2.81,4.58,1.13,10.9.56,5.74-5.64,9.17-10.14,1.72-1.13,10.33-18.59,10.9-4.51,1.72-5.63-5.16-10.7,12.61,3.38,11.47L187,318.84l-.57,15.49-5.49,27-8.58-4.11-1.69-10.89-13.52-4-1.13-9.75L130.7,249.44,114.09,197l8.51-1.22,5.27,1.45v-9.17l2.82-19.5,9-16.64,5.07-14.34L138,129V107.78l2.82-3.44,2.82-9.75-.56-5.16-.57-17.21L148.72,55l10.14-31.54,7.32-14.92h4.51l4.51.58v4l4.5,8,9.58,2.3,2.82-2.87V17.16L206.18,6.83l6.2-6.31,5.07.58,20.84,8.6,6.77,3.44L276.6,119.25h20.85l2.82,6.88.56,17.2,10.14,8h2.82l.56-1.72-1.69-4Zm-73,107,5.35-5.45,4.79,3.73,2,8.6-5.92,3.15Zm23.38-20.94,6.2,6.6s4.5.29,4.5-.86.85-7.17.85-7.17l3.1-2.87-2.81-6.31-7.05,2.58Z"/></a>
                  	    <a href="http://www.ilru.org/projects/cil-net/cil-center-and-association-directory-results/nh" target="_blank"><path id="NH" class="state" d="M176.4,377.61l1-5.43,3.8-11.67-8.86-3.25-1.69-10.89-13.52-4-1.13-9.75L130.7,249.44l-16-51.6h-3.12l-2.26,5.73L107,201.84l-3.38-3.44-5.07,6.88-3.33,19.46,1.08,20.11,6.76,9.75v14.34l-13,14.33-9,4v4l3.94,6.32V328L82.25,360.7l-.57,17.21,3.38,4.59-.56,16.06-1.69,6.3L87.88,412l60.28-14.63,7.89-2.3,7.32-11.47Z"/></a>
                  	    <a href="http://www.ilru.org/projects/cil-net/cil-center-and-association-directory-results/VT" target="_blank"><path id="VT" class="state" d="M46.19,420.93l-2.82-20.08-10.7-39-2.25-1.14-10.15-4.59,2.82-10.33-2.82-7.45-9-16.06,3.38-13.77-2.81-18.35L3.38,267.22.57,249.75,95.2,224.21l1.13,20.64,6.76,9.75v14.34l-13,14.33-9,4v4l3.94,6.31V328L82.25,360.7l-.57,17.21,3.38,4.59-.56,16.06-1.69,6.31L87.88,412l-24.23,4.87Z"/></a>
                  	    <path class="state-name" d="M115.2,440.22h-2.14l-4.29-10.78v10.78h-3v-14.7h4.43l4,10.24,4.09-10.24h4.35v14.7h-3V429.49Z"/>
                  	    <path class="state-name" d="M135.06,440.22l-.94-2.73h-5.23l-1,2.73h-3.33l6-14.7H133l5.52,14.7ZM129.77,435h3.51l-1.7-4.93h0Z"/>
                  	    <path class="state-name" d="M96.12,501.58a4.81,4.81,0,0,0,3.12-1.22l1.68,2.18A8,8,0,0,1,96,504.2a7.39,7.39,0,0,1-7.71-7.6,7.66,7.66,0,0,1,7.84-7.6,8.22,8.22,0,0,1,4.51,1.43l-1.45,2.33a5.1,5.1,0,0,0-3.06-1.14,4.71,4.71,0,0,0-4.62,5C91.5,499.54,93.24,501.58,96.12,501.58Z"/>
                  	    <path class="state-name" d="M108.51,491.79V504h-3V491.79H102v-2.54h10v2.54Z"/>
                  	    <path class="state-name" d="M116.94,350.52l-6.74-9.2v9.2h-3v-14.7h2.65l6.8,9.22v-9.22h3v14.7Z"/>
                  	    <path class="state-name" d="M132.15,350.52v-6.3h-6.24v6.3h-3v-14.7h3v5.86h6.24v-5.86h3v14.7Z"/>
                  	    <path class="state-name" d="M159,469.55c2.37,0,5.38,1.22,5.38,4.41a4.25,4.25,0,0,1-3.2,4.1c1.31,1.82,3.49,4.82,4.5,6.19h-3.78l-4-5.86h-.53v5.86h-3v-14.7Zm-1.66,6.3h1.18c1.51,0,2.64-.54,2.64-1.83a2,2,0,0,0-2.24-1.93H157.3Z"/>
                  	    <path class="state-name" d="M167.61,484.25v-14.7h3v14.7Z"/>
                  	    <path class="state-name" d="M210.42,186.5h-2.15L204,175.73V186.5h-3V171.8h4.43l4,10.25,4.09-10.25h4.35v14.7h-3V175.77Z"/>
                  	    <path class="state-name" d="M221.06,186.5V171.8h9.43v2.54h-6.42v3.32h6.42v2.54h-6.42V184h6.42v2.54Z"/>
                  	    <path class="state-name" d="M47.24,328.12H44.78l-5.52-14.7h3.48l3.49,10.1h0l3.65-10.1h3.32Z"/>
                  	    <path class="state-name" d="M60.83,316v12.16h-3V316H54.34v-2.54h10V316Z"/>
                  	  </svg>
                  	</div>
                  </div>
                </div>
              </div>
            </div>
            <!--End Tab 4-->

          </div>
        </div>
      </div>
    </div>
  </div>
  </section>
</div><!--End of Tab Container-->
</div><!--End of Tabs-->

<!-- Mid Container Ends Here -->
