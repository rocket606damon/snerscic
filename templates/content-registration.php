<?php if(is_page('registration-10')): ?>
<!-- Quick Links Starts Here -->
<section class="quick-links-sec">
  <div class="container">
    <div class="row">
      <div class="registration-description">
        <?php the_content(); ?>
      </div>  
      <div id="cp65003d70d3" style="margin: 10px auto 30px" onclick="document.getElementById('pay_now_form_f1a5b24061').submit()"><form action="https://checkout.globalgatewaye4.firstdata.com/pay" id="pay_now_form_f1a5b24061" method="post"><input type="hidden" name="x_login" value="WSP-PARTN-T1fsJACA1w" /><input type="hidden" name="x_show_form" value="PAYMENT_FORM" /><input type="hidden" name="x_fp_sequence" value="14932371731910034528" /><input type="hidden" name="x_fp_hash" value="PNB-1.0-54ceb6da4f26fab1076a15f571c89e14c1e86b8f" /><input type="hidden" name="x_amount" value="10" /><input type="hidden" name="x_currency_code" value="USD" /><input type="hidden" name="x_test_request" value="FALSE" /><input type="hidden" name="x_relay_response" value="" /><input type="hidden" name="donation_prompt" value="" /><input type="hidden" name="button_code" value="Pay Now SRN Class/Lecture" /><div class="cpwrap"><button type="button">Click Here to Pay Online</button></div></form></div><style type="text/css">div#cp65003d70d3{width: 200px; background-color: #0015ff;}div#cp65003d70d3:hover{cursor: pointer}div#cp65003d70d3 * {background-color: #0015ff;}div#cp65003d70d3 form{margin:0; padding:0;text-align:center}div#cp65003d70d3 div.cpwrap {width: 90%;border:0;margin:0 auto;padding: 0px; background-color: #0015ff}div#cp65003d70d3 button{width: 95%;border:0;margin:0;padding: 3px 0; background-color: #0015ff;text-align: center; color: #FFFFFF; }div#cp65003d70d3:hover button {text-decoration: underline}div#cp65003d70d3 button:focus,div#cp65003d70d3 button:visited,div#cp65003d70d3 button:active{border:none;outline: none}div#cp65003d70d3 button {font-size: 16px}div#cp65003d70d3 {border: 2px solid #0015ff}</style>
    </div>
  </div>
</section>
<!-- Quick Links Ends Here -->
<?php else: ?>
<!-- Quick Links Starts Here -->
<section class="quick-links-sec">
  <div class="container">
    <div class="row">
      <div class="registration-description">
        <?php the_content(); ?>
      </div> 
      <div id="cp4a10ffd83a" style="margin: 10px auto 30px" onclick="document.getElementById('pay_now_form_37cf3875cb').submit()"><div><div class='r-bg '><span class='r-fg r-fg-3'></span> <span class='r-fg r-fg-2'></span> <span class='r-fg r-fg-1'></span> <span class='r-fg r-fg-0'></span> </div><form action="https://checkout.globalgatewaye4.firstdata.com/pay" id="pay_now_form_37cf3875cb" method="post"><input type="hidden" name="x_login" value="WSP-PARTN-T1fsJACA1w" /><input type="hidden" name="x_show_form" value="PAYMENT_FORM" /><input type="hidden" name="x_fp_sequence" value="1493237472362992958" /><input type="hidden" name="x_fp_hash" value="PNB-1.0-437ef596daecb0a89166f9b77175e13114ad69b2" /><input type="hidden" name="x_amount" value="20" /><input type="hidden" name="x_currency_code" value="USD" /><input type="hidden" name="x_test_request" value="FALSE" /><input type="hidden" name="x_relay_response" value="" /><input type="hidden" name="donation_prompt" value="" /><input type="hidden" name="button_code" value="Pay Now SRN Class/Lecture" /><div class="cpwrap"><button type="button">Click Here to Pay Online</button></div></form><div class='r-bg '><span class='r-fg r-fg-0'></span> <span class='r-fg r-fg-1'></span> <span class='r-fg r-fg-2'></span> <span class='r-fg r-fg-3'></span> </div></div></div><style type="text/css">div#cp4a10ffd83a{width: 200px; background-color: #0015ff;}div#cp4a10ffd83a button {font-family: "Arial Black", "Arial Bold", Gadget, sans-serif}div#cp4a10ffd83a:hover{cursor: pointer}div#cp4a10ffd83a * {background-color: #0015ff;}div#cp4a10ffd83a form{margin:0; padding:0;text-align:center}div#cp4a10ffd83a div.cpwrap {width: 90%;border:0;margin:0 auto;padding: 0px; background-color: #0015ff}div#cp4a10ffd83a button{width: 95%;border:0;margin:0;padding: 3px 0; background-color: #0015ff;text-align: center; color: #FFFFFF; }div#cp4a10ffd83a:hover button {text-decoration: underline}div#cp4a10ffd83a button:focus,div#cp4a10ffd83a button:visited,div#cp4a10ffd83a button:active{border:none;outline: none}div#cp4a10ffd83a button {font-size: 16px}div#cp4a10ffd83a div.cpwrap {border-left: 3px solid #0015ff; border-right: 3px solid #0015ff}div#cp4a10ffd83a .r-fg{background-color: #0015ff; border-color: #0015ff}div#cp4a10ffd83a .r-bg{background-color: white}div#cp4a10ffd83a .r-fg{border-style: solid; border-width: 0px 1px; overflow: hidden; display: block; height: 1px; font-size: 1px}div#cp4a10ffd83a .r-fg-0{margin-left: 1px; margin-right: 1px; border-width: 0px 1px !important; height: 1px !important}div#cp4a10ffd83a .r-fg-1{margin-left: 2px; margin-right: 2px}div#cp4a10ffd83a .r-fg-2{margin-left: 3px; margin-right: 3px}div#cp4a10ffd83a .r-fg-3{margin-left: 5px; margin-right: 5px}</style>
    </div>
  </div>
</section>
<!-- Quick Links Ends Here -->
<?php endif; ?>