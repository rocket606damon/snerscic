<!-- Banner Starts Here -->
<section class="inner-banner lazy" data-src="<?php bloginfo('url'); ?>/media/reserch-banner.jpg">
  <div class="tbl">
    <div class="tbl-cell-bottom">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 col-sm-12">
            <h1>Research</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Banner Ends Here -->

<!-- Mid Content Starts Here -->
<section class="mid-content mid-content-blue">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
        <?php the_content(); ?>
      </div>
    </div>
  </div>
</section>
<!-- Mid Content Starts Here -->

<!--Research Mid-Banner-->
<section class="mid-banner lazy" data-src="<?php bloginfo('url'); ?>/media/mid-banner.jpg">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
        <h2><?php the_field('mid_research_heading', 121); ?></h2>
        <div class="mid-banner-content">
            <?php the_field('mid_research_content', 121); ?>
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12">
                <a href="#" data-toggle="modal" data-target="#mailinglist-signup" style="text-decoration: none;">
                  <p><strong><?php the_field('mid_research_mailing_list', 121); ?></strong></p>
                  <figure>
                    <?php $mid_research_photo = get_field('mid_research_photo', 121); ?>
                    <img class="lazy" src="<?php bloginfo('url'); ?>/media/spinner.gif" alt="<?php echo $mid_research_photo['alt']; ?>" data-src="<?php echo $mid_research_photo['url']; ?>" />
                  </figure>
                </a>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12">
                <?php the_field('mid_research_address', 121); ?>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--End Research Mid-Banner-->

<!-- Page Links Starts Here -->
<style>
#research-publications .publication-content p{
  margin: 15px 0;
}
#research-publications .publication-content p.indentation{
  margin-bottom: 20px;
  margin-top: -15px;
  margin-left: 30px;
}
</style>
<div id="tabs" class="research-page">
<section class="page-links research-nav" id="research-nav">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
        <ul class="links-listing">

          <li class="research-tabs-1 li-clicker">
            <a href="#tabs-1" class="tab-clicker" title="Currently Recruiting">
              <div class="tbl">
                <div class="tbl-cell">
                  <span>Currently Recruiting</span>
                </div>
              </div>
            </a>
          </li>

          <li class="research-tabs-2 li-clicker">
            <a href="#tabs-2" class="tab-clicker" title="SNERSCIC Studies in 2011-2016">
              <div class="tbl">
                <div class="tbl-cell">
                  <span>SNERSCIC Studies in 2016-2021</span>
                </div>
              </div>
            </a>
          </li>

          <li class="research-tabs-3 li-clicker">
            <a href="#tabs-3" class="tab-clicker" title="Quick Reviews of SNERSCIC Research">
              <div class="tbl">
                <div class="tbl-cell">
                  <span>Quick Reviews of SNERSCIC Research</span>
                </div>
              </div>
            </a>
          </li>

          <li class="research-tabs-4 li-clicker">
            <a href="#tabs-4" class="tab-clicker" title="Publications">
              <div class="tbl">
                <div class="tbl-cell">
                  <span>Publications</span>
                </div>
              </div>
            </a>
          </li>

          <li class="research-tabs-5 li-clicker">
            <a href="#tabs-5" class="tab-clicker" title="Presentations">
              <div class="tbl">
                <div class="tbl-cell">
                  <span>Presentations</span>
                </div>
              </div>
            </a>
          </li>

          <li class="research-tabs-6 li-clicker">
            <a href="#tabs-6" class="tab-clicker" title="Other Research Opportunities">
              <div class="tbl">
                <div class="tbl-cell">
                  <span>Other Research Opportunities</span>
                </div>
              </div>
            </a>
          </li>

        </ul>
      </div>
    </div>
  </div>
</section>
<!-- Page Links Ends Here -->

<!-- Mid Container Starts Here -->
<div id="tab-container" class="tab-container research-page">
  <section>
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="tab-content">

            <!--Start Tab 1-->
            <div id="tabs-1">
              <div class="tab-pane" id="currently-recruiting">
                <div class="research-snerscic-content">
                  <div class="row">

                    <?php $g = 0; while (have_rows('currently_recruiting', 121)) : the_row(); ?>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <div class="currently-recruiting-content">
                        <div class="text-center">
                          <h3><?php the_sub_field('header_title'); ?></h3>
                        </div>
                      </div>  
                    </div>  

                    <?php  $study = 0; while (have_rows('recruiting_list', 121)) : the_row(); ?>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <div class="currently-recruiting-content">
                        <div class="text-center">
                          <h2><?php the_sub_field('heading'); ?></h2>
                          <a href="#" class="btn btn-primary btn-lg special-btn" data-toggle="modal" data-target="#section<?php echo $g; ?>-study<?php echo $study; ?>">See Details of Study</a>
                        </div>
                        <div class="modal fade" id="section<?php echo $g; ?>-study<?php echo $study; ?>" tabindex="-1" role="dialog" aria-hidden="true">
                          <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title"><?php the_sub_field('heading'); ?></h4>
                              </div>
                              <div class="modal-body">
                                <?php the_sub_field('content'); ?>
                              </div>
                            </div>
                          </div>
                        </div>
                        
                      </div>
                    </div>

                    <?php $study++; endwhile; ?>

                    <?php $g++;  endwhile; ?>
                  </div>
                </div>
              </div>
            </div>
            <!--End Tab 1-->

            <!--Start Tab 2-->
            <div id="tabs-2">
              <div class="tab-pane" id="snerscic-studies-in-2016-2021">
                <div class="research-snerscic-content">
                  <div class="row">

                    <div class="col-lg-12">
                      <div class="review-text">
                        <p><?php the_field('snerscic_studies_text', 121); ?></p>
                      </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <div id="accordion" class="panel-group">

                        <?php if(have_rows('snerscic_studies', 121)): $count = 0;
                        while (have_rows('snerscic_studies', 121)) : the_row(); ?>
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title">
                              <a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapse<?php echo $count; ?>"><?php the_sub_field('title'); ?></a>
                            </h4>
                          </div>
                          <div id="collapse<?php echo $count; ?>" class="panel-collapse collapse">
                            <div class="panel-body">
                              <?php the_sub_field('content'); ?>
                            </div>
                          </div>
                        </div>
                        <?php $count++; endwhile;endif; ?>

                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
            <!--End Tab 2-->

            <!--Start Tab 3-->
            <div id="tabs-3">
              <div class="tab-pane" id="quick-reviews-of-snerscic-research">
                <div class="research-snerscic-content">
                  <div class="row">

                    <div class="col-lg-12">
                      <div class="review-text">
                        <?php the_field('quick_review_text', 121); ?>
                      </div>
                    </div>

                    <?php if(have_rows('quick_review', 121)): $i = 0;
                    while (have_rows('quick_review', 121)) : $i++; the_row();

                      $quick_review = get_sub_field('photo');
                    ?>

                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                      <div class="research-boxes quickreview-boxes">
                        <img class="lazy" src="<?php bloginfo('url'); ?>/media/spinner.gif" alt="<?php echo $quick_review['alt']; ?>" data-src="<?php echo $quick_review['url']; ?>">
                          <h3 data-mh="quick-review"><?php the_sub_field('heading'); ?></h3>
                          <a href="<?php the_sub_field('link'); ?>" target="_blank" title="Learn More" class="btn blue-btn">Learn More</a>
                      </div>
                    </div>
                    <?php endwhile; endif; ?>
                  </div>
                </div>
              </div>
            </div>
            <!--End Tab 3-->

                        <!--Start Tab 4-->
            <div id="tabs-4">
              <div class="tab-pane" id="research-publications">
                <div class="research-snerscic-content">
                  <div class="row">

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <div class="publication-content">
                        <h2>Publications</h2>
                        <?php $pub = 0; while (have_rows('publications_list', 121)) : the_row(); 

                          if(get_sub_field('type') == 'Link'){
                            $link = get_sub_field('link');
                            $target = "_blank";
                          } else{
                            $link = get_sub_field('file');
                            $target = "_self";
                          }
                        ?>
                          <p class="standard"><?php the_sub_field('authors'); ?>&nbsp;<a href="<?php echo $link; ?>" target="<?php echo $target; ?>"><?php the_sub_field('resource_title'); ?></a>&nbsp;<img src="<?php bloginfo('url'); ?>/media/pdf-icon.png" alt="pdf-icon"/><?php the_sub_field('trailing_details'); ?></p>
                          <?php if(get_sub_field('quick_review')): the_sub_field('quick_review'); endif; ?>
                        
                        <?php $pub++; endwhile; ?>
                        <div class="share-email"> 
                          <strong>Share this:</strong>
                          <a href="mailto:" title="Email">Email</a>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
            <!--End Tab 4-->

            <!--Start Tab 5-->
            <div id="tabs-5">
              <div class="tab-pane" id="research-presentations">
                <div class="research-snerscic-content">
                  <div class="row presentations">

                    <div class="col-lg-12 col-md-12">
                      <div class="presentation-main"> 
                        <div class="row">
                        <div class="col-sm-12 select-btn">
                          <div class="pres-toggle-select btn-group">
                            <a id="snerscic-top-nav" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#" value="" postid="<?php echo $post->ID; ?>" metakey="pres_select">Select a Type<span class="caret"></span>
                            </a>
                            <ul id="pres-dropdown-menu" class="dropdown-menu">
                              <li><a href="#" data-value="media">Media</a></li>
                              <li><a href="#" data-value="pres-course">Presentations and Courses</a></li>
                              <li><a href="#" data-value="poster">Posters</a></li>
                              <li><a href="#" data-value="award">Awards</a></li>
                              <li><a href="#" data-value="all">All</a></li>
                            </ul>
                          </div>
                        </div>

                        <?php if(have_rows('presentations', 121)): $i = 0;
                        while (have_rows('presentations', 121)) : $i++; the_row();

                          if(get_sub_field('presentation_select') == '1'){
                            $fa = 'fa-link';
                            $type = 'media';
                          } 
                          elseif(get_sub_field('presentation_select') == '2'){
                            $fa = 'fa-desktop';
                            $type = 'pres-course';
                          }
                          elseif(get_sub_field('presentation_select') == '3'){
                            $fa = 'fa-file';
                            $type = 'poster';
                          }
                          else{
                            $fa = 'fa-trophy';
                            $type = 'award';
                          }

                          if(get_sub_field('type') == 'Link'){
                            $link = get_sub_field('link');
                            $target = "_blank";
                          } else{
                            $link = get_sub_field('file');
                            $target = "_self";
                          }
                        ?>  
                        <div class="col-md-12 presentation <?php echo $type; ?>">
                          <div class="presentation-box">
                            <div class="presentation-content">
                              <h3><i class="fa <?php echo $fa; ?>"></i><a href="<?php echo $link; ?>" target="<?php echo $target; ?>"><?php the_sub_field('heading'); ?></a></h3>
                              <p class="authors"><?php the_sub_field('authors'); ?></p>
                              <?php the_sub_field('description'); ?>
                              <a href="<?php echo $link; ?>" class="btn blue-btn" target="<?php echo $target; ?>"><?php the_sub_field('button_text'); ?></a>
                            </div>
                          </div>
                        </div>
                        <?php endwhile; endif; ?>
                      </div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
            <!--End Tab 5-->

            <!--Start Tab 6-->
            <div id="tabs-6">
              <div class="tab-pane" id="other-research-opportunities">
                <div class="research-snerscic-content">
                  <div class="row">

                    <div class="col-lg-12">
                      <div class="review-text">
                        <p><?php the_field('other_opportunities_text', 121); ?></p>
                      </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <?php $g = 0; while(have_rows('research_network', 121)) : the_row(); ?>
                        <h3><?php the_sub_field('header_title'); ?></h3>
                      <div id="accordion" class="panel-group">

                        <?php $count = 0 ; while (have_rows('other_research_opportunities', 121)) : the_row(); ?>
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title">
                              <a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapse<?php echo $g . "-" . $count; ?>"><?php the_sub_field('title'); ?></a>
                            </h4>
                          </div>
                          <div id="collapse<?php echo $g . "-" . $count; ?>" class="panel-collapse collapse">
                            <div class="panel-body">
                              <?php the_sub_field('content'); ?>
                            </div>
                          </div>
                        </div>
                        <?php $count++; endwhile; ?>
                        <?php $g++; endwhile; ?>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
            <!--End Tab 6-->

          </div>
        </div>
      </div>
    </div>
  </div>
  </section>
</div>
</div><!--End of Tabs-->
  <!-- Mid Container Ends Here -->
