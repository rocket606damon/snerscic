<!-- Banner Starts Here -->
<section class="inner-banner" style="background-image: url('<?php bloginfo('url'); ?>/media/reserch-banner.jpg')">
  <div class="tbl">
    <div class="tbl-cell-bottom">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 col-sm-12">
            <h1>Research</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Banner Ends Here -->

<!-- Mid Content Starts Here -->
<section class="mid-content mid-content-blue">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
        <p>The overall goal of the Spaulding New England Regional Spinal Cord Injury Center (SNERSCIC) is to provide a multidisciplinary comprehensive system of care for individuals with SCI as a basis for conducting research that extends the existing evidence base for SCI rehabilitation.Our focus is health, function and technology, especially for the most vulnerable, to empower individuals with SCI to lead fulfilling and meaningful lives.</p>
      </div>
    </div>
  </div>
</section>
<!-- Mid Content Starts Here -->

<!--Research Mid-Banner-->
<section class="mid-banner" style="background-image: url(<?php bloginfo('url'); ?>/media/mid-banner.jpg)">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
        <h2>Participate In A Study</h2>
        <div class="mid-banner-content">
            <h3>Why Participate?</h3>
            <p>To contribute to research that is focused on improving the day-to-day life in ways that matter for people with SCI.  We have all different kinds of studies, and many SNERSCIC studies have an interview format. Interviews can be performed in person or by telephone. We are also happy to make other accommodations as needed.</p>
            <p><strong>See Currently Recruiting for studies you can participate in right now. To learn more about participating in SNERSCIC studies:</strong></p>
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12">
                <a href="#" data-toggle="modal" data-target="#mailinglist-signup" style="text-decoration: none;">
                  <p><strong>Join Our Mailing List</strong></p>
                  <figure>
                    <img src="<?php bloginfo('url'); ?>/media/list-img.png" alt="" />
                  </figure>
                </a>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12">
                <address>
                  <strong>Dave Estrada</strong><br>
                  <em>SNERSCIC Program Manager</em><br>
                  <a href="tel:857-225-2472" title="Call Us">857-225-2472</a><br>
                  <a href="mailto:destrada@partners.org" title="Mail Us">destrada@partners.org</a>
                </address>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--End Research Mid-Banner-->

<!-- Page Links Starts Here -->
<div id="tabs" class="research-page">
<section class="page-links research-nav" id="research-nav">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
        <ul class="links-listing">

          <li class="research-tabs-1 li-clicker">
            <a href="#tabs-1" class="tab-clicker" title="Currently Recruiting">
              <div class="tbl">
                <div class="tbl-cell">
                  <span>Currently Recruiting</span>
                </div>
              </div>
            </a>
          </li>

          <li class="research-tabs-2 li-clicker">
            <a href="#tabs-2" class="tab-clicker" title="SNERSCIC Studies in 2011-2016">
              <div class="tbl">
                <div class="tbl-cell">
                  <span>SNERSCIC Studies in 2016-2021</span>
                </div>
              </div>
            </a>
          </li>

          <li class="research-tabs-3 li-clicker">
            <a href="#tabs-3" class="tab-clicker" title="Quick Reviews of SNERSCIC Research">
              <div class="tbl">
                <div class="tbl-cell">
                  <span>Quick Reviews of SNERSCIC Research</span>
                </div>
              </div>
            </a>
          </li>

          <li class="research-tabs-4 li-clicker">
            <a href="#tabs-4" class="tab-clicker" title="Publications">
              <div class="tbl">
                <div class="tbl-cell">
                  <span>Publications</span>
                </div>
              </div>
            </a>
          </li>

          <li class="research-tabs-5 li-clicker">
            <a href="#tabs-5" class="tab-clicker" title="Presentations">
              <div class="tbl">
                <div class="tbl-cell">
                  <span>Presentations</span>
                </div>
              </div>
            </a>
          </li>

          <li class="research-tabs-6 li-clicker">
            <a href="#tabs-6" class="tab-clicker" title="Other Research Opportunities">
              <div class="tbl">
                <div class="tbl-cell">
                  <span>Other Research Opportunities</span>
                </div>
              </div>
            </a>
          </li>

        </ul>
      </div>
    </div>
  </div>
</section>
<!-- Page Links Ends Here -->

<!-- Mid Container Starts Here -->
<div id="tab-container" class="tab-container research-page">
  <section>
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="tab-content">

            <!--Start Tab 1-->
            <div id="tabs-1">
              <div class="tab-pane" id="currently-recruiting">
                <div class="research-snerscic-content">
                  <div class="row">

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <div class="currently-recruiting-content">
                        <div class="text-center">
                          <h3>The following SNERSCIC studies are currently looking for participants:</h3>
                          <h2>National Spinal Cord Injury Model Systems Outcomes Database Study (the Outcomes Study)</h2>
                        </div>
                        <p>The purpose of this core study is to allow researchers and healthcare professionals to evaluate
                          trends over time for many aspects of SCI to improve rehabilitation services and help people with
                          SCI live the best they can in the community.  Data collection begins during the inpatient
                          rehabilitation stay, is updated approximately one year after injury, and every five years
                          thereafter.  No medical procedures are involved and information is gathered through a one-on-one
                          interview by phone or in person.
                        </p>
                        <h4>You may be eligible for follow up interviews if you:</h4>
                        <ol>
                          <li>Have a diagnosis of traumatic SCI </li>
                          <li>Received your initial inpatient rehabilitation at a Spaulding New England Regional Spinal Cord Injury</li>
                        </ol>
                        <p>Center network site:</p>
                        <ul>
                          <li>Spaulding Rehabilitation Hospital</li>
                          <li>Boston Medical Center (previously named Boston City Hospital, University Hospital, and Boston University Medical Center)</li>
                          <li>Gaylord Specialty Healthcare </li>
                          <li>Hospital for Special Care</li>
                        </ul>
                        <h4>What we would ask you to do:</h4>
                        <p>Answer a 35-45 minute interview by phone or in person at Year 1 and every 5 years post-injury</p>
                        <p style="color:red;">If you received your inpatient rehabilitation at any of the SNERSCIC Network
                          hospitals, have moved or changed your contact information, and are interested in continuing your
                          participation in these important follow-up interviews, please contact:
                        </p>
                        <div class="contact-dtl text-center">
                          <h4>Diana Pernigotti</h4>
                          <a href="mailto:dpernigotti@gaylord.org" title="Mail us">dpernigotti@gaylord.org</a><br> <a href="tel:1-866-429-5673" title="Call Us">1-866-429-5673</a> ext. 3563 (toll free)
                        </div>
                        <div class="text-center">
                          <p style="font-style: italic;">Please provide your name, phone number and best day/time to call,<br> and/or your email address so we can contact you about your next follow-up interview.</p>
                          <p style="font-style: italic;">If you are not sure if you signed up for this study, that’s ok – call us anyway!  We’d LOVE to hear from you!</p>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <div class="currently-recruiting-content">
                        <div class="text-center">
                          <h2>Equity and Quality in Assistive Technology (EQuATe) 2.0 for Individuals with Spinal Cord Injury</h2>
                        </div>
                        <p>SNERSCIC is joining eight other model systems nationwide, led by the University of Pittsburgh, to
                          explore wheelchair equipment and services provided to individuals with SCI of different ethnic,
                          economic, and social backgrounds. The goal of this study is to improve the quality of wheelchairs
                          prescribed for individuals with SCI. In addition, the proposed research may benefit all individuals
                          who use manual wheelchairs and power wheelchairs in the future, given recent changes in health care
                          policy of competitive bidding that limit financial reimbursement for medical services and supplies.
                          The impact of changes in coverage are likely to be profound, but can only be measured if they are tracked.
                        </p>
                        <p>For this study, we will collect information on Assistive Technology (AT) used by individuals with SCI,
                          including wheelchair make, model, failures, and repairs. This data will enable us to track the impact of
                          insurance market changes, find differences in wheelchair failures, and present this information to
                          wheelchair users.
                        </p>
                        <h4>You may be eligible if you:</h4>
                        <ol>
                          <li>Have a non-progressive (i.e., traumatic) spinal cord injury</li>
                          <li>Have an injury that occurred at least one year ago</li>
                          <li>Must be at least 18 years of age</li>
                        </ol>
                        <h4>What would we ask you to do:</h4>
                        <p>There are two parts to the study, and you can take part in one or both:</p>
                        <ul>
                          <li>Part 1: Answer Interview questions - about 45 minutes long, by phone or in person We will give participants a $10 gift certificate when you complete interview.</li>
                          <li>Part 2: Use a mobile app– very quick check-ins on your wheelchair repairs over 6 months  (NOTE: This part will be run by Dr. Mike Boninger, <a href="http://www.upmc-sci.org/" target="_blank">University of Pittsburgh SCI Model System)</a></li>
                        </ul>
                        <br>
                        <p style="text-align:center;">Contact Info COMING SOON</p>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
            <!--End Tab 1-->

            <!--Start Tab 2-->
            <div id="tabs-2">
              <div class="tab-pane" id="snerscic-studies-in-2016-2021">
                <div class="research-snerscic-content">
                  <div class="row">

                    <div class="col-lg-12">
                      <div class="review-text">
                        <p>As a Spinal Cord Injury Model System Center, SNERSCIC conducts research and contributes
                          data to the National Spinal Cord Injury Statistical Center. SNERSCIC conducts center-specific
                          research (at its own center), and also conducts multi-center or module studies with other SCIMS
                          Centers. SNERSCIC studies in 2016-2021 include:
                        </p>
                      </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <div id="accordion" class="panel-group">

                        <!--Panel 1-->
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title">
                              <a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapseOne">National Spinal Cord Injury Database</a>
                            </h4>
                          </div>
                          <div id="collapseOne" class="panel-collapse collapse">
                            <div class="panel-body">
                              <p>SNERSCIC contributes data to the National Spinal Cord Injury Database, which includes more than 32,000 individuals with SCI.  Information is gathered from individuals, with their consent, during hospitalization and through follow up  at one and five years after injury, and every five years thereafter. Data from the SCIMS National Database provides information about the course of recovery, trends in cause and severity of SCI, health service delivery and costs, treatment, and rehabilitation outcomes. The database is a rich source of data for research and analysis of outcomes.</p>
                            </div>
                          </div>
                        </div>

                        <!--Panel 2-->
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title">
                              <a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapseTwo">Development and Validation of the SCI – Functional Index / Assistive Technology (SCI-FI/AT) Inpatient Short Forms</a>
                            </h4>
                          </div>
                          <div id="collapseTwo" class="panel-collapse collapse">
                            <div class="panel-body">
                              <p>The SCI-FI instrument was designed specifically for persons with SCI, regardless of level or severity of injury. The content of the SCI-FI is based upon an extensive review of the literature, input from patients with SCI, and clinician focus groups, and is grounded within the International Classification of Function, Disability, and Health (ICF) framework.</p>
                              <p>With the development of the SCI-FI/AT, we now have available SCI-FI scales that evaluate a person’s ability to function using adaptive technology in the areas of Basic Mobility, Self-Care, Ambulation, Fine Motor, and Wheelchair function. There are 178 total items in the final SCI- FI/AT domain item banks. Short forms are aptly named as a short paper-based version of the full item banks of each functional domain of the SCI-FI/AT, which is currently administered via computer as a computer adaptive test (CAT).</p>
                              <p><strong>Aim 1 -</strong>Create a SCI-FI/AT inpatient short form that can be completed efficiently by clinicians on patients while they are in the inpatient rehabilitation setting.  This form can be used as part of the SCI Model Systems (SCIMS) National Database inpatient assessment, and for general use by clinicians and researchers.</p>
                              <p><strong>Aim 2 -</strong> We will conduct a pilot study to evaluate the inter-rater reliability of having clinicians complete the SCI-FI/AT inpatient short forms with a sample of patients in the SNERSCIC SCIMS network.</p>
                            </div>
                          </div>
                        </div>

                        <!--Panel 3-->
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapseThree">Equity and Quality in Assistive Technology for Individuals with SCI 2.0 (EQuATe)</a>
                            </h4>
                          </div>
                          <div id="collapseThree" class="panel-collapse collapse">
                            <div class="panel-body">
                              <p><strong>Lead Principal Investigator: Dr. Mike Boninger, UPitt</strong>
                                <br>These tables are abstracted from the clinical trial registration website at <a href="http://www.scope-sci.org/" target="_blank">www.clinicaltrials.gov</a>
                                using the search term “Spinal Cord Injury.”  The most recent update occurred December 5, 2016, at which time the search found a total of 831 SCI trials.
                                Of these, the status of 271 trials was known. The trials listed in these tables are currently actively recruiting or soon-to-be recruiting subjects.</p>
                                <p><strong>Aim 1 -</strong> Collect descriptive data on Assistive Technology (AT) used by individuals with Spinal Cord Injury (SCI)
                                  including wheelchair make, model, failures, and repairs. This data will enable us to track the impact of insurance market changes,
                                  find differences in wheelchair failures, and present this information to wheelchair users.
                                </p>
								<p><strong>Hypothesis:</strong></p>
								<ol>
                                  <li>The prevalence and type of wheelchair failures will vary across manufacturers.</li>
                                  <li>Device satisfaction will be inversely correlated with adverse consequences.</li>
                                  <li>Response in service time for repairs will vary by vendor and service region.</li>
                                  <li>Users will experience additional consequences secondary to waiting for a repair to be completed.</li>
                                  <li>Intensity of wheelchair use will be associated with incidence of repairs.</li>
                                </ol>
                                <p><strong>Aim 2 -</strong> Explore the influence of individual characteristics such as financial strain, educational quality,
                                  health literacy, geographic location, and self-efficacy on differences in AT quality and other health related outcomes
                                  for individuals with SCI.
                                </p>
                            </div>
                          </div>
                        </div>


                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
            <!--End Tab 2-->

            <!--Start Tab 3-->
            <div id="tabs-3">
              <div class="tab-pane" id="quick-reviews-of-snerscic-research">
                <div class="research-snerscic-content">
                  <div class="row">

                    <div class="col-lg-12">
                      <div class="review-text">
                        <p>These are quick, easy-to-understand summaries of research we have done in recent years, without all the research mumbo-jumbo. Enjoy! And please <a href="<?php echo home_url(); ?>/contact-us/" target="_blank">contact us</a> with any questions! You can also find all the SCI Model Systems Research Quick Reviews <a href="http://www.msktc.org/sci/Quick-Reviews" target="_blank">here.</a></p>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                      <div class="research-boxes quickreview-boxes">
                        <img src="<?php bloginfo('url')?>/media/quicklink-icon1.png" alt="Randomized Trial of a Telephone Intervention">
                          <h3 data-mh="quick-review">Randomized Trial of a Telephone Intervention</h3>
                          <a href="http://www.msktc.org/lib/docs/Quick_Reviews/SCI_Randomized_Trial.pdf" target="_blank" title="Learn More" class="btn blue-btn">Learn More</a>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                        <div class="research-boxes quickreview-boxes">
                          <img src="<?php bloginfo('url')?>/media/quicklink-icon2.png" alt="Care Call:  A First Look at a Telephone Program for Persons with Spinal Cord Injury">
                            <h3 data-mh="quick-review">Care Call:  A First Look at a Telephone Program for Persons with Spinal Cord Injury</h3>
                            <a href="http://www.msktc.org/lib/docs/Quick_Reviews/SCI_Telehealth_Intervention_.pdf" target="_blank" title="Learn More" class="btn blue-btn">Learn More</a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                        <div class="research-boxes quickreview-boxes">
                          <img src="<?php bloginfo('url')?>/media/quicklink-icon3.png" alt="Differential Impact and Use of a Telehealth Intervention by Persons with MS or SCI">
                            <h3 data-mh="quick-review">Differential Impact and Use of a Telehealth Intervention by Persons with MS or SCI</h3>
                            <a href="http://www.msktc.org/lib/docs/Quick_Reviews/SCI_Telehealth_Intervention_.pdf" target="_blank" title="Learn More" class="btn blue-btn">Learn More</a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                      <div class="research-boxes quickreview-boxes">
                        <img src="<?php bloginfo('url')?>/media/quicklink-icon4.png" alt="Development and Initial Evaluation of the SCI-FI/AT">
                          <h3 data-mh="quick-review">Development and Initial Evaluation of the SCI-FI/AT</h3>
                          <a href="http://www.msktc.org/lib/docs/Quick_Reviews/SCI_Development_SCI-FIAT.pdf" target="_blank" title="Learn More" class="btn blue-btn">Learn More</a>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                      <div class="research-boxes quickreview-boxes">
                        <img src="<?php bloginfo('url')?>/media/quicklink-icon5.png" alt="Spinal Cord Injury-Functional Index: Item Banks to Measure Physical Functioning in Individuals with Spinal Cord Injury">
                          <h3 data-mh="quick-review">Spinal Cord Injury-Functional Index: Item Banks to Measure Physical Functioning in Individuals with Spinal Cord Injury</h3>
                          <a href="http://www.msktc.org/lib/docs/Quick_Reviews/SCI_Functional_Index.pdf" target="_blank" title="Learn More" class="btn blue-btn">Learn More</a>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                      <div class="research-boxes quickreview-boxes">
                        <img src="<?php bloginfo('url')?>/media/quicklink-icon6.png" alt="Biomarker of Lower Extremity Bone Density">
                          <h3 data-mh="quick-review">Biomarker of Lower Extremity Bone Density</h3>
                          <a href="http://www.msktc.org/lib/docs/Quick_Reviews/SCI_Adiponectin_Candidate_Biomarker.pdf" target="_blank" title="Learn More" class="btn blue-btn">Learn More</a>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                      <div class="research-boxes quickreview-boxes">
                        <img src="<?php bloginfo('url')?>/media/quicklink-icon7.png" alt="Measuring Depression After Spinal Cord Injury">
                          <h3 data-mh="quick-review">Measuring Depression After Spinal Cord Injury</h3>
                          <a href="http://www.msktc.org/lib/docs/Quick_Reviews/SCI_Measuring_Depression.pdf" target="_blank" title="Learn More" class="btn blue-btn">Learn More</a>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                      <div class="research-boxes quickreview-boxes">
                        <img src="<?php bloginfo('url')?>/media/quicklink-icon8.png" alt="Physical Activity Measures and Their Association with Depression and Satisfaction">
                          <h3 data-mh="quick-review">Physical Activity Measures and Their Association with Depression and Satisfaction</h3>
                          <a href="http://www.msktc.org/lib/docs/Quick_Reviews/SCI_Objective_and_self-reported_measures_of_depression.pdf" target="_blank" title="Learn More" class="btn blue-btn">Learn More</a>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                      <div class="research-boxes quickreview-boxes">
                        <img src="<?php bloginfo('url')?>/media/quicklink-icon9.png" alt="Tracking Functional Status across the SCI Lifespan">
                          <h3 data-mh="quick-review">Tracking Functional Status across the SCI Lifespan</h3>
                          <a href="http://www.msktc.org/lib/docs/Quick_Reviews/SCI_Tracking_Functional_Status.pdf" target="_blank" title="Learn More" class="btn blue-btn">Learn More</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!--End Tab 3-->

            <!--Start Tab 4-->
            <div id="tabs-4">
              <div class="tab-pane" id="research-publications">
                <div class="research-snerscic-content">
                  <div class="row">

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <div class="publication-content">
                        <h2>Publications</h2>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. <a href="#" title="Lorem Ipsum has been the industry's">Lorem Ipsum has been the industry's</a> <img src="<?php bloginfo('url'); ?>/media/pdf-icon.png" alt=""/> <em>standard dummy text ever since the 1500s, when an unknown printer</em> took a galley of type and scrambled it to make a type specimen book.</p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. <a href="#" title="Lorem Ipsum has been the industry's">Lorem Ipsum has been the industry's</a> <img src="<?php bloginfo('url'); ?>/media/pdf-icon.png" alt=""/> <em>standard dummy text ever since the 1500s, when an unknown printer</em> took a galley of type and scrambled it to make a type specimen book.</p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. <a href="#" title="Lorem Ipsum has been the industry's">Lorem Ipsum has been the industry's</a> <img src="<?php bloginfo('url'); ?>/media/pdf-icon.png" alt=""/> <em>standard dummy text ever since the 1500s, when an unknown printer</em> took a galley of type and scrambled it to make a type specimen book.</p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. <a href="#" title="Lorem Ipsum has been the industry's">Lorem Ipsum has been the industry's</a> <img src="<?php bloginfo('url'); ?>/media/pdf-icon.png" alt=""/> <em>standard dummy text ever since the 1500s, when an unknown printer</em> took a galley of type and scrambled it to make a type specimen book.</p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. <a href="#" title="Lorem Ipsum has been the industry's">Lorem Ipsum has been the industry's</a> <img src="<?php bloginfo('url'); ?>/media/pdf-icon.png" alt=""/> <em>standard dummy text ever since the 1500s, when an unknown printer</em> took a galley of type and scrambled it to make a type specimen book.</p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. <a href="#" title="Lorem Ipsum has been the industry's">Lorem Ipsum has been the industry's</a> <img src="<?php bloginfo('url'); ?>/media/pdf-icon.png" alt=""/> <em>standard dummy text ever since the 1500s, when an unknown printer</em> took a galley of type and scrambled it to make a type specimen book.</p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. <a href="#" title="Lorem Ipsum has been the industry's">Lorem Ipsum has been the industry's</a> <img src="<?php bloginfo('url'); ?>/media/pdf-icon.png" alt=""/> <em>standard dummy text ever since the 1500s, when an unknown printer</em> took a galley of type and scrambled it to make a type specimen book.</p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. <a href="#" title="Lorem Ipsum has been the industry's">Lorem Ipsum has been the industry's</a> <img src="<?php bloginfo('url'); ?>/media/pdf-icon.png" alt=""/> <em>standard dummy text ever since the 1500s, when an unknown printer</em> took a galley of type and scrambled it to make a type specimen book.</p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. <a href="#" title="Lorem Ipsum has been the industry's">Lorem Ipsum has been the industry's</a> <img src="<?php bloginfo('url'); ?>/media/pdf-icon.png" alt=""/> <em>standard dummy text ever since the 1500s, when an unknown printer</em> took a galley of type and scrambled it to make a type specimen book.</p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. <a href="#" title="Lorem Ipsum has been the industry's">Lorem Ipsum has been the industry's</a> <img src="<?php bloginfo('url'); ?>/media/pdf-icon.png" alt=""/> <em>standard dummy text ever since the 1500s, when an unknown printer</em> took a galley of type and scrambled it to make a type specimen book.</p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. <a href="#" title="Lorem Ipsum has been the industry's">Lorem Ipsum has been the industry's</a> <img src="<?php bloginfo('url'); ?>/media/pdf-icon.png" alt=""/> <em>standard dummy text ever since the 1500s, when an unknown printer</em> took a galley of type and scrambled it to make a type specimen book.</p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. <a href="#" title="Lorem Ipsum has been the industry's">Lorem Ipsum has been the industry's</a> <img src="<?php bloginfo('url'); ?>/media/pdf-icon.png" alt=""/> <em>standard dummy text ever since the 1500s, when an unknown printer</em> took a galley of type and scrambled it to make a type specimen book.</p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. <a href="#" title="Lorem Ipsum has been the industry's">Lorem Ipsum has been the industry's</a> <img src="<?php bloginfo('url'); ?>/media/pdf-icon.png" alt=""/> <em>standard dummy text ever since the 1500s, when an unknown printer</em> took a galley of type and scrambled it to make a type specimen book.</p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. <a href="#" title="Lorem Ipsum has been the industry's">Lorem Ipsum has been the industry's</a> <img src="<?php bloginfo('url'); ?>/media/pdf-icon.png" alt=""/> <em>standard dummy text ever since the 1500s, when an unknown printer</em> took a galley of type and scrambled it to make a type specimen book.</p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. <a href="#" title="Lorem Ipsum has been the industry's">Lorem Ipsum has been the industry's</a> <img src="<?php bloginfo('url'); ?>/media/pdf-icon.png" alt=""/> <em>standard dummy text ever since the 1500s, when an unknown printer</em> took a galley of type and scrambled it to make a type specimen book.</p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. <a href="#" title="Lorem Ipsum has been the industry's">Lorem Ipsum has been the industry's</a> <img src="<?php bloginfo('url'); ?>/media/pdf-icon.png" alt=""/> <em>standard dummy text ever since the 1500s, when an unknown printer</em> took a galley of type and scrambled it to make a type specimen book.</p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. <a href="#" title="Lorem Ipsum has been the industry's">Lorem Ipsum has been the industry's</a> <img src="<?php bloginfo('url'); ?>/media/pdf-icon.png" alt=""/> <em>standard dummy text ever since the 1500s, when an unknown printer</em> took a galley of type and scrambled it to make a type specimen book.</p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. <a href="#" title="Lorem Ipsum has been the industry's">Lorem Ipsum has been the industry's</a> <img src="<?php bloginfo('url'); ?>/media/pdf-icon.png" alt=""/> <em>standard dummy text ever since the 1500s, when an unknown printer</em> took a galley of type and scrambled it to make a type specimen book.</p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. <a href="#" title="Lorem Ipsum has been the industry's">Lorem Ipsum has been the industry's</a> <img src="<?php bloginfo('url'); ?>/media/pdf-icon.png" alt=""/> <em>standard dummy text ever since the 1500s, when an unknown printer</em> took a galley of type and scrambled it to make a type specimen book.</p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. <a href="#" title="Lorem Ipsum has been the industry's">Lorem Ipsum has been the industry's</a> <img src="<?php bloginfo('url'); ?>/media/pdf-icon.png" alt=""/> <em>standard dummy text ever since the 1500s, when an unknown printer</em> took a galley of type and scrambled it to make a type specimen book.</p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. <a href="#" title="Lorem Ipsum has been the industry's">Lorem Ipsum has been the industry's</a> <img src="<?php bloginfo('url'); ?>/media/pdf-icon.png" alt=""/> <em>standard dummy text ever since the 1500s, when an unknown printer</em> took a galley of type and scrambled it to make a type specimen book.</p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. <a href="#" title="Lorem Ipsum has been the industry's">Lorem Ipsum has been the industry's</a> <img src="<?php bloginfo('url'); ?>/media/pdf-icon.png" alt=""/> <em>standard dummy text ever since the 1500s, when an unknown printer</em> took a galley of type and scrambled it to make a type specimen book.</p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. <a href="#" title="Lorem Ipsum has been the industry's">Lorem Ipsum has been the industry's</a> <img src="<?php bloginfo('url'); ?>/media/pdf-icon.png" alt=""/> <em>standard dummy text ever since the 1500s, when an unknown printer</em> took a galley of type and scrambled it to make a type specimen book.</p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. <a href="#" title="Lorem Ipsum has been the industry's">Lorem Ipsum has been the industry's</a> <img src="<?php bloginfo('url'); ?>/media/pdf-icon.png" alt=""/> <em>standard dummy text ever since the 1500s, when an unknown printer</em> took a galley of type and scrambled it to make a type specimen book.</p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. <a href="#" title="Lorem Ipsum has been the industry's">Lorem Ipsum has been the industry's</a> <img src="<?php bloginfo('url'); ?>/media/pdf-icon.png" alt=""/> <em>standard dummy text ever since the 1500s, when an unknown printer</em> took a galley of type and scrambled it to make a type specimen book.</p>
                        <div class="share-email">
                          <strong>Share this:</strong>
                          <a href="mailto:" title="Email">Email</a>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
            <!--End Tab 4-->

            <!--Start Tab 5-->
            <div id="tabs-5">
              <div class="tab-pane" id="research-presentations">
                <div class="research-snerscic-content">
                  <div class="row">

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <div class="presentation-main">

                        <div class="row">
                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">
                            <div class="sort-by">
                              <label>Sort by:</label>
                              <select>
                                <option value="">Newest</option>
                                <option value="">Newest1</option>
                                <option value="">Newest2</option>
                              </select>
                            </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="presentation-box">
                              <figure>
                                <a href="#" title="Efficacy of “Care Call” Telerehabilitation Intervention for Persons with Spinal Cord Dysfunction: Randomized Controlled Trial"><img src="<?php bloginfo('url'); ?>/media/presentation-news-img.jpg" alt="" /></a>
                              </figure>
                              <div class="presentation-content">
                                <h3><a href="#" title="Efficacy of “Care Call” Telerehabilitation Intervention for Persons with Spinal Cord Dysfunction: Randomized Controlled Trial">Efficacy of “Care Call” Telerehabilitation Intervention for Persons with Spinal Cord Dysfunction: Randomized Controlled Trial.</a></h3>
                                <p>Houlihan B, Jette A, Pengsheng N, Paasche-Orlow M, Friedman R, Ducharme S, Wierbicky J, Zazula J, Rosenblum D, Williams S.</p>
                                <p><em>Presented at the 2011 ACRM-ASNR Annual Conference, Atlanta, GA, Oct 11-15, 2011.</em></p>
                              </div>
                            </div>
                          </div>
                          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="presentation-box">
                              <figure>
                                <a href="#" title="Engaging Consumers in Community Research and Education"><img src="<?php bloginfo('url'); ?>/media/presentation-news-img1.jpg" alt="" /></a>
                              </figure>
                              <div class="presentation-content">
                                <h3><a href="#" title="Engaging Consumers in Community Research and Education">Engaging Consumers in Community Research and Education.</a></h3>
                                <p>Zazula J, Pernigotti D, Houlihan B, DeJoie C, Rosenblum D, Williams K, Williams S.</p>
                                <p><em>Presented at Academy of Spinal Cord Injury Professionals Annual Meeting, Las Vegas, NV September 5-7, 2011.</em></p>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="presentation-box">
                              <figure>
                                <a href="#" title="Using the Internet as a Treatment Modality for Individuals with SCI"><img src="<?php bloginfo('url'); ?>/media/presentation-news-img2.jpg" alt="" /></a>
                              </figure>
                              <div class="presentation-content">
                                <h3><a href="#" title="Using the Internet as a Treatment Modality for Individuals with SCI">Using the Internet as a Treatment Modality for Individuals with SCI.</a></h3>
                                <p>DeJoie, C, Houlihan B, Williams S.</p>
                                <p><em>Presented at American Spinal Cord Injury Association Annual Meeting 2010, Nashville, TN 5/26-5/28/10.</em></p>
                              </div>
                            </div>
                          </div>
                          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="presentation-box">
                              <figure>
                                <a href="#" title="Development of a Novel Automated Telehealth System for People with Spinal Cord Dysfunction"><img src="<?php bloginfo('url'); ?>/media/presentation-news-img3.jpg" alt="" /></a>
                              </figure>
                              <div class="presentation-content">
                                <h3><a href="#" title="Development of a Novel Automated Telehealth System for People with Spinal Cord Dysfunction">Development of a Novel Automated Telehealth System for People with Spinal Cord Dysfunction.</a></h3>
                                <p>Houlihan B, Jette A, Paasche-Orlow M, Wierbicky J, Zazula J, Dicker K, Cuevas P, Ducharme S, Williams S.</p>
                                <p><em>presented at 2009 SCI Contemporary Forums conference March 18-20, Orlanda, FL; 2009</em></p>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="presentation-box">
                              <figure>
                                <a href="#" title="Use of the Internet with SCI"><img src="<?php bloginfo('url'); ?>/media/presentation-news-img4.jpg" alt="" /></a>
                              </figure>
                              <div class="presentation-content">
                                <h3><a href="#" title="Use of the Internet with SCI">Use of the Internet with SCI.</a></h3>
                                <p>Houlihan B.</p>
                                <p><em>presented at SCI Contemporary Forums, Boston, MA, March 2007.</em></p>
                              </div>
                            </div>
                          </div>
                          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="presentation-box">
                              <figure>
                                <a href="#" title="Computer &amp; Internet Use by Individuals after a Traumatic Spinal Cord Injury"><img src="<?php bloginfo('url'); ?>/media/presentation-news-img5.jpg" alt="" /></a>
                              </figure>
                              <div class="presentation-content">
                                <h3><a href="#" title="Computer &amp; Internet Use by Individuals after a Traumatic Spinal Cord Injury">Computer &amp; Internet Use by Individuals after a Traumatic Spinal Cord Injury.</a></h3>
                                <p>Jette A, Goodman N, Houlihan B, Williams S.</p>
                                <p><em>presented at Global Spinal Cord Injury Conference,Reykjavik, Iceland July 27- July 1, 2007</em></p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
            <!--End Tab 5-->

          </div>
        </div>
      </div>
    </div>
  </div>
  </section>
</div>
</div><!--End of Tabs-->
  <!-- Mid Container Ends Here -->
