<!-- Quick Links Starts Here -->
<section class="quick-links-sec">
  <div class="container">
    <div class="row">

      <?php 
        $i = 0; while(have_rows('quick_links_top', 5)): the_row(); $top_block = get_sub_field('image');
      ?>
      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 home-block" style="background: #<?php echo ($i == 1) ? '02459f': '003070'; ?>;">
        <div class="top-block-container">
          <?php if(get_sub_field('link')): ?><a href="<?php the_sub_field('link'); ?>"><?php endif; ?>
          <img class="lazy" src="<?php bloginfo('url'); ?>/media/spinner.gif" alt="<?php echo $top_block['alt']; ?>" data-src="<?php echo $top_block['url']; ?>" />
          <?php the_sub_field('quick_content'); ?>
          <?php if(get_sub_field('link')): ?></a><?php endif; ?>
        </div>
      </div>
      <?php $i++; endwhile;?>

    </div>
  </div>
</section>
<!-- Quick Links Ends Here -->

<!-- Mid Container Starts Here -->
<section class="mid-container home-mid-container">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="title-heading">
          <h2>Videos</h2>
        </div>
      </div>
      <div class="col-lg-8 col-md-7 col-sm-12">
        <?php 
          $i = 0; while(have_rows('video_home', 5)): the_row();
        ?>
        <div class="row videos-lists">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3><?php the_sub_field('heading'); ?></h3>
          </div>
          <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
            <div class="iframe-video">
              <iframe src="<?php the_sub_field('embed_link'); ?>" allowfullscreen></iframe>
            </div>
          </div>
          <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
            <div class="video-content">
              <?php the_sub_field('video_content'); ?>
            </div>
          </div>
        </div>
        <?php $i++; endwhile; ?>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 more-videos">
            <a href="<?php echo home_url(); ?>/sci-education/kim-online-videos/" title="View More Videos">View More Videos</a>
        </div>

      </div><!--End of Left Content-->

      <!--Start Sidebar-->
      <div class="col-lg-4 col-md-5 col-sm-12">
        <aside class="sidebar">
          <section>
            <h2>SNERSCIC Network Sites</h2>
            <ul>
              <li><a href="http://spauldingrehab.org/conditions-and-treatments/spinal-cord-injury-rehabilitation" target="_blank" title="Spaulding Rehabilitation Hospital">Spaulding Rehabilitation Hospital</a></li>
              <li><a href="https://www.gaylord.org/Home/OurServices/Rehabilitation/SpinalCordInjuryProgram" target="_blank" title="Gaylord Specialty Healthcare, CT">Gaylord Specialty Healthcare, CT</a></li>
            </ul>
          </section>
          <section>
            <?php the_field('save_the_date', 5, false, false); ?>
          </section>
          <section>
            <h2>Connect On Twitter</h2>
            <?php echo do_shortcode('[custom-twitter-feeds]'); ?>
          </section>
        </aside>
      </div>
      <!--End of Sidebar-->

    </div>
  </div>
</section>
<!-- Mid Container Ends Here -->
