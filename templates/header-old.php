<!--Header Starts here -->
<header id="header" class="banner">
  <?php 
    $language = get_field('language_img', 'option');
    $spaulding = get_field('spaulding_img', 'option');
    $gaylord = get_field('gaylord_img', 'option');
    $logo = get_field('site_logo', 'option');
  ?>
  <div class="top-nav">
    <button onclick="topFunction()" id="backToTop" title="Go to top"><i class="fa fa-caret-up" aria-hidden="true"></i></button>
    <div class="container">
      <div class="row">
        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 cf">
          <div class="translate-bix">
            <script type="text/javascript">
            function googleTranslateElementInit() {
              new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
            }
            </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
            <div id="google-translator">
              <span class="up_bold">Translate:</span>
              <a href="#" title="Translate Page" class="btn-translate">
                <img class="lazy" src="<?php bloginfo('url'); ?>/media/spinner.gif" alt="<?php echo $language['alt']; ?>" data-src="<?php echo $language['url']; ?>" />
              </a>
            <div id="google_translate_element"></div></div>
            <div class="text-size-list">
              <p><span class="up_bold">Text Size: </span>
              <a id="decrease-font" href="#">A- </a>
              <a id="increase-font" href="#">A+ </a>
              </p>
            </div>
          </div>
        </div>
        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 cf">
          <ul class="top-nav-list">
            <li><a href="<?php the_field('spaulding_link', 'option'); ?>" target="_blank"><img class="lazy" src="<?php bloginfo('url'); ?>/media/spinner.gif" alt="<?php echo $spaulding['alt']; ?>" data-src="<?php echo $spaulding['url']; ?>" ></a></li>
            <li><a href="https://www.facebook.com/SpauldingRehab" target="_blank" title="Facebook" class="fa fa-facebook"></a></li>
            <li><a href="https://twitter.com/SpauldingSCIMS" target="_blank" title="Twitter" class="fa fa-twitter"></a></li>
            <li><a href="<?php the_field('gaylord_link', 'option'); ?>" target="_blank"><img class="lazy" src="<?php bloginfo('url'); ?>/media/spinner.gif" alt="<?php echo $gaylord['alt']; ?>" data-src="<?php echo $gaylord['url']; ?>" ></a></li>
            <li><a href="https://www.facebook.com/gaylordspecialtyhealthcare" target="_blank" title="Facebook" class="fa fa-facebook"></a></li>
            <li><a href="https://twitter.com/GaylordHealth" target="_blank" title="Twitter" class="fa fa-twitter"></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <div class="nav-center">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cf">
          <a href="<?php echo home_url(); ?>" title="Spaulding New England Regional Spinal Cord Injury Center" class="logo">
          <img class="lazy" src="<?php bloginfo('url'); ?>/media/spinner.gif" alt="Spaulding New England Regional Spinal Cord Injury Center" data-src="<?php echo $logo['url']; ?>"></a>
          <div class="nav-right">
            <div class="search-form">
              <!--Add sage search form-->
              <?php get_search_form(); ?>
              <!-- <form>
                <fieldset>
                  <input type="search" placeholder="Search..." />
                </fieldset>
                <button type="submit">Submit</button>
              </form> -->
              <!--Add sage search form-->
            </div>
            <div class="nav-icons">
              <ul class="top-nav-list">
                <li><a href="<?php echo home_url(); ?>/sci-education/new-to-sci/" title="New To SCI?"><span class="fa fa-question"></span>&nbsp;<?php the_field('question_heading', 'option'); ?></a></li>
                <li><a href="#" data-toggle="modal" data-target="#mailinglist-signup" title="Our Mailing List"><span class="fa fa-envelope"></span>&nbsp;<?php the_field('envelope_heading', 'option'); ?></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="main-nav navbar navbar-default navbar-static-top">
    <div class="container">
      <div class="row">
        <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cf"> -->
         <!--  <nav class="navbar navbar-default"> -->
<!--             <div class="container-fluid"> -->
              <div class="navbar-header">
        <button class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse" aria-expanded="false">
                <!--   <a class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse" aria-expanded="false"> -->
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
              </div>
              <!-- Collect the nav links, forms, and other content for toggling -->
              <div id="nav-wrapper">
                <?php
                  wp_nav_menu( array(
                  'menu'              => 'primary_navigation',
                  'theme_location'    => 'primary_navigation',
                  'depth'             => 2,
                  'container'         => 'nav',
                  'container_class'   => 'collapse navbar-collapse nav-justified',
                  'menu_class'        => 'nav nav-justified',
                  'container_id'      => 'menu-primary-navigation-top',
                  'fallback_cb'       => 'custom_navwalker::fallback',
                  'walker'            => new custom_navwalker())
                  );
                ?>
              </div>
            <!-- </div> -->
<!--           </nav> -->
        <!-- </div> -->
      </div>
    </div>
  </div>

</header>
<!-- Header Ends here-->

<div class="modal fade" id="mailinglist-signup" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="vertical-alignment-helper">
    <div class="modal-dialog modal-lg vertical-align-center">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
           <h3 class="modal-title" id="myModalLabel">Join SNERSCIC Mailing List</h3>
        </div>
        <div class="modal-body">
          <?php echo do_shortcode('[gravityform id="2" title="false" description="false" ajax="true"]'); ?>
        </div>
      </div>
    </div>
  </div>
</div>
