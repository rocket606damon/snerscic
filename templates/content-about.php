<!-- Banner Starts Here -->
<section class="inner-banner about-banner lazy" data-src="<?php bloginfo('url'); ?>/media/about-banner-1.jpg">
  <div class="tbl">
    <div class="tbl-cell-bottom">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 col-sm-12">
            <h1>About</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Banner Ends Here -->

<!-- Mid Content Starts Here -->
<section class="mid-content">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
        <?php the_content(); ?>
      </div>
    </div>
  </div>
</section>
<!-- Mid Content Starts Here -->

<!--Start of Tab-->
<div id="tabs">
<!-- Page Links Starts Here -->
<section class="page-links about-nav" id="about-nav">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
        <ul class="links-listing">

          <li class="about-tabs about-tabs-1 li-clicker">
            <a href="#tabs-1" class="tab-clicker" title="About">
              <div class="tbl">
                <div class="tbl-cell">
                  <span>About</span>
                </div>
              </div>
            </a>
          </li>

          <li class="about-tabs about-tabs-2 li-clicker">
            <a href="#tabs-2" class="tab-clicker" title="Our Team">
              <div class="tbl">
                <div class="tbl-cell">
                  <span>Our Team</span>
                </div>
              </div>
            </a>
          </li>

          <li class="about-tabs about-tabs-3 li-clicker">
            <a href="#tabs-3" class="tab-clicker" title="Spinal Cord Injury Model Systems Program">
              <div class="tbl">
                <div class="tbl-cell">
                  <span>Spinal Cord Injury Model Systems Program</span>
                </div>
              </div>
            </a>
          </li>

          <li class="about-tabs about-tabs-4 li-clicker">
            <a href="#tabs-4" class="tab-clicker" title="SNERSCIC Network Sites">
              <div class="tbl">
                <div class="tbl-cell">
                  <span>SNERSCIC Network Sites</span>
                </div>
              </div>
            </a>
          </li>

          <li class="about-tabs about-tabs-5 li-clicker">
            <a href="#tabs-5" class="tab-clicker" title="Snerscic Community Advisory Boards">
              <div class="tbl">
                <div class="tbl-cell">
                  <span>Snerscic Community Advisory Boards</span>
                </div>
              </div>
            </a>
          </li>

        </ul>
      </div>
    </div>
  </div>
</section>
<!-- Page Links Ends Here -->

<!-- Mid Container Starts Here -->
<div id="tab-container" class="tab-container about-page">
  <section>
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="tab-content">

            <!--Start Tab 1-->
            <div id="tabs-1">
              <div class="tab-pane" id="about">
                <div class="about-snerscic-content">
                  <div class="row">
                    <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1 text-center">

                      <?php 
                        $about_snerscic = get_field('about_snerscic_img', 119);
                      ?>

                      <img class="lazy" src="<?php bloginfo('url'); ?>/media/spinner.gif" alt="<?php echo $about_snerscic['alt']; ?>" data-src="<?php echo $about_snerscic['url']; ?>" />

                      <div class="about-inside">
                        <?php the_field('about_inside', 119); ?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!--End of Tab 1-->

            <!--Start of Tab 2-->
            <div id="tabs-2">
              <div class="tab-pane" id="our-team">
                <div class="about-snerscic-content">
                  <div class="row">

                  <?php if(have_rows('our_team', 119)): $i = 0;
                  while (have_rows('our_team', 119)) : $i++; the_row();
                  
                  ?>

                  <?php
                      $modal = get_sub_field('name');
                      $modal = preg_replace("/[^A-Za-z0-9 ]/", '', $modal);
                      $modal = str_replace(" ", "-", $modal);
                      $modal = strtolower($modal);

                      $photo = get_sub_field('photo');

                      //echo $modal;
                  ?>
                  <!--Team Member -->
                  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="about-boxes">
                      <img class="lazy" src="<?php bloginfo('url'); ?>/media/spinner.gif" alt="<?php echo $photo['alt']; ?>" data-src="<?php echo $photo['url']; ?>" />
                      <h3><?php the_sub_field('name'); ?></h3>
                      <p data-mh="team"><?php the_sub_field('title'); ?></p>
                      <a href="#" data-toggle="modal" data-target="#<?php echo $modal; ?>" title="View Bio" class="btn blue-btn">View Bio</a>
                    </div>
                  </div>
                  <!--Modal -->
                  <div class="modal fade" id="<?php echo $modal; ?>" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                          <h4 id="largeModal-t2-1" class="modal-title" ><?php the_sub_field('name'); ?> - <?php the_sub_field('title'); ?></h4>
                        </div>
                        <div class="modal-body">
                          <div class="thumbnail left-align">
                            <img class="lazy" src="<?php bloginfo('url'); ?>/media/spinner.gif" alt="<?php echo $photo['alt']; ?>" data-src="<?php echo $photo['url']; ?>" />
                          </div>
                          <?php the_sub_field('bio'); ?>
                        </div>
                      </div>
                    </div>
                  </div>  
                  <?php endwhile; endif; ?>  

                  </div>
                </div>
              </div>
            </div>
            <!--End of Tab 2-->

            <!--Start of Tab 3-->
            <div id="tabs-3">
              <div class="tab-pane" id="spinal-cord-injury-model-systems-program">
                <div class="about-snerscic-content">
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="about-sci-systems-top">
                        <?php the_field('sci_systems_intro', 119); ?>
                      </div>
                    </div>

                    <?php if(have_rows('sci_systems', 119)): $i = 0;
                    while (have_rows('sci_systems', 119)) : $i++; the_row();

                    ?>
                    <?php
                        if(get_sub_field('toggle') == 'modal'){
                          $modal = get_sub_field('heading');
                          $modal = preg_replace("/[^A-Za-z0-9 ]/", '', $modal);
                          $modal = str_replace(" ", "-", $modal);
                          $modal = strtolower($modal);
                        }

                        $sci_image = get_sub_field('sci_image');
                        //echo $modal;
                    ?>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                      <div class="about-boxes">
                        <figure>
                          <img class="lazy" src="<?php bloginfo('url'); ?>/media/spinner.gif" alt="<?php echo $sci_image['alt']; ?>" data-src="<?php echo $sci_image['url']; ?>" />
                        </figure>
                        <h3><?php the_sub_field('heading'); ?></h3>
                        <p><?php the_sub_field('sub_heading'); ?></p>
                        <?php if(get_sub_field('toggle') == 'modal'): ?>
                        <a href="#" data-toggle="modal" data-target="#<?php echo $modal; ?>" class="btn blue-btn">More Info</a>
                        <?php else: ?>
                        <a href="<?php the_sub_field('link'); ?>" target="_blank" title="Visit Site" class="btn blue-btn">Visit Site</a>
                        <?php endif; ?>
                      </div>
                    </div>
                    <!--MODAL-->
                    <?php if(get_sub_field('toggle') == 'modal'): ?>
                    <div class="modal fade" id="<?php echo $modal; ?>" tabindex="-1" role="dialog" aria-labelledby="largeModal-t3-1" aria-hidden="true">
                      <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 id="largeModal-t3-1" class="modal-title" ><?php the_sub_field('heading'); ?>&nbsp;<?php the_sub_field('sub_heading'); ?></h4>
                          </div>
                          <div class="modal-body">
                            <?php the_sub_field('modal_body'); ?>
                          </div>
                        </div>
                      </div>
                    </div>
                    <?php endif; ?>

                    <?php endwhile; endif; ?>
                  </div>
                </div>
              </div>
            </div>
            <!--End of Tab 3-->

            <!--Start of Tab 4-->
            <div id="tabs-4">
              <div class="tab-pane" id="snerscic-network-sites">
                <div class="about-snerscic-content">
                  <div class="row">

                    <?php if(have_rows('network_sites', 119)): $i = 0;
                    while (have_rows('network_sites', 119)) : $i++; the_row();

                    ?>
                    <?php
                      $modal = get_sub_field('heading');
                      $modal = preg_replace("/[^A-Za-z0-9 ]/", '', $modal);
                      $modal = str_replace(" ", "-", $modal);
                      $modal = strtolower($modal);

                      $network_img = get_sub_field('network_img');
                      $modal_network_img = get_sub_field('modal_network_img');

                      //echo $modal;
                    ?>
                    <div class="<?php echo ($i == 1) ? 'col-lg-offset-2' : ''; ?> col-lg-4 col-md-4 col-sm-6 col-xs-12">
                      <div class="about-boxes">
                        <figure>
                          <img class="lazy" src="<?php bloginfo('url'); ?>/media/spinner.gif" alt="<?php echo $network_img['alt']; ?>" data-src="<?php echo $network_img['url']; ?>" />
                        </figure>
                        <h3><?php the_sub_field('heading'); ?></h3>
                        <p><?php the_sub_field('sub_heading'); ?></p>
                        <a href="#" data-toggle="modal" data-target="#<?php echo $modal; ?>" class="btn blue-btn">More Info</a>
                      </div>
                    </div>
                    <!--MODAL-->
                    <div class="modal fade" id="<?php echo $modal; ?>" tabindex="-1" role="dialog" aria-hidden="true">
                      <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" ><?php the_sub_field('heading'); ?></h4>
                          </div>
                          <div class="modal-body">
                            <img class="lazy" src="<?php bloginfo('url'); ?>/media/spinner.gif" alt="<?php echo $modal_network_img['alt']; ?>" data-src="<?php echo $modal_network_img['url']; ?>" />
                            <?php the_sub_field('modal_content'); ?>
                          </div>
                        </div>
                      </div>
                    </div>
                    <?php endwhile; endif; ?>
                  </div>
                </div>
              </div>
            </div>
            <!--End of Tab 4-->

            <!--Start of Tab 5-->
            <div id="tabs-5">
              <div class="tab-pane" id="snerscic-community-advisory-board">
                <div class="about-snerscic-content">
                  <div class="row">

                  <div class="col-lg-12">
                    <div class="about-sci-systems-top">
                      <?php the_field('advisory_boards_intro', 119); ?>
                    </div>
                  </div>                  

                  <?php if(have_rows('advisory_boards', 119)): $i = 0;
                  while (have_rows('advisory_boards', 119)) : $i++; the_row();
                  
                  ?>

                  <?php
                    $modal = get_sub_field('name');
                    $modal = preg_replace("/[^A-Za-z0-9 ]/", '', $modal);
                    $modal = str_replace(" ", "-", $modal);
                    $modal = strtolower($modal);

                    $photo = get_sub_field('photo');

                    //echo $modal;
                  ?>
                  <!--Advisory Board Member -->
                  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="about-boxes">
                      <img class="lazy" src="<?php bloginfo('url'); ?>/media/spinner.gif" alt="<?php echo $photo['alt']; ?>" data-src="<?php echo $photo['url']; ?>" />
                      <h3 data-mh="advisory"><?php the_sub_field('name'); ?></h3>
                      <a href="#" data-toggle="modal" data-target="#<?php echo $modal; ?>" title="View Bio" class="btn blue-btn">View Bio</a>
                    </div>
                  </div>
                  <!--Modal -->
                  <div class="modal fade" id="<?php echo $modal; ?>" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                          <h4 id="largeModal-t2-1" class="modal-title" ><?php the_sub_field('name'); ?></h4>
                        </div>
                        <div class="modal-body">
                          <div class="thumbnail left-align">
                            <img class="lazy" src="<?php bloginfo('url'); ?>/media/spinner.gif" alt="<?php echo $photo['alt']; ?>" data-src="<?php echo $photo['url']; ?>" />
                          </div>
                          <?php the_sub_field('bio'); ?>
                        </div>
                      </div>
                    </div>
                  </div>  
                  <?php endwhile; endif; ?>  

                  </div>
                </div>
              </div>
            </div>
            <!--End of Tab 5-->

          </div>
        </div>
      </div>
    </div>
  </div>
</section>
</div><!--End of Tab Container-->
<!-- Mid Container Ends Here -->
</div><!--End of Tabs-->

