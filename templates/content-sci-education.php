<!-- Banner Starts Here -->
<section class="inner-banner lazy" data-src="<?php bloginfo('url'); ?>/media/education-banner.png">
  <div class="tbl">
    <div class="tbl-cell-bottom">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 col-sm-12">
            <h1>SCI EDUCATION</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Banner Ends Here -->

<!-- Mid Content Starts Here -->
<section class="mid-content mid-content-blue">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
        <?php the_content(); ?>
      </div>
    </div>
  </div>
</section>
<!-- Mid Content Ends Here -->

<!-- Mid Page Links Starts Here -->
<div id="tabs" class="education-page">

<section class="page-links">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
        <ul class="links-listing">
          <li class="education-tabs-1" class="li-clicker">
            <a href="#tabs-1" class="tab-clicker" title="KiM LECTURE WITH LIVE WEBCASTS">
              <div class="tbl">
                <div class="tbl-cell">
                  <i class="fa fa-film" aria-hidden="true"></i>
                  <span>KiM LECTURE WITH LIVE WEBCASTS</span>
                </div>
              </div>
            </a>
          </li>
          <li class="education-tabs-2" class="li-clicker">
            <a href="#tabs-2" class="tab-clicker" title="Register for Next KiM Lecture/Webcast">
              <div class="tbl">
                <div class="tbl-cell">
                  <i class="fa fa-user-plus" aria-hidden="true"></i>
                  <span>REGISTER FOR NEXT KiM LECTURE/WEBCAST</span>
                </div>
              </div>
            </a>
          </li>
          <li class="education-tabs-3" class="li-clicker">
            <a href="#tabs-3" class="tab-clicker" title="Knowledge in Motion Online Videos">
              <div class="tbl">
                <div class="tbl-cell">
                  <i class="fa fa-video-camera" aria-hidden="true"></i>
                  <span>KiM ONLINE VIDEOS</span>
                </div>
              </div>
            </a>
          </li>
          <li class="education-tabs-4" class="li-clicker">
            <a href="#tabs-4" class="tab-clicker" title="Consumer Fact Sheets">
              <div class="tbl">
                <div class="tbl-cell">
                  <i class="fa fa-paperclip" aria-hidden="true"></i>
                  <span>CONSUMER FACT SHEETS</span>
                </div>
              </div>
            </a>
          </li>
          <li class="education-tabs-5" class="li-clicker">
            <a href="#tabs-5" class="tab-clicker" title="New to SCI?">
              <div class="tbl">
                <div class="tbl-cell">
                  <i class="fa fa-question" aria-hidden="true"></i>
                  <span>NEW TO SCI?</span>
                </div>
              </div>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>
<!-- Mid Page Links Ends Here -->

<!-- Mid Container Starts Here -->
<div id="tab-container" class="tab-container education-page">
  <section>
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="tab-content">

            <!--Start Tab 1-->
            <div id="tabs-1">
              <div class="tab-pane" id="kim-lecture-with-live-webcast">
                <div class="education-snerscic-content">
                  <div class="row">

                    <?php $live_webcast = get_field('lw_image', 123); ?>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <div class="currently-recruiting-content">
                        <div class="text-center">
                          <img class="lazy" src="<?php bloginfo('url'); ?>/media/spinner.gif" data-src="<?php echo $live_webcast['url']; ?>" alt="<?php echo $live_webcast['alt']; ?>" />
                        </div>
                        <?php the_field('lw_content', 123); ?>
                        <div class="text-center">
                          <button class="btn past-btn"><a href="<?php the_field('lw_link_1'); ?>"><?php the_field('lw_text_1', 123); ?></a></button>
                          <button class="btn upcoming-btn"><a href="<?php the_field('lw_link_2'); ?>" ><?php the_field('lw_text_2', 123); ?></a></button>
                          <p><a class="mailing-list" href="#" data-toggle="modal" data-target="#mailinglist-signup">Join our Mailing List</a> and get notifications and info sent directly to your inbox.</p>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
            <!--End Tab 1-->

            <!--Start Tab 2-->
            <div id="tabs-2">
              <div class="tab-pane" id="register-for-next-kim-lecturewebcast">
                <div class="education-snerscic-content">
                  <div class="row">

                      <?php $register_next = get_field('rn_image', 123); ?>

                      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="text-center">
                          <img class="lazy" src="<?php bloginfo('url'); ?>/media/spinner.gif" data-src="<?php echo $register_next['url']; ?>" alt="<?php echo $register_next['alt']; ?>" />
                        </div>
                      </div>

                      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="text-center">
                          <?php the_field('rn_content', 123); ?>
                        </div>
                      </div>

                      <div class="text-center">
                        <?php the_field('rn_upcoming_content', 123); ?>
                      </div>

                      <div class="modal fade" id="kim-full-description" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="vertical-alignment-helper">
                          <div class="modal-dialog modal-lg vertical-align-center">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                 <h3 class="modal-title" id="myModalLabel">Knowledge in Motion | Lecture &amp; Webcast Series</h3>
                              </div>
                              <div class="modal-body">
                                <?php the_field('rn_full_description', 123); ?>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="modal fade" id="kim-signup" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="vertical-alignment-helper">
                          <div class="modal-dialog modal-lg vertical-align-center">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                 <h3 class="modal-title" id="myModalLabel">Signup for Next Knowledge in Motion Lecture and Webcast</h3>
                              </div>
                              <div class="modal-body">
                                <iframe height="1272" allowTransparency="true" frameborder="0" scrolling="no" style="width:100%;border:none" src="https://spauldingrehab.wufoo.com/embed/<?php the_field('rn_signup_code', 123); ?>/"> <a href="https://spauldingrehab.wufoo.com/forms/<?php the_field('rn_signup_code', 123); ?>/">Fill out my Wufoo form!</a>
                                </iframe>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                  </div>
                </div>
              </div>
            </div>
            <!--End Tab 2-->

<script>

// var currentOffset = 2;
// var videosCount = 0;
// var segmentLength = 2;
// jQuery(document).ready(function () {
//  console.log('loadMoreEducation1 clicked');
//     videosCount = 22;
//     jQuery('#loadMoreEducation1').click(function (e) {
//    console.log('loadMoreEducation1 clicked');
//         if (currentOffset + segmentLength <= videosCount){
//           var start = currentOffset;
//           var finish = currentOffset + segmentLength;
//           if (finish > videosCount) finish = videosCount;
//           for(var i = start; i <= finish; i++){
//             jQuery('.video-'+currentOffset).slideDown();
//             currentOffset++;
//           }
//         }
//    e.preventDefault();
//     });
// });
</script>

            <!--Start Tab 3-->
            <div id="tabs-3">
              <div class="tab-pane" id="kim-online-videos">
                <div class="education-snerscic-content">
                  <div class="row videos">

                    <div class="col-sm-12" style="display: block;">
                      <h2><?php the_field('online_videos_description', 123); ?></h2>
                    </div>

                    <?php if(have_rows('online_videos', 123)): $i = 0;
                    while (have_rows('online_videos', 123)) : $i++; the_row();

                      $url = get_sub_field('video_link');
                      $url = str_replace("http:", "", $url);
                      $url = str_replace("https:", "", $url);
                      $url = str_replace("www.", "", $url);
                      $url = str_replace("watch?v=", "", $url);
                      $url = str_replace("youtube.com", "", $url);
                      $url = str_replace("youtu.be", "", $url);
                      $url = str_replace("embed", "", $url);
                      $url = str_replace("/", "", $url);
                      $videoURL = '//www.youtube.com/embed/' . $url;

                    ?>  

                    <div class="col-sm-6 video video-<?php echo $i; ?>">
                      <div class="col-sm-12">
                        <div class="embed-responsive embed-responsive-16by9">
                          <!-- <iframe class="embed-responsive-item lazy" src="<?php //echo $videoURL; ?>" allowfullscreen=""></iframe> -->
                          <div class="lazy" data-loader="youtubeLoader" data-video="<?php echo $videoURL; ?>"></div>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="video-info" data-mh="info">
                        <h3><a href="<?php the_sub_field('video_link'); ?>" target="_blank">
                          <?php the_sub_field('video_heading'); ?></a></h3>
                        <p class="video-description" data-mh="description">
                          <?php the_sub_field('video_description', false, false); ?>
                        </p>
                        <?php if(get_sub_field('survey_link')): ?><p>After viewing the webcast, please complete this brief <a href="<?php the_sub_field('survey_link'); ?>">online survey</a></p><?php endif; ?>
                        </div>
                      </div>
                    </div>

                    <?php endwhile; endif; ?>

                    <div class="load-more col-sm-12 text-center">
                      <a href="#" id="loadMoreEducation1" class="load-more-videos blue-btn btn">Load More Videos</a>
                    </div>

                  </div>
                </div>
              </div>
            </div>
            <!--End Tab 3-->

            <!--Start Tab 4-->
            <div id="tabs-4">
              <div class="tab-pane" id="consumer-fact-sheets">
                <div class="education-snerscic-content">
                  <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                      <div class="mid-banner-referral-content">
                        <?php the_field('consumer_factsheets_description', 123); ?>
                      </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                      <div class="education-content">

                        <?php if(have_rows('consumer_factsheets', 123)): $i = 0;
                        while (have_rows('consumer_factsheets', 123)) : $i++; the_row();

                          $photo = get_sub_field('photo');

                        ?>

                        <!--Block 1-->
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                          <div class="factsheet-box">
                            <img class="factsheet-img lazy" src="<?php bloginfo('url'); ?>/media/spinner.gif" data-src="<?php echo $photo['url']; ?>" alt="<?php echo $photo['alt']; ?>">
                            <div class="internal-factsheet factsheet-<?php echo $i; ?>">
                              <?php if($i == 14 || $i == 15): ?>
                              <h3 data-mh="factsheets-title"><a class="crackle" tabindex="0" role="button" data-container="body" data-popover="true" data-html="true" data-content="This factsheet was created by Spaulding New England Regional Spinal Cord Injury Center" data-placement="top" data-trigger="hover"><?php the_sub_field('title'); ?></a></h3>
                              <?php else: ?>
                                <h3 data-mh="factsheets-title"><?php the_sub_field('title'); ?></h3>
                              <?php endif; ?>
                              <?php if(get_sub_field('blurb')): ?><p class="factsheet-text" data-mh="factsheets"><?php the_sub_field('blurb'); ?></p><?php endif; ?>
                              <a href="<?php the_sub_field('link'); ?>" class="english"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>View Factsheet</a>
                            </div>
                          </div>
                        </div>
                        <!--End of Block-->

                        <?php endwhile; endif; ?>
                        <!-- <div class="loader">
                          <a href="#">Load More</a>
                        </div> -->
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
            <!--End Tab 4-->

            <!--Start Tab 5-->
            <div id="tabs-5">
              <div class="tab-pane" id="new-to-sci">
                <div class="education-snerscic-content">
                  <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <div class="currently-recruiting-content">
                        <?php the_field('new_to_sci', 123); ?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!--End Tab 5-->

          </div>
        </div>
      </div>
    </div>
  </div>
  </section>
</div><!--End of Tab Container-->
</div><!--End of Tabs-->
<!-- Mid Container Ends Here -->
