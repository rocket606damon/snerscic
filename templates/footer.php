<!-- Footers Starts Here -->
<footer id="footer">
  <?php 
    $footer_logo = get_field('footer_logo', 'option');
    $partner_a = get_field('partner_1_image', 'option');
    $partner_b = get_field('partner_2_image', 'option');
    $partner_c = get_field('partner_3_image', 'option');
    $script = '/srv/users/serverpilot/apps/snerscic/public/wp-content/themes/snerscic/lib/pageChangeHandler.js';
  ?>
  <div class="footer-content">
    <div class="container">
      <div class="row">
      	<!--Start Footer Left-->
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <a class="foo-logo" href="<?php echo home_url(); ?>" title="Spaulding New England Regional Spinal Cord Injury Center"><img src="<?php echo $footer_logo['url']; ?>" alt="footer-logo-snerscic" /></a>
          <div class="social-media">
            <h4><?php the_field('social_header'); ?></h4>
            <ul class="social-icons">
              <li><a href="https://www.facebook.com/SpauldingRehab" target="_blank" title="Facebook" class="fa fa-facebook"></a></li>
              <li><a href="https://twitter.com/SpauldingSCIMS" target="_blank" title="Twitter" class="fa fa-twitter"></a></li>
            </ul>
          </div>
        </div>
        <!--End Footer Left-->

        <!--Start Footer Center-->
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="recive-news">
            <h3><?php the_field('front_center_text', 'option'); ?></h3>
            <a href="#" data-toggle="modal" data-target="#mailinglist-signup" class="btn yellow-btn" title="Join Our Mailing List">Join Our Mailing List</a>
          </div>
          <div class="our-partners">
            <h4>Our Partners:</h4>
            <ul>
              <li><a href="<?php the_field('partner_1_link', 'option'); ?>" title="" target="_blank"><img src="<?php echo $partner_a['url']; ?>" alt="<?php echo $partner_a['alt']; ?>" /></a></li>
              <li><a href="<?php the_field('partner_2_link', 'option'); ?>" title="" target="_blank"><img src="<?php echo $partner_b['url']; ?>" alt="<?php echo $partner_b['alt']; ?>" /></a></li>
              <li><a href="<?php the_field('partner_3_link', 'option'); ?>" title="" target="_blank"><img src="<?php echo $partner_c['url']; ?>" alt="<?php echo $partner_b['alt']; ?>" /></a></li>
            </ul>
          </div>
        </div>
        <!--End Footer Center-->

        <!--Start Footer Right-->
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <ul class="contact-footer">
            <li class="header">Contact Us</li>
            <li><a href="#"><i class="fa fa-phone" aria-hidden="true"></i><?php the_field('footer_phone_number', 'option'); ?></a></li>
            <li><a href="mailto:<?php the_field('footer_email', 'option'); ?>"><i class="fa fa-envelope" aria-hidden="true"></i><?php the_field('footer_email', 'option'); ?></a></li>
            <!-- <li><i class="fa fa-clock-o" aria-hidden="true"></i>9:00AM to 5:00PM</li> -->
          </ul>
        </div>
        <!--End Footer Right-->

      </div>
    </div>
  </div>
</footer>
<script type="text/javascript" src="/wp-content/themes/snerscic/lib/pageChangeHandler.js?<?php echo filemtime($script); ?>"></script>

<!-- Footers Ends Here -->
