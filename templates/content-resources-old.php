<!-- Banner Starts Here -->
<section class="inner-banner" style="background-image: url('<?php bloginfo('url'); ?>/media/resources-banner.jpg')">
  <div class="tbl">
    <div class="tbl-cell-bottom">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 col-sm-12">
            <h1>Resources</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Banner Ends Here -->

<!-- Mid Content Starts Here -->
<section class="mid-content mid-content-blue">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
        <h2>Resources Galore! The SNERSCIC team has put together a bunch of top-notch resources just for you!</h2>
      </div>
    </div>
  </div>
</section>

<!-- Mid Page Links Starts Here -->
<div id="tabs" class="resources-page">
<section class="page-links">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
        <ul class="links-listing">

          <li class="resources-tabs-1" class="li-clicker">
            <div class="tbl">
              <div class="tbl-cell">
                <a href="#tabs-1" class="tab-clicker" title="New England SCI Toolkit">
                  <i class="icon icon-toolkit"></i>
                  <span>New England sci Toolkit (NESCIT)</span>
                </a>
              </div>
            </div>
          </li>

          <li class="resources-tabs-2" class="li-clicker">
            <div class="tbl">
              <div class="tbl-cell">
                <a href="#tabs-2" class="tab-clicker" title="Resource Referrals">
                  <i class="icon icon-referral"></i>
                  <span>Resource Referral</span>
                </a>
              </div>
            </div>
          </li>

          <li class="resources-tabs-3" class="li-clicker">
            <div class="tbl">
              <div class="tbl-cell">
                <a href="#tabs-3" class="tab-clicker" title="Resource List">
                  <i class="icon icon-list"></i>
                  <span>Resource List</span>
                </a>
              </div>
            </div>
          </li>

          <li class="resources-tabs-4" class="li-clicker">
            <div class="tbl">
              <div class="tbl-cell">
                <a href="#tabs-4" class="tab-clicker" title="Community News and Events">
                  <i class="icon icon-news"></i>
                  <span>Community News and Events
                </a>
              </div>
            </div>
          </li>

        </ul>
      </div>
    </div>
  </div>
</section>
<!-- Mid Page Links Ends Here -->


<!-- Mid Container Starts Here -->
<div id="tab-container" class="tab-container about-page">
  <section>
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="tab-content">

            <!--Start Tab 1-->
            <div id="tabs-1">
              <div class="tab-pane" id="new-england-sci-toolkit-nescit">
                <div class="resource-snerscic-content">
                  <div class="row">

                    <!--Top Section-->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                      <div class="mid-banner-referral-content">
                        <p>The New England SCI Toolkit (NESCIT) is a collaborative effort between facilities providing spinal cord injury (SCI) rehabilitation in New England. This collaboration ensures patients throughout New England and beyond are receiving the same coordinated standard of care wherever they receive rehabilitation. Further, this Toolkit will aid in building capacity at facilities that may not treat patients with SCI often enough to have developed expertise.</p>
                        <p>Our objective was to develop a toolkit for SCI education and care designed as an educational tool for use after acute rehabilitation to help improve functional outcomes while decreasing the frequency of complications associated with paralysis for people with SCI.</p>
                        <p>Facilities participating with NESCIT will contribute to the communication between clinicians to share treatment ideas, problem solve, and discuss program development ideas. We encourage NESCIT Network and others, to share their processes and resources with other facilities to ensure consistency of care.</p>
                        <p><strong>THIS TOOL DOES NOT PROVIDE MEDICAL ADVICE.</strong> It is intended for informational purposes only. It is not a substitute for professional medical advice, diagnosis, or treatment. Never ignore professional medical advice in seeking treatment because of something you have read in the Toolkit. If you think you may have a medical emergency, immediately dial 911.</p>
                      </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" id="toolkit-list">
                      <div class="resources-content">

                      <!--Block 1-->
                      <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="spacer">
                        <div class="toolkit-icon">
                          <img src="<?php bloginfo('url'); ?>/media/toolkit1.png" alt="Patient/Family/Caregiver Education">
                        </div>
                        <div class="toolkit-text" data-mh="toolkit-text">
                          <h3>Patient/Family/Caregiver Education</h3>
                          <p>Individuals, their family members and caregivers taking part in inpatient rehabilitation for spinal cord injury (SCI) will receive appropriate education to maximize functional independence and minimize secondary health conditions.</p>
                        </div>
                        <a href="#" data-toggle="modal" data-target="#largeModal1">Learn More</a>
                      </div>
                      </div>
                      <!--MODAL#1 BEGINS-->
                      <div class="modal fade" id="largeModal1" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title" id="myModalLabel">1. Patient/Family/Caregiver Education</h4>
                            </div>

                            <div class="modal-body">
                              <p><a href="https://www.gaylord.org/Portals/0/PDFS/SCI%20Toolkit/2016_1%20Pt%20Fam%20Care%20Sum.pdf" target="_blank">[Print Friendly Version]</a></p>
                              <p><strong>1.1 STANDARD:</strong> Individuals, their family members and caregivers taking part in inpatient rehabilitation for spinal cord injury (SCI) will receive appropriate education to maximize functional independence and minimize secondary health conditions. Education will encompass all areas of SCI rehabilitation and will be included in all standards listed.</p>
                              <p><strong>Minimum Requirements:</strong></p>
                              <ul>
                                <li>Core educational areas: autonomic dysreflexia, bowel/bladder, SCI anatomy &amp; physiology, skin care,
                                respiratory care, sexuality, medical complications, psychosocial, home modifications, equipment
                                needs, rehabilitation team roles, community reentry (including accessibility), medications,
                                discharge planning, nutrition &amp; lifestyle (exercise), spasticity management and contracture
                                prevention, pain, driving, recreation, community and educational resources, assistive technology,
                                vocational rehabilitation, mobility, self-care, self-advocacy, circulatory (DVT, etc.), heterotopic
                                ossification (HO), health care maintenance (preventative care, follow up with outpatient
                                appointments, etc.)</li>
                                <li>Patient and/or caregiver competency established via verbal recall and/or demonstration.</li>
                              </ul>
                              <p><strong>Recommended Resources:</strong></p>
                              <ul>
                                <li><a href="http://tinyurl.com/ksa3z5e" target="_blank">Yes, You Can! A Guide to Self-Care for Persons with Spinal Cord Injury</a> published by the Paralyzed Veterans Association (PVA)</li>
                                <li><a href="http://tinyurl.com/mfqnnj2" target="_blank">PVA Consumer Guidelines*</a> are available on multiple topics and are free to download</li>
                                <li><a href="http://www.pva.org/publications/clinical-practice-guidelines" target="_blank">PVA Clinical Practice Guidelines*</a> are available on multiple topics and are free download</li>
                                <li><a href="http://www.christopherreeve.org" target="_blank">Paralysis Resource Center,</a> The Christopher and Dana Reeve Foundation - call toll-free 1-800-225-0292. Information is offered in multiple languages</li>
                                <li><a href="http://www.christopherreeve.org" target="_blank">Connecticut Resource Directory &amp; patient packet</a> by Spinal Cord Injury Association of Connecticut, a chapter of United Spinal at www.sciact.org</li>
                                <li>Peer mentor programming offered through United Spinal local chapters, call 203-284-2910 in Connecticut or 781-933-8666 in Massachusetts</li>
                                <li><a href="http://tinyurl.com/NERSCICvids" target="_blank">SNERSCIC Education Program Online Videos</a>
                                <li><a href="http://www.sciguide.org" target="_blank">SCI Guide</a> – including Top Sites for Newly Injured and Best of the Best sites</li>
                                <li>Inpatient and outpatient support groups including family/caregiver support groups</li>
                                <li>Patient Education Manual(site-specific – to obtain a copy from Gaylord Hospital, contact Diana Pernigotti at <a href="#">DPernigotti@gaylord.org</a> and contact Joanne Morello at <a href="#">Jmorello@northeastrehab.com</a> for a copy from Northeast Rehabilitation)</li>
                                <li><a href="http://tinyurl.com/335k9n" target="_blank">National Institute of Health Physical Exam Recommendations</a></li>
                                <li>Model Systems Knowledge Translation Center SCI Factsheets (available online from SNERSCIC website and the MSKTC website as a <a href="http://tinyurl.com/pth9kky" target="_blank">complete booklet</a> or <a href="http://www.MSKTC.org" target="_blank">separate topics</a></li>
                              </ul>
                            </div>

                            <div class="modal-footer">
                              <button type="button" class="btn-prev">&lt; Prev</button>
                              <button type="button" class="btn-next">Next ></button>
                              <button type="button" class="btn-default" data-dismiss="modal">Close</button>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!--MODAL#1 ENDS-->

                      <!--Block 2-->
                      <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="spacer">
                          <div class="toolkit-icon">
                            <img src="<?php bloginfo('url'); ?>/media/toolkit2.png" alt="Autonomic Dysreflexia">
                          </div>
                          <div class="toolkit-text" data-mh="toolkit-text">
                            <h3>Autonomic Dysreflexia</h3>
                            <p>All care providers will identify episodes of autonomic dysreflexia (AD) and provide appropriate intervention. Intervention will include identifying and removing noxious stimuli, providing medical care as indicated, and monitoring patient’s status.</p>
                          </div>
                          <a href="#" data-toggle="modal" data-target="#largeModal2">Learn More</a>
                        </div>
                      </div>
                      <!--MODAL#2 BEGINS-->
                      <div class="modal fade" id="largeModal2" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title" id="myModalLabel">2. Autonomic Dysreflexia</h4>
                            </div>
                            <div class="modal-body">
                              <p><a href="https://www.gaylord.org/Portals/0/PDFS/SCI%20Toolkit/2016_2%20Auto%20Dys%20Sum.pdf" target="_blank">[Print Friendly Version]</a></p>
                              <p><strong>2.1  STANDARD:</strong> All care providers will identify episodes of autonomic dysreflexia (AD) and provide appropriate intervention. Intervention will include identifying and removing noxious stimuli, providing medical care as indicated, and monitoring patient’s status.</p>
                              <p><strong>Minimum Requirements:</strong></p>
                              <ul>
                                <li>Facility has an AD policy and procedure or guideline  in place</li>
                                <li>All care providers are trained to identify signs and symptoms of AD</li>
                                <li>All care providers are trained to follow facility policy and procedure on AD</li>
                                <li>All care providers will understand the causes of AD for prevention</li>
                                <li>Staff competency conducted for all care providers</li>
                              </ul>
                              <p><strong>Recommended Resources:</strong></p>
                              <ul>
                                <li><a href="http://tinyurl.com/ksa3z5e" target="_blank">Yes, You Can! A Guide to Self-Care for Persons with Spinal Cord Injury</a> published by the Paralyzed Veterans Association (PVA)</li>
                                <li>Patient Education Manual (site-specific – to obtain a copy from Gaylord Hospital, contact Diana Pernigotti at <a href="#">DPernigotti@gaylord.org</a> and contact Joanne Morello at  <a href="#">Jmorello@northeastrehab.com</a> for a copy from Northeast Rehabilitation)</li>
                                <li>Specialized courses offered through NESCIT facilities on SCI – available to outside caregivers with CEUs. The SCI Specialist course is periodically offered at Gaylord Hospital. Contact Janine Clarkson at <a href="#">Jclarkson@gaylord.org</a> for more information.</li>
                                <li><a href="http://tinyurl.com/qzll2gq" target="_blank">Christopher and Dana Reeve Foundation Paralysis Resource Guide</a> (available in many languages for free)</li>
                                <li><a href="http://tinyurl.com/p36sff6" target="_blank">PVA Clinical Practice Guideline Acute Management of Autonomic Dysreflexia*</a></li>
                                <li>General SCI Competency at Northeast Rehabilitation Contact Joanne Morello at <a href="#">Jmorello@northeastrehab.com</a> for more information</li>
                              </ul>
                              <p><strong>2.2  STANDARD:</strong> Refer to STANDARD 1: Patient/Family/Caregiver Teaching</p>
                              <p><strong>Recommended Resources:</strong></p>
                              <ul>
                                <li><a href="http://tinyurl.com/qh6gmoj" target="_blank">Credit card-style AD education cards</a> for patients from the PVA</li>
                                <li>Education cards in Yes, You Can! book The book can be ordered online <a href="http://tinyurl.com/ksa3z5e" target="_blank">here</a></li>
                                <li><a href="http://tinyurl.com/kfrobkd" target="_blank">PVA Consumer Guideline Autonomic Dysreflexia: What You Should Know*</a></li>
                              </ul>
                            </div>

                            <div class="modal-footer">
                              <button type="button" class="btn-prev">&lt; Prev</button>
                              <button type="button" class="btn-next">Next ></button>
                              <button type="button" class="btn-default" data-dismiss="modal">Close</button>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!--MODAL#2 ENDS-->

                      <!--Block 3-->
                      <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="spacer">
                        <div class="toolkit-icon">
                          <img src="<?php bloginfo('url'); ?>/media/toolkit3.png" alt="Skin Care">
                        </div>
                        <div class="toolkit-text" data-mh="toolkit-text">
                          <h3>Skin Care</h3>
                          <p>Patients with existing pressure ulcers will receive appropriate treatment to decrease severity of existing ulcers and prevent the development of new ulcers.</p>
                        </div>
                        <a href="#" data-toggle="modal" data-target="#largeModal3">Learn More</a>
                      </div>
                      </div>
                      <!--MODAL#3 BEGINS-->
                      <div class="modal fade" id="largeModal3" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title" id="myModalLabel">3. Skin Care</h4>
                            </div>
                            <div class="modal-body">
                              <p><a href="https://www.gaylord.org/Portals/0/PDFS/SCI%20Toolkit/2016_3%20Skin%20Care%20Sum.pdf" target="_blank">[Print Friendly Version]</a></p>
                              <p><strong>3.1  STANDARD:</strong> Patients with existing pressure ulcers will receive appropriate treatment to decrease severity of existing ulcers and prevent the development of new ulcers.</p>
                              <p><strong>Minimum Requirements:</strong></p>
                              <ul>
                                <li>Facility has a skin care policy and procedure or guideline in place</li>
                                <li>Staff competency conducted for appropriate providers for prevention and standardized assessment of risk</li>
                                <li>All care providers are trained to follow facility policy and procedure/guidelines on skin care</li>
                                <li>Site-specific skin management program encompassing assessment, prevention and treatment</li>
                                <li>Access to certified wound care specialist (WOCN) (for most up-to-date evidence-based care)</li>
                                <li>Process in place for measuring outcomes</li>
                              </ul>
                              <p><strong>Recommended Resources:</strong></p>
                              <ul>
                                <li>PVA Clinical Practice Guide Pressure Ulcer Prevention and Treatment Following Spinal Cord Injury* available for free download at  http://tinyurl.com/m6xubgd</li>
                                <li>PVA Consumer Guide Pressure Ulcers: What you Should Know* available for <a href="http://tinyurl.com/lqff2kv" target="_blank">free download</a></li>
                                <li>MSKTC SCI Factsheet Skin Care and Pressure Sores in Spinal Cord Injury 6 Part Series available for <a href="http://tinyurl.com/k9orwb7" target="_blank">free download</a></li>
                                <li>Clinical Practice Guidelines for Prevention and Management of Pressure Ulcers available to purchase from the <a href="http://tinyurl.com/kaadnqs" target="_blank">Wound Ostomy</a> and <a href="http://www.wocn.org/" target="_blank">Continence Nursing Society</a></li>
                              </ul>
                              <p><strong>3.2 STANDARD:</strong> Patients taking part in inpatient rehabilitation will be free from new pressure ulcers throughout their stay. All disciplines involved in patient care will take part in the education and prevention of pressure ulcers.</p>
                              <p><strong>Minimum Requirements:</strong></p>
                              <ul>
                                <li>Facility follows professional standards of care for treatment of existing ulcers, such as Clinical Practice Guidelines for Prevention and Management of Pressure Ulcers per WOCN Society</li>
                                <li>Staff competency conducted for appropriate providers for standardized assessment regarding identification and treatment of pressure ulcers</li>
                                <li>see 3.1</li>
                              </ul>
                              <p><strong>3.3 STANDARD:</strong> Refer to STANDARD 1: Patient/Family/Caregiver Education</p>
                              <p><strong>Recommended Resources (in addition to Standard 1 resources):</strong></p>
                              <ul>
                                <li>Online lecture from Lauren Harney, RN, BSN, CWON <a href="http://tinyurl.com/k9orwb7" target="_blank">“The Skin You’re In: An Overview of Maintaining Skin Integrity for Individuals with Spinal Cord Injury”</a></li>
                              </ul>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn-prev">&lt; Prev</button>
                              <button type="button" class="btn-next">Next ></button>
                              <button type="button" class="btn-default" data-dismiss="modal">Close</button>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!--MODAL#3 ENDS-->

                      <!--Block 4-->
                      <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="spacer">
                          <div class="toolkit-icon">
                            <img src="<?php bloginfo('url'); ?>/media/toolkit4.png" alt="Bladder Management">
                          </div>
                          <div class="toolkit-text" data-mh="toolkit-text">
                            <h3>Bladder Management</h3>
                            <p>Upon admission, each patient will have an assessment of bladder function. A bladder management program will be initiated and followed for all patients taking part in rehabilitation in order to resume regular and complete emptying of the bladder for urinary system health.</p>
                          </div>
                          <a href="#" data-toggle="modal" data-target="#largeModal4">Learn More</a>
                        </div>
                      </div>
                      <!--MODAL#4 BEGINS-->
                      <div class="modal fade" id="largeModal4" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title" id="myModalLabel">4. Bladder Management</h4>
                            </div>
                            <div class="modal-body">
                              <p><a href="https://www.gaylord.org/Portals/0/PDFS/SCI%20Toolkit/2016_4%20Bladder%20Management%20Sum.pdf" target="_blank">[Print Friendly Version]</a></p>
                              <p><strong>4.1.  STANDARD:</strong> Upon admission, each patient will have an assessment of bladder function. A bladder management program will be initiated and followed for all patients taking part in rehabilitation in order to resume regular and complete emptying of the bladder for urinary system health.</p>
                              <p><strong>Minimum Requirements:</strong></p>
                              <ul>
                                <li>Facility has a bladder management policy and procedure/guidelines in place</li>
                                <li>All appropriate care providers are trained to follow facility policy and procedure/guidelines on bladder management</li>
                                <li>All pertinent care providers are trained in appropriate technique and use of adaptive equipment for bladder emptying</li>
                                <li>Site-specific bladder management program in place encompassing assessment, intervention and ongoing management</li>
                                <li>Staff competency conducted for all direct care providers</li>
                                <li>Access to urologic assessment and monitoring as needed</li>
                              </ul>
                              <p><strong>Recommended Resources:</strong></p>
                              <ul>
                                <li>PVA Consumer Guide Bladder Management Following Spinal Cord Injury: What You Should Know* available for <a href="http://tinyurl.com/n67wb8b" target="_blank">free download</a></li>
                                <li>PVA Clinical Practice Guideline Bladder Management for Adults with Spinal Cord Injury* available for <a href="http://tinyurl.com/mqldjq3" target="_blank">free download</a></li>
                                <li>University of Alabama SCI InfoSheets: <a href="http://tinyurl.com/kbw43ws" target="_blank">Bladder Care and Management</a></li>
                              </ul>
                              <p><strong>4.2.  STANDARD:</strong> Staff members will be able to identify urinary complications and provide appropriate intervention.</p>
                              <p><strong>Minimum Requirements:</strong></p>
                              <ul>
                                <li>All appropriate care providers are trained to identify signs and symptoms of urinary complications</li>
                                <li>Access to ongoing urologic assessment and monitoring to diagnose and develop treatment plans for urinary complications</li>
                                <li>Facility has a process in place for measuring and managing outcomes</li>
                              </ul>
                              <p><strong>Recommended Resources:</strong></p>
                              <ul>
                                <li>Institute for Healthcare Improvement: How-to Guide: Prevent Catheter-Associated Urinary Tract Infection* available for free download at http://tinyurl.com/lwkunaq</li>
                                <li>PVA Consumer Guide Bladder Management Following Spinal Cord Injury: What You Should Know* available for <a href="http://tinyurl.com/n67wb8b" target="_blank">free download</a></li>
                                <li>PVA Clinical Practice Guideline Bladder Management for Adults with Spinal Cord Injury* available for <a href="http://tinyurl.com/mqldjq3" target="_blank">free download</a></li>
                                <li>University of Alabama SCI InfoSheets: <a href="http://tinyurl.com/kbw43ws" target="_blank">Bladder Care and Management</a></li>
                                <li>MSKTC Factsheet Surgical Alternatives for Bladder Management Following SCI available for <a href="http://tinyurl.com/pqzjoqr" target="_blank">free download</a></li>
                              </ul>
                              <p><strong>4.3.  STANDARD:</strong> Individuals, their family members and caregivers taking part in inpatient rehabilitation for SCI will receive appropriate education in bladder management techniques to maximize functional independence and minimize secondary health conditions.</p>
                              <p><strong>Minimum Requirements:</strong></p>
                              <ul>
                                <li>Family/caregiver/patient training on bladder management techniques and use of adaptive equipment during inpatient rehabilitation</li>
                                <li>Family/caregiver/patient training on identification of signs and/or symptoms of urinary complications and possible interventions, including seeking medical attention</li>
                                <li>Family/caregiver/patient competency established via verbal recall and/or demonstration</li>
                              </ul>
                              <p><strong>Recommended Resources:</strong></p>
                              <ul>
                                <li>Institute for Healthcare Improvement: How-to Guide: Prevent Catheter-Associated Urinary Tract Infection* available for <a href="http://tinyurl.com/lwkunaq" target="_blank">free download</a></li>
                                <li>PVA Consumer Guide Bladder Management Following Spinal Cord Injury: What You Should Know* available for <a href="http://tinyurl.com/n67wb8b" target="_blank">free download</a></li>
                                <li>PVA Clinical Practice Guideline Bladder Management for Adults with Spinal Cord Injury* available for <a href="http://tinyurl.com/mqldjq3" target="_blank">free download</a></li>
                                <li>University of Alabama SCI InfoSheets: <a href="http://tinyurl.com/kbw43ws" target="_blank">Bladder Care and Management</a></li>
                                <li>MSKTC Factsheet Surgical Alternatives for Bladder Management Following SCI available for <a href="http://tinyurl.com/pqzjoqr" target="_blank">free download</a></li>
                              </ul>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn-prev">&lt; Prev</button>
                              <button type="button" class="btn-next">Next ></button>
                              <button type="button" class="btn-default" data-dismiss="modal">Close</button>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!--MODAL#4 ENDS-->

                      <!--Block 5-->
                      <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="spacer">
                          <div class="toolkit-icon">
                            <img src="<?php bloginfo('url'); ?>/media/toolkit5.png" alt="Bowel Management">
                          </div>
                          <div class="toolkit-text" data-mh="toolkit-text">
                            <h3>Bowel Management</h3>
                            <p>Each patient will have an assessment of bowel function. A bowel program will be initiated and followed for all patients in order to strive for regular and complete emptying of bowels.</p>
                          </div>
                          <a href="#" data-toggle="modal" data-target="#largeModal5">Learn More</a>
                      </div>
                    </div>
                      <!--MODAL#5 BEGINS-->
                      <div class="modal fade" id="largeModal5" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title" id="myModalLabel">5.  Bowel Management</h4>
                            </div>
                            <div class="modal-body">
                              <p><a href="https://www.gaylord.org/Portals/0/PDFS/SCI%20Toolkit/2016_5%20Bowel%20Mgmt%20Sum.pdf" target="_blank">[Print Friendly Version]</a></p>
                              <p><strong>5.1  STANDARD:</strong> Each patient will have an assessment of bowel function. A bowel program will be initiated and followed for all patients in order to strive for regular and complete emptying of bowels.</p>
                              <p><strong>Minimum Requirements:</strong></p>
                              <ul>
                                <li>Facility has bowel management policy and procedures/guidelines in place</li>
                                <li>All appropriate care providers trained to follow policy and procedure/guidelines on bowel management</li>
                                <li>All pertinent care providers are trained in appropriate technique and use of adaptive equipment for bowel emptying</li>
                                <li>Site-specific bowel management program in place encompassing assessment, intervention, and ongoing management</li>
                                <li>Access to bowel management assessment and monitoring as needed</li>
                              </ul>
                              <p><strong>Recommended Resources:</strong></p>
                              <ul>
                                <li>PVA Consumer Guide Neurogenic Bowel: What You Should Know* available for <a href="http://tinyurl.com/n3qfaxl" target="_blank">free download</a></li>
                                <li>PVA Clinical Practice Guideline Neurogenic Bowel Management in Adults with Spinal Cord Injury* available for <a href="http://tinyurl.com/kzkbfwe" target="_blank">free download</a></li>
                                <li>University of Alabama InfoSheets: <a href="http://tinyurl.com/kbw43ws" target="_blank">Bowel Management after Spinal Cord Injury</a></li>
                                <li><a href="http://tinyurl.com/nypud95" target="_blank">Spinal Cord Injury Rehabilitation Evidence (SCIRE) Project factsheet. Canadian sourced</a></li>
                              </ul>
                              <p><strong>5.2  STANDARD:</strong> Each patient will have an assessment of bowel function. A bowel program will be initiated and followed for all patients in order to strive for regular and complete emptying of bowels.</p>
                              <p><strong>Minimum Requirements:</strong></p>
                              <ul>
                                <li>All appropriate care providers trained to identify signs and symptoms of bowel dysfunction</li>
                                <li>Ongoing bowel monitoring and assessment to diagnose and develop treatment plans for bowel complications</li>
                                <li>Process in place for measuring outcomes</li>
                              </ul>
                              <p><strong>Recommended Resources:</strong></p>
                              <ul>
                                <li>One page information sheet plus extra notes for each individual patient being discharged from acute rehabilitation with specific comments regarding their bowel program</li>
                                <li>PVA Consumer Guide Neurogenic Bowel: What You Should Know* available for <a href="http://tinyurl.com/n3qfaxl" target="_blank">free download</a></li>
                                <li>PVA Clinical Practice Guideline Neurogenic Bowel Management in Adults with Spinal Cord Injury* available for <a href="http://tinyurl.com/kzkbfwe" target="_blank">free download</a></li>
                                <li>University of Alabama InfoSheets: <a href="http://tinyurl.com/kbw43ws" target="_blank">Bowel Management after Spinal Cord Injury</a></li>
                                <li><a href="http://tinyurl.com/nypud95" target="_blank">Spinal Cord Injury Rehabilitation Evidence (SCIRE) Project factsheet. Canadian sourced</a></li>
                                <li><a href="http://tinyurl.com/mve9nwv" target="_blank">Spaulding-Harvard Model System patient education bowel PowerPoint presentation Bowel and Bladder Management</a></li>
                              </ul>
                              <p><strong>5.3  STANDARD:</strong> Individuals, family members and caregivers taking part in inpatient rehabilitation for SCI will receive appropriate education in bowel management techniques to maximize functional independence and minimize secondary health conditions.</p>
                              <p><strong>Minimum Requirements:</strong></p>
                              <ul>
                                <li>Patient and/or caregiver competency established via verbal recall and/or demonstration</li>
                              </ul>
                              <p><strong>Recommended Resources:</strong></p>
                              <ul>
                                <li>PVA Clinical Practice Guideline Neurogenic Bowel Management in Adults with Spinal Cord Injury* available for <a href="http://tinyurl.com/kzkbfwe" target="_blank">free download</a></li>
                              </ul>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn-prev">&lt; Prev</button>
                              <button type="button" class="btn-next">Next ></button>
                              <button type="button" class="btn-default" data-dismiss="modal">Close</button>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!--MODAL#5 ENDS-->

                      <!--Block 6-->
                      <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="spacer">
                          <div class="toolkit-icon">
                            <img src="<?php bloginfo('url'); ?>/media/toolkit6.png" alt="Sexual Health and Fertility">
                          </div>
                          <div class="toolkit-text" data-mh="toolkit-text">
                            <h3>Sexual Health and Fertility</h3>
                            <p>Individuals and their appropriate family member(s) or partner(s) will be offered age specific education and counseling regarding medical and psychosocial issues related to sexual health and function after an SCI.</p>
                          </div>
                          <a href="#" data-toggle="modal" data-target="#largeModal6">Learn More</a>
                        </div>
                      </div>
                      <!--MODAL#6 BEGINS-->
                      <div class="modal fade" id="largeModal6" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title" id="myModalLabel">6. Sexual Health and Fertility</h4>
                            </div>
                            <div class="modal-body">
                              <p><a href="https://www.gaylord.org/Portals/0/PDFS/SCI%20Toolkit/2016_6%20Sexuality%20Sum.pdf" target="_blank">[Print Friendly Version]</a></p>
                              <p><strong>6.1.  STANDARD:</strong> Individuals and their appropriate family member(s) or partner(s) will be offered age specific education and counseling regarding medical and psychosocial issues related to sexual health and function after an SCI.</p>
                              <p><strong>Minimum Requirements:</strong></p>
                              <ul>
                                <li>Facility has a process to assess/educate re: sexual health in SCI- “Who says what when?”</li>
                                <li>Maintain an open discussion and provide access to education about sex in both formal and informal settings throughout the treatment continuum</li>
                                <li>Care providers may offer referral to the appropriate rehabilitation professional in collaboration with the medical provider</li>
                              </ul>
                              <p><strong>Recommended Resources:</strong></p>
                              <ul>
                                <li>PVA Consumer Guide Sexuality and Reproductive Health for Adults with Spinal Cord Injury: What You Should Know* available for<a href="http://tinyurl.com/kfnvtby" target="_blank"> free download</a></li>
                                <li>PVA Clinical Practice Guideline Sexuality and Reproductive Health in Adults with Spinal Cord Injury* available for<a href="" target="_blank"> free download</a></li>
                                <li><a href="http://tinyurl.com/m5w3yo3" target="_blank">Christopher and Dana Reeve Foundation</a> Paralysis Resource Guide pp. 188-197</li>
                              </ul>
                              <p><strong>Additional Resources:</strong></p>
                              <ul>
                                <li><a href="http://www.stanleyducharme.com" target="_blank">Dr. Stan Ducharme</a></li>
                                <li><a href="http://mitchelltepper.com" target="_blank">Mitchell S. Tepper, PhD, MPH</a></li>
                              </ul>
                              <p>These sites may contain health- or medical-related materials that are sexually explicit. If you find these materials offensive, you may not want to use them. The site and the content are provided on an “as is” basis.</p>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn-prev">&lt; Prev</button>
                              <button type="button" class="btn-next">Next ></button>
                              <button type="button" class="btn-default" data-dismiss="modal">Close</button>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!--MODAL#6 ENDS-->

                      <!--Block 7-->
                      <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="spacer">
                        <div class="toolkit-icon">
                          <img src="<?php bloginfo('url'); ?>/media/toolkit7.png" alt="Spasticity">
                        </div>
                        <div class="toolkit-text" data-mh="toolkit-text">
                        <h3>Spasticity</h3>
                        <p>Individuals and their appropriate family member(s) or partner(s) will be offered age specific education and counseling regarding medical and psychosocial issues related to sexual health and function after an SCI.</p>
                      </div>
                      <a href="#" data-toggle="modal" data-target="#largeModal7">Learn More</a>
                      </div>
                    </div>
                      <!--MODAL#7 BEGINS-->
                      <div class="modal fade" id="largeModal7" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title" id="myModalLabel">7. Spasticity</h4>
                            </div>
                            <div class="modal-body">
                              <p><a href="https://www.gaylord.org/Portals/0/PDFS/SCI%20Toolkit/2016_7%20Spasticity%20Sum.pdf" target="_blank">[Print Friendly Version]</a></p>
                              <p><strong>7.1.  STANDARD:</strong> Staff members will identify spasticity and provide intervention as appropriate. Intervention will include identifying and addressing potential triggers, providing medical and therapeutic care as needed and monitoring patient’s status.</p>
                              <p><strong>Minimum Requirements:</strong></p>
                              <ul>
                                <li>Facility has a process to educate staff members re: the identification, evaluation and management of spasticity using an inter-disciplinary approach</li>
                                <li>Staff competency conducted for all care providers</li>
                                <li>Maintain an open discussion and provide access to education about spasticity in both formal and informal settings throughout the treatment continuum</li>
                                <li>Care providers may offer referral to the appropriate rehabilitation professional in collaboration with the medical provider</li>
                              </ul>
                              <p><strong>Recommended Resources:</strong></p>
                              <ul>
                                <li><a href="http://tinyurl.com/m5w3yo3" target="_blank">Christopher and Dana Reeve Foundation Paralysis Resource Guide</a></li>
                                <li><a href="http://tinyurl.com/lsqbezc" target="_blank">MSKTC SCI Factsheet Spasticity and Spinal Cord Injury</a></li>
                                <li><a href="http://tinyurl.com/mxc3azy" target="_blank">FacingDisability.com</a></li>
                                <li><a href="http://tinyurl.com/kv8qu4b" target="_blank">eLearn</a> A web-based teaching and educational resource</li>
                              </ul>

                              <p><strong>Additional Resources:</strong></p>
                              <ul>
                                <li>Dr. Stan Ducharme www.stanleyducharme.com</li>
                                <li>Mitchell S. Tepper, PhD, MPH http://mitchelltepper.com</li>
                              </ul>
                              <p>These sites may contain health- or medical-related materials that are sexually explicit. If you find these materials offensive, you may not want to use them. The site and the content are provided on an “as is” basis.</p>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn-prev">&lt; Prev</button>
                              <button type="button" class="btn-next">Next ></button>
                              <button type="button" class="btn-default" data-dismiss="modal">Close</button>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!--MODAL#7 ENDS-->
                      </div>
                    </div><!--End of Toolkit DIV-->

                  </div>
                </div>
              </div>
            </div>
            <!--End Tab 1-->

            <!--Start Tab 2-->
            <div id="tabs-2">
              <div class="tab-pane" id="resource-referral">
                <div class="resource-snerscic-content">
                  <div class="row">

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                      <div class="mid-banner-referral-content">
                        <p>One of our main goals is to ensure each and every person affected by SCI receives
                          the information and resources they need when they need them.  We help connect people
                          to community and national resources to empower individuals to advocate for themselves
                          to lead independent and meaningful lives.
                        </p>
                        <p>The SNERSCIC team provides ongoing informational referrals to people living with SCI,
                          their families and friends, caregivers, and healthcare professionals, as well as PCPs
                          and care providers at hospitals, rehabilitation facilities, and nursing facilities
                          throughout New England.
                        </p>
                        <span>For resource referral, please feel free to contact: </span>
                        <h2>Dave Estrada</h2>
                        <div class="referral-contact">
                          <span>SNERSCIC Program Manager</span>
                          <span>857-225-2472</span>
                          <a href="#">destrada@partners.org</a>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
            <!--End Tab 2-->


          <div id="tabs-3">
              <div class="tab-pane" id="resource-list">
                <div class="resource-snerscic-content">

                  <div class="row">
                    <div class="col-lg-12">
                    <div class="resource-list-top-text">
                      <p>Have you been searching high and low for GOOD and RELEVANT resources so you can take care of some important things for living your best life?!? Then look no further! Below, we offer our most trusted local, national, and online resources under topics that matter for people living with SCI, not to mention their family, friends, and caregivers.</p>
                      <p>Healthcare professionals - never fear - this list also contains relevant resources for you. </p>
                      <p>If you could use more personalized support, feel free to request a <a href="<?php echo home_url(); ?>/resources/resource-referral/">Resource Referral</a> from our team.</p>
                    </div>
                    <div class="btn-group">
                      <a href="#" id="list"><i class="fa fa-bars" aria-hidden="true"></i>List</a>
                      <a href="#" id="grid"><i class="fa fa-th-large" aria-hidden="true"></i>Grid</a>
                    </div>

                    <div id="products" class="row list-group">
                      <!--Item 1-->
                      <div class="item col-xs-12 col-md-6 col-lg-4">
                        <div class="thumbnail">
                          <a href="#" data-toggle="modal" data-target="#resourceModal1">
                            <img class="group list-group-image" src="<?php bloginfo('url'); ?>/media/resource-list-image1.png" alt="One-Stop Shopping"/>
                            <div class="caption-grid caption-words">
                              <h4 class="group inner list-group-item-heading">One-Stop Shopping</h4>
                              <span class="links-number hide">12 <i class="fa fa-link" aria-hidden="true"></i></span>
                            </div>
                          </a>
                        </div>
                      </div>
                      <!--MODAL#1 BEGINS-->
                      <div class="modal fade" id="resourceModal1" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title" id="myModalLabel">One-Stop Shopping</h4>
                            </div>
                            <div class="modal-body">
                              <ul>
                                <li>United Spinal Association- Spinal Cord Resource Center</li>
                                <ul>
                                  <li><a href="http://www.spinalcord.org/ask-us/" target="_blank">Ask Us</a></li>
                                  <li><a href="http://www.spinalcord.org/chapters/directory/" target="_blank">United Spinal Chapters</a></li>
                                  <ul>
                                    <li><a href="http://sciboston.com/" target="_blank">Greater Boston Chapter</a></li>
                                    <li><a href="http://www.sciact.org/" target="_blank">CT Chapter</a></li>
                                    <li><a href="http://www.spinalcord.org/chapters/directory/chapter-directory/directory-category/new-hampshire/" target="_blank">NH Chapter</a></li>
                                  </ul>
                                  <li><a href="http://www.newmobility.com/" target="_blank">New Mobility Magazine</a></li>
                                </ul>
                                <li><a href="http://www.pva.org/" target="_blank">Paralyzed Veterans of America</a></li>
                                <li><a href="http://www.nepva.org/" target="_blank">New England Paralyzed Veterans of America</a></li>
                                <li><a href="https://www.christopherreeve.org/" target="_blank">Christopher and Dana Reeve Foundation</a></li>
                                <li><a href="http://www.uab.edu/medicine/sci/" target="_blank">SCI Information Network</a></li>
                                <li><a href="http://asia-spinalinjury.org/" target="_blank">American Spinal Cord Injury Association (ASIA)</a></li>
                                <li><a href="http://www.iscos.org.uk/" target="_blank">International Spinal Cord Society (ISCoS)</a></li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>

                      <!--Item 2-->
                      <div class="item col-xs-12 col-md-6 col-lg-4">
                        <div class="thumbnail">
                          <a href="#" data-toggle="modal" data-target="#resourceModal2">
                            <img class="group list-group-image" src="<?php bloginfo('url'); ?>/media/resource-list-image2.png" alt="Government Resources"/>
                            <div class="caption-grid caption-words">
                              <h4 class="group inner list-group-item-heading">Government Resources</h4><span class="links-number hide">9 <i class="fa fa-link" aria-hidden="true"></i></span>
                            </div>
                          </a>
                        </div>
                      </div>
                      <!--MODAL#2 BEGINS-->
                      <div class="modal fade" id="resourceModal2" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title" id="myModalLabel">Government Resources</h4>
                            </div>
                            <div class="modal-body">
                              <ul>
                                <li><a href="https://www.ssa.gov/disability/" target="_blank">Benefits for People with Disabilities</a></li>
                                <li><a href="https://disabilityinfo.org/" target="_blank">DisabilityInfo</a></li>
                                <li><a href="https://www.medicare.gov/" target="_blank">Medicare</a></li>
                                <li>Health Insurance through the Marketplace</li>
                                <ul>
                                  <li><a href="https://www.healthcare.gov/quick-guide/" target="_blank">A quick guide to the Health Insurance Marketplace</a></li>
                                  <li><a href="https://localhelp.healthcare.gov/#intro" target="_blank">Find someone nearby to help you apply for a Healthcare Insurance Plan</a></li>
                                  <li><a href="https://www.healthcare.gov/choose-a-plan/" target="_blank">How to pick a Health Insurance Plan</a></li>
                                  <li><a href="https://www.healthcare.gov/coverage/" target="_blank">What Marketplace Health Insurance Plans cover</a></li>
                                  <li>CT Marketplace: <a href="https://www.accesshealthct.com/AHCT/LandingPageCTHIX" target="_blank">Access Health CT</a></li>
                                  <li>MA Marketplace: <a href="https://www.mahealthconnector.org/" target="_blank">MA Health Connector</a></li>
                                </ul>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>

                      <!--Item 3-->
                      <div class="item col-xs-12 col-md-6 col-lg-4">
                        <div class="thumbnail">
                            <a href="#" data-toggle="modal" data-target="#resourceModal3">
                              <img class="group list-group-image" src="<?php bloginfo('url'); ?>/media/resource-list-image4.png" alt="Community Resources"/>
                              <div class="caption-grid caption-words">
                                <h4 class="group inner list-group-item-heading">Community Resources</h4><span class="links-number hide">8 <i class="fa fa-link" aria-hidden="true"></i></span>
                              </div>
                            </a>
                        </div>
                      </div>
                      <!--MODAL#3 BEGINS-->
                      <div class="modal fade" id="resourceModal3" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title" id="myModalLabel">Community Resources</h4>
                            </div>
                            <div class="modal-body">
                              <ul>
                                <li><a href="http://www.mass.gov/eohhs/gov/departments/mrc/" target="_blank">Massachusetts Rehabilitation Commission</a></li>
                                <li><a href="http://www.ct.gov/dors/site/default.asp" target="_blank">Connecticut Department of Rehabilitation Services</a></li>
                                <li>Independent Living Centers</li>
                                <ul>
                                  <li><a href="http://www.ilru.org/projects/cil-net/cil-center-and-association-directory-results/MA" target="_blank">Massachusetts</a></li>
                                  <li><a href="http://www.ilru.org/projects/cil-net/cil-center-and-association-directory-results/ct" target="_blank">Connecticut</a></li>
                                  <li><a href="http://www.ilru.org/projects/cil-net/cil-center-and-association-directory-results/nh" target="_blank">New Hampshire</a></li>
                                  <li><a href="http://www.ilru.org/projects/cil-net/cil-center-and-association-directory-results/RI" target="_blank">Rhode Island</a></li>
                                  <li><a href="http://www.ilru.org/projects/cil-net/cil-center-and-association-directory-results/me" target="_blank">Maine</a></li>
                                  <li><a href="http://www.ilru.org/projects/cil-net/cil-center-and-association-directory-results/vt" target="_blank">Vermont</a></li>
                                </ul>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>

                      <!--Item 4-->
                      <div class="item col-xs-12 col-md-6 col-lg-4">
                        <div class="thumbnail">
                          <a href="#" data-toggle="modal" data-target="#resourceModal4">
                            <img class="group list-group-image" src="<?php bloginfo('url'); ?>/media/resource-list-image3.png" alt="Support Groups"/>
                            <div class="caption-grid caption-words">
                              <h4 class="group inner list-group-item-heading">Support Groups</h4><span class="links-number hide">6 <i class="fa fa-link" aria-hidden="true"></i></span>
                            </div>
                          </a>
                        </div>
                      </div>
                      <!--MODAL#4 BEGINS-->
                      <div class="modal fade" id="resourceModal4" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title" id="myModalLabel">Support Groups</h4>
                            </div>
                              <div class="modal-body">
                                <ul>
                                  <li><a href="http://sciboston.com/peer-mentoring/support-group/" target="_blank">Greater Boston Chapter Support Groups</a> includes:</li>
                                  <ul>
                                    <li>Boston Area  ? Spinal Rap @ Spaulding</li>
                                    <li>Family Circle @ Spaulding</li>
                                    <li>Rhode Island</li>
                                    <li>South Shore/Cape Cod</li>
                                    <li>Springfield, MA</li>
                                    <li>Berkshire/Pittsfield, MA</li>
                                  </ul>
                                  <li><a href="http://www.sciact.org/support_grp.aspx" target="_blank">CT Chapter Support Groups</a> includes:</li>
                                  <ul>
                                    <li>Wallingford, CT @ Gaylord</a></li>
                                    <li>New Britain, CT</a></li>
                                    <li>Hartford, CT</a></li>
                                    <li>Bristol, CT</a></li>
                                  </ul>
                                  <li>Online Support Groups</li>
                                  <ul>
                                    <li><a href="http://www.apparelyzed.com/" target="_blank">Apparelyzed SCI Peer Support</a></li>
                                    <li><a href="http://sci.rutgers.edu/" target="_blank">CareCure Community</a></li>
                                    <li><a href="https://facingdisability.com/" target="_blank">Facing Disability</a></li>
                                    <li><a href="http://www.wheel-life.org/about/" target="_blank">Wheel:Life</a></li>
                                  </ul>
                                </ul>
                              </div>
                          </div>
                        </div>
                      </div>

                      <!--Item 5-->
                      <div class="item col-xs-12 col-md-6 col-lg-4">
                        <div class="thumbnail">
                            <a href="#" data-toggle="modal" data-target="#resourceModal5">
                              <img class="group list-group-image" src="<?php bloginfo('url'); ?>/media/resource-list-image5.png" alt="Exercise &amp; Fitness"/>
                              <div class="caption-grid caption-words">
                                <h4 class="group inner list-group-item-heading">Exercise &amp; Fitness</h4><span class="links-number hide">11 <i class="fa fa-link" aria-hidden="true"></i></span>
                              </div>
                            </a>
                        </div>
                      </div>
                      <!--MODAL#5 BEGINS-->
                      <div class="modal fade" id="resourceModal5" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title" id="myModalLabel">Exercise &amp; Fitness</h4>
                            </div>
                            <div class="modal-body">
                              <ul>
                                <li>MA Accessible Fitness Centers</li>
                                <ul>
                                  <li><a href="http://spauldingrehab.org/research-and-clinical-trials/exercise-persons-disabilities" target="_blank">ExPD program @ Spaulding Cambridge</a></li>
                                  <li><a href="https://journey-forward.org/" target="_blank">Journey Forward</a></li>
                                  <li><a href="http://ssymca.org/quincy/" target="_blank">Quincy YMCA</a> 617.479.8500  Sheryl Rosa</li>
                                  <li>Brighton, Oak Square YMCA  978-264-0985  <a href="mailto:Betty@accessportamerica.org" target="_blank">Betty Miller</a></li>
                                </ul>
                                <li>CT Accessible Fitness Centers</li>
                                <ul>
                                  <li><a href="http://www.wallingfordymca.org/" target="_blank">Wallingford YMCA </a></li>
                                  <li><a href="http://www.cccymca.org/" target="_blank">Central CT Coast YMCAs </a></li>
                                  <li><a href="http://www.ghymca.org/index.cfm" target="_blank">Greater Hartford </a></li>
                                  <li><a href="http://lifedesignsreabilitycenter.com/" target="_blank">Life Designs ReAbility Center </a></li>
                                  <li><a href="https://oakhillct.org/Chapter-126" target="_blank">Chapter 126 </a></li>
                                </ul>
                                <li>Online Resources</li>
                                <ul>
                                  <li><a href="http://www.msktc.org/sci/model-system-centers" target="_blank">Fact Sheet: SCI and Risk of Bone Fracture</a></li>
                                  <li>SCI-Ex Mobile Fitness App: Free download for people with SCI in the Apple iTunes and Google Play stores (search "Shepherd Center")</li>
                                  <li><a href="https://members.aapmr.org/AAPMR/AAPMR_FINDER.aspx" target="_blank">National Center for Health &amp; Physical Activity in Disability</a></li>
                                </ul>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>

                      <!--Item 6-->
                      <div class="item col-xs-12 col-md-6 col-lg-4">
                        <div class="thumbnail">
                          <a href="#" data-toggle="modal" data-target="#resourceModal6">
                            <img class="group list-group-image" src="<?php bloginfo('url'); ?>/media/resource-list-image6.png" alt="Healthcare"/>
                            <div class="caption-grid caption-words">
                              <h4 class="group inner list-group-item-heading">Healthcare</h4><span class="links-number hide">9 <i class="fa fa-link" aria-hidden="true"></i></span>
                            </div>
                          </a>
                        </div>
                      </div>
                      <!--MODAL#6 BEGINS-->
                      <div class="modal fade" id="resourceModal6" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title" id="myModalLabel">Healthcare</h4>
                            </div>
                            <div class="modal-body">
                              <ul>
                                <li><a href="http://www.msktc.org/sci/model-system-centers" target="_blank">SCI Model Care Systems</a></li>
                                <li><a href="http://search.naric.com/public/choosingquality.pdf" target="_blank">Choosing a High-Quality Medical Rehabilitation Program</a></li>
                                <li><a href="https://members.aapmr.org/AAPMR/AAPMR_FINDER.aspx" target="_blank">Find a Physical Medicine &amp; Rehabilitation Physician (Physiatrist) near me </a></li>
                                <li><a href="https://www.takingcharge.csh.umn.edu/navigate-healthcare-system" target="_blank">Taking Charge: How to Navigate the Healthcare System</a></li>
                                <li><a href="https://www.unitedspinal.org/resource-center/askus/?pg=kb.printer.friendly&id=24" target="_blank">Advocating for Your Healthcare</a></li>
                                <li><a href="http://www.massmatch.org/help/advocacy/medical.php" target="_blank">Advocacy for Medical Needs:</a> Helpful guides for disputes/appeals and MA organizations that can help </li>
                                <li><a href="http://sci-health.org/RRTC/publications/PDF/You%20and%20Your%20Doc%20FINAL%20REVISED_5.5.08.pdf" target="_blank">You and Your Doc: A Short Guide to Your Rights and Responsibilities</a></li>
                                <li><a href="https://www.ahrq.gov/patients-consumers/patient-involvement/ask-your-doctor/questions-before-appointment.html" target="_blank">Questions to Ask Before your Healthcare Visit</a></li>
                                <li><a href="<?php bloginfo('url');?>/resources/resource-list/" target="_blank">New England Regional SCI Toolkit</a></li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>

                      <!--Item 7-->
                      <div class="item col-xs-12 col-md-6 col-lg-4">
                        <div class="thumbnail">
                          <a href="#" data-toggle="modal" data-target="#resourceModal7">
                            <img class="group list-group-image" src="<?php bloginfo('url'); ?>/media/resource-list-image7.png" alt="Consumer Guides"/>
                            <div class="caption-grid caption-words">
                              <h4 class="group inner list-group-item-heading">Consumer Guides</h4><span class="links-number hide">24 <i class="fa fa-link" aria-hidden="true"></i></span>
                            </div>
                          </a>
                        </div>
                      </div>
                      <!--MODAL#7 BEGINS-->
                      <div class="modal fade" id="resourceModal7" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title" id="myModalLabel">Consumer Guides</h4>
                            </div>
                            <div class="modal-body">
                              <p>1 -	Consortium for SCI Medicine Clinical Practice Guidelines (PVA)</p>
                              <ul>
                                <li>Bladder Management Following Spinal Cord Injury: What You Should Know<a href="http://sh-sci.org/index.php?option=com_docman&task=doc_download&gid=62&Itemid=31" target="_blank"> Download</a></li>
                                <li>Neurogenic Bowel: What You Should Know<a href="http://sh-sci.org/index.php?option=com_docman&task=doc_download&gid=61&Itemid=31" target="_blank"> Download</a></li>
                                <li>Pressure Ulcers: What You Should Know<a href="http://sh-sci.org/index.php?option=com_docman&task=doc_download&gid=68&Itemid=31" target="_blank"> Download</a></li>
                                <li>Depression: What You Should Know <a href="http://sh-sci.org/index.php?option=com_docman&task=doc_download&gid=63&Itemid=31" target="_blank"> Download</a></li>
                                <li>Autonomic Dysreflexia: What you should know <a href="http://sh-sci.org/index.php?option=com_docman&task=doc_download&gid=64&Itemid=31" target="_blank"> Download</a></li>
                                <li>Preservation of Upper Limb Function: What You Should Know<a href="http://sh-sci.org/index.php?option=com_docman&task=doc_download&gid=65&Itemid=31" target="_blank"> Download</a></li>
                                <li>Respiratory Management Following Spinal Cord Injury: What You Should Know<a href="http://sh-sci.org/index.php?option=com_docman&task=doc_download&gid=66&Itemid=31" target="_blank"></a></li>
                                <li>Sexuality and Reproductive Health in Adults with Spinal Cord Injury: What You Should Know<a href="http://sh-sci.org/index.php?option=com_docman&task=doc_download&gid=67&Itemid=31" target="_blank"> Download</a></li>
                                <li>Expected Outcomes after C1-C3 SCI: What You Should Know<a href="http://sh-sci.org/index.php?option=com_docman&task=doc_download&gid=51&Itemid=31" target="_blank"> Download</a></li>
                                <li>Expected Outcomes after C4 SCI: What You Should Know<a href="http://sh-sci.org/index.php?option=com_docman&task=doc_download&gid=52&Itemid=31" target="_blank"> Download</a></li>
                                <li>Expected Outcomes after C5 SCI: What You Should Know<a href="http://sh-sci.org/index.php?option=com_docman&task=doc_download&gid=56&Itemid=31" target="_blank"> Download</a></li>
                                <li>Expected Outcomes after C6 SCI: What You Should Know<a href="http://sh-sci.org/index.php?option=com_docman&task=doc_download&gid=57&Itemid=31" target="_blank"> Download</a></li>
                                <li>Expected Outcomes after C7-C8 SCI: What You Should Know<a href="http://sh-sci.org/index.php?option=com_docman&task=doc_download&gid=53&Itemid=31" target="_blank"> Download</a></li>
                                <li>Expected Outcomes after T1-T9 SCI: What You Should Know<a href="http://sh-sci.org/index.php?option=com_docman&task=doc_download&gid=54&Itemid=31" target="_blank"> Download</a></li>
                                <li>Expected Outcomes after T10-L1 SCI: What You Should Know<a href="http://sh-sci.org/index.php?option=com_docman&task=doc_download&gid=55&Itemid=31" target="_blank"> Download</a></li>
                                <li>Expected Outcomes after L2-S5 SCI: What You Should Know<a href="http://sh-sci.org/index.php?option=com_docman&task=doc_download&gid=58&Itemid=31" target="_blank"> Download</a></li>
                              </ul>
                              <p>2 - Yes, You Can! A Guide to Self-Care for Persons with SCI (Courtesy of PVA)</p>
                              <ul>
                                  <li>Part 1 - How SCI Affects Your Body p1-31<a href="http://sh-sci.org/index.php?option=com_docman&task=doc_download&gid=78&Itemid=31" target="_blank"> Download</a></li>
                                  <li>Part 2 - How SCI Affects Your Body/ Maximizing Your Function p32-72<a href="http://sh-sci.org/index.php?option=com_docman&task=doc_download&gid=79&Itemid=31" target="_blank"> Download</a></li>
                                  <li>Part 3 - Maximizing Your Function p73-91<a href="http://sh-sci.org/index.php?option=com_docman&task=doc_download&gid=80&Itemid=31" target="_blank"> Download</a></li>
                                  <li>Part 4 - Maximizing Your Function p92-109<a href="http://sh-sci.org/index.php?option=com_docman&task=doc_download&gid=81&Itemid=31" target="_blank"> Download</a></li>
                                  <li>Part 5 - Maximizing Your Function p110-118<a href="http://sh-sci.org/index.php?option=com_docman&task=doc_download&gid=82&Itemid=31" target="_blank"> Download</a></li>
                                  <li>Part 6 - Coping and Living with SCI p119-132<a href="http://sh-sci.org/index.php?option=com_docman&task=doc_download&gid=83&Itemid=31" target="_blank"> Download</a></li>
                                  <li>Part 7 - Coping and Living with SCI p133-141<a href="http://sh-sci.org/index.php?option=com_docman&task=doc_download&gid=84&Itemid=31" target="_blank"> Download</a></li>
                                  <li>Part 8 - Coping and Living with SCI p142-148<a href="http://sh-sci.org/index.php?option=com_docman&task=doc_download&gid=85&Itemid=31" target="_blank"> Download</a></li>
                                  <li>Part 9 - Coping and Living with SCI/ Staying Healthy after SCI p149-192<a href="http://sh-sci.org/index.php?option=com_docman&task=doc_download&gid=86&Itemid=31" target="_blank"> Download</a></li>
                                </ul>
                            </div>
                          </div>
                        </div>
                      </div>

                      <!--Item 8-->
                      <div class="item col-xs-12 col-md-6 col-lg-4">
                        <div class="thumbnail">
                          <a href="#" data-toggle="modal" data-target="#resourceModal8">
                            <img class="group list-group-image" src="<?php bloginfo('url'); ?>/media/resource-list-image8.png" alt="Information for Healthcare Providers"/>
                            <div class="caption-grid caption-words">
                              <h4 class="group inner list-group-item-heading">Information for Healthcare Providers</h4><span class="links-number hide">7 <i class="fa fa-link" aria-hidden="true"></i></span>
                            </div>
                          </a>
                        </div>
                      </div>
                      <!--MODAL#8 BEGINS-->
                      <div class="modal fade" id="resourceModal8" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title" id="myModalLabel">Information for Healthcare Providers</h4>
                            </div>
                            <div class="modal-body">
                              <p>1 -	Consortium for SCI Medicine Clinical Practice Guidelines (PVA)</p>
                              <ul>
                                <li>Acute Management of Autonomic Dysreflexia: Individuals with SCI Presenting to Health-Care Facilities:  A Clinical Practice Guideline <a href="http://sh-sci.org/index.php?option=com_docman&task=doc_download&gid=73&Itemid=31" target="_blank"> Download</a></li>
                                <li>Bladder Management for Adults with Spinal Cord Injury: A Clinical Practice Guideline <a href="http://sh-sci.org/index.php?option=com_docman&task=doc_download&gid=72&Itemid=31" target="_blank">Download</a></li>
                                <li>Early Acute Management in Adults with Spinal Cord Injury: A Clinical Practice Guideline <a href="http://sh-sci.org/index.php?option=com_docman&task=doc_download&gid=74&Itemid=31" target="_blank">Download</a></li>
                                <li>Preservation of Upper Limb Function Following SCI?For Healthcare Providers <a href="http://sh-sci.org/index.php?option=com_docman&task=doc_download&gid=76&Itemid=31" target="_blank">Download</a></li>
                                <li>Respiratory Management Following Spinal Cord Injury: A Clinical Practice Guideline <a href="http://sh-sci.org/index.php?option=com_docman&task=doc_download&gid=75&Itemid=31" target="_blank">Download</a></li>
                                <li>Sexuality and Reproductive Health in Adults with Spinal Cord Injury: A Clinical Practice <a href="http://sh-sci.org/index.php?option=com_docman&task=doc_download&gid=71&Itemid=31" target="_blank">Download</a></li>
                              </ul>
                              <p>2 -	<a href="<?php bloginfo('url'); ?>/resources/new-england-sci-toolkit-nescit/">New England Regional SCI Toolkit</a></p>
                            </div>
                          </div>
                        </div>
                      </div>

                      <!--Modal 9-->
                      <div class="item col-xs-12 col-md-6 col-lg-4">
                        <div class="thumbnail">
                          <a href="#" data-toggle="modal" data-target="#resourceModal9">
                            <img class="group list-group-image" src="<?php bloginfo('url'); ?>/media/resource-list-image9.png" alt="Assistive Technology (AT)"/>
                            <div class="caption-grid caption-words">
                              <h4 class="group inner list-group-item-heading">Assistive Technology (AT)</h4><span class="links-number hide">21 <i class="fa fa-link" aria-hidden="true"></i></span>
                            </div>
                          </a>
                        </div>
                      </div>
                      <!--MODAL#9 BEGINS-->
                      <div class="modal fade" id="resourceModal9" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title" id="myModalLabel">Assistive Technology (AT)</h4>
                            </div>
                            <div class="modal-body">
                              <ul>
                                <li><a href="http://www.massmatch.org/aboutus/what_is_at.php" target="_blank">What is Assistive Technology?</a></li>
                                <li><a href="http://www.ataporg.org/programs" target="_blank">Find your state?s AT Act Program</a></li>
                                <li><a href="http://sh-sci.org/index.php?option=com_content&view=article&id=55:assistive-technology&catid=4:resources&Itemid=5" target="_blank"></a>Read more...
                                  <ul>
                                    <li>Where can I find AT?
                                      <ul>
                                        <li>Demonstration Centers of AT ? <a href="http://www.massmatch.org/find_at/demo_at.php" target="_blank">MA</a>,  <a href="http://cttechact.com/device-demo/#sthash.PCZKxlfV.hz4ql3Bq.dpbs" target="_blank">CT</a></li>
                                        <li>Borrowing AT ? <a href="http://www.massmatch.org/find_at/borrow.php" target="_blank">MA</a>, <a href="http://cttechact.com/loan/#sthash.LZ9sO3YG.4tRGYkhq.dpbs" target="_blank">CT</a></li>
                                        <li>AT services directory ? <a href="http://www.massmatch.org/find_at/search_provider.php" target="_blank">MA</a>, <a href="http://cttechact.com/resources/#sthash.jgTz0V1R.29I1rftu.dpbs" target="_blank">CT</a></li>
                                        <li>AT Swap and Shop - <a href="http://www.getatstuff.com/" target="_blank">GetATStuff.com</a> ? New England</li>
                                        <li>AT Reutilization for free refurbished AT ? <a href="http://www.dmerequipment.org/" target="_blank">MA</a>, <a href="http://cttechact.com/reutilization/#sthash.S1hqIBKM.yNy1grJh.dpbs" target="_blank">CT</a></li>
                                        <li><a href="http://atschoolshare.org/" target="_blank">MassMATCH School Share</a> ? MA and RI only</li>
                                        <li>AT purchase from <a href="http://www.massmatch.org/find_at/at_products.php" target="_blank">these sites</a></li>
                                      </ul>
                                    </li>
                                    <li>How can I pay for AT?
                                      <ul>
                                        <li>Is it medically necessary?
                                          <ul>
                                            <li>?If yes (or you?re unsure) ? you can start with <a href="http://www.massmatch.org/fund_at/medical/index.php" target="_blank">this resource.?</a></li>
                                          </ul>
                                        </li>
                                        <li>Information on Public and Private insurance provided by <a href="http://dati.org/funding/insurance_basics.html" target="_blank">DATI?The Delaware Assistive Technology Initiative.</a></li>
                                        <li>Is it for home, work or school?
                                          <ul>
                                            <li><a href="http://www.massmatch.org/fund_at/bypurpose/home/index.php" target="_blank">For home/community</a></li>
                                            <li><a href="http://www.massmatch.org/fund_at/bypurpose/work/index.php" target="_blank">For work</a></li>
                                            <li><a href="http://www.massmatch.org/fund_at/bypurpose/school/index.php" target="_blank">For school</a></li>
                                          </ul>
                                        </li>
                                        <li>Is it for Telecommunications?
                                          <ul>
                                            <li><a href="http://www.massmatch.org/fund_at/telecommunications/index.php" target="_blank">This resource</a> outlines the type of telecommunications technology. </li>
                                          </ul>
                                        </li>
                                        <li>What else is available?
                                          <ul>
                                            <li><a href="http://www.massatloan.org/" target="_blank">Massachusetts Assistive Technology Loan Program (MATLP)</a></li>
                                            <li><a href="http://www.mass.gov/eohhs/consumer/disability-services/housing-disability/home-mod-loan/" target="_blank">Home Modification Loan Program (HMLP)</a></li>
                                          </ul>
                                        </li>
                                      </ul>
                                    </li>
                                  </ul>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>

                      <!--Item 10-->
                      <div class="item col-xs-12 col-md-6 col-lg-4">
                        <div class="thumbnail">
                          <a href="#" data-toggle="modal" data-target="#resourceModal10">
                            <img class="group list-group-image" src="<?php bloginfo('url'); ?>/media/resource-list-image10.png" alt="Travel &amp; Recreation"/>
                            <div class="caption-grid caption-words">
                              <h4 class="group inner list-group-item-heading">Travel &amp; Recreation</h4><span class="links-number hide">32 <i class="fa fa-link" aria-hidden="true"></i></span>
                            </div>
                          </a>
                        </div>
                      </div>
                      <!--MODAL#10 BEGINS-->
                      <div class="modal fade" id="resourceModal10" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title" id="myModalLabel">Travel &amp; Recreation</h4>
                            </div>
                            <div class="modal-body">
                              <ul>
                                <li><a href="http://sh-sci.org/index.php?option=com_docman&task=doc_download&gid=118&Itemid=31" target="_blank">Accessible Air Travel- A Guide for People with Disabilities (United Spinal)</a></li>
                                <li><a href="https://www.tsa.gov/travel/passenger-support" target="_blank">TSA Cares Helpline for Travelers with Disabilities</a></li>
                                <li><a href="https://www.tsa.gov/travel/special-procedures" target="_blank">TSA Screening Procedures for People with Disabilities and Medical Conditions</a></li>
                                <li><a href="https://www.christopherreeve.org/living-with-paralysis" target="_blank">Mobile Apps for accessible routes when traveling (CDRF)</a></li>
                                <li><a href="http://www.wheelchairtraveling.com/" target="_blank">http://www.wheelchairtraveling.com/</a></li>
                                <li><a href="http://www.executiveclasstravelers.com/1/" target="_blank">Disabled Travelers Accessible Travel Information</a></li>
                                <li><a href="http://emerginghorizons.com/" target="_blank">Emerging Horizons- Your Accessible Travel News Source</a></li>
                                <li><a href="http://www.disabilitytravel.com/" target="_blank">Accessible Journeys- Making the World More Accessible</a></li>
                                <li><a href="http://www.accesstours.org/index.html" target="_blank">Access Tours- Travel the American Western National Parks</a></li>
                                <li><a href="http://www.sagetraveling.com/" target="_blank">Sage Traveling- The European Disabled Travel Experts</a></li>
                                <li><a href="http://sath.org/index.php" target="_blank">Society for Accessible Travel &amp; Hospitality (SATH)</a></li>
                                <li><a href="https://www.christopherreeve.org/living-with-paralysis/home-travel" target="_blank">Travel tips from the Reeve Foundation</a></li>
                                <li>Community, Leisure and Travel <a href="http://sh-sci.org/index.php?option=com_docman&task=doc_download&gid=60&Itemid=31" target="_blank">Download</a></li>
                                <li><a href="http://www.usatechguide.org/articledisplay.php?artid=22" target="_blank">Wheelchair Sports, Recreation, and Accessible Travel  (United Spinal?s Techguide)</a></li>
                                <li>New England Adaptive Recreation and Sports
                                  <ul>
                                    <li><a href="http://www.alloutadventures.org/" target="_blank">All Out Adventures </a>(MA) 413-527-8980</li>
                                    <li><a href="http://www.capeableadventures.org/" target="_blank">CAPEable Adventures </a>(MA)  508-566-3298</li>
                                    <li><a href="http://www.communityrowing.org/programs/" target="_blank">Community Rowing </a>(MA)  617-779-8267</li>
                                    <li><a href="http://www.community-boating.org/programs/universal-access-program/" target="_blank">Community Boating, Inc. </a>(MA)  617-523-1038</li>
                                    <li><a href="https://www.gaylord.org/About-Gaylord/Sports-Association" target="_blank">Gaylord Sports Association </a>(CT)  203-284-2772</li>
                                    <li><a href="https://www.maineadaptive.org/programs/" target="_blank">Maine Adaptive Sports and Recreation   </a>207-824-2440;  800-639-7770 (toll free)</li>
                                    <li><a href="http://nedisabledsports.org/" target="_blank">New England Disabled Sports </a>(NH)  603-745-6281</li>
                                    <li><a href="http://nehsa.org/" target="_blank">New England Handicapped Sports Assn.  </a>(NH)  603-763-9158 </li>
                                    <li><a href="http://www.nepva.org/" target="_blank">Northeast Passage </a>(NH, VT, MA, RI, CT)  800-660-1181</li>
                                    <li><a href="http://www.newaa.org/" target="_blank">New England Wheelchair Athletic Assn. </a>(MA)  781-830-8751</li>
                                    <li><a href="http://nepassage.org/" target="_blank">Northeast Passage </a>(NH)  603-862-0070</li>
                                    <li><a href="http://piersparksailing.org/adaptive-sailing" target="_blank">Piers Park Sailing </a>(MA)  617-561-6677</li>
                                    <li><a href="http://sailtoprevail.org/" target="_blank">Sail to Prevail </a>(RI)  401-849-8898</li>
                                    <li><a href="http://www.usahockey.com/sledhockey" target="_blank">Sled Hockey USA </a>(CT, NH, MA)  800-360-2009</li>
                                    <li><a href="http://www.stride.org/sports-programs/" target="_blank">Stride Adaptive Sports </a>(NY)  518-598-1279</li>
                                    <li><a href="http://www.mass.gov/eea/agencies/dcr/massparks/accessibility/" target="_blank">Universal Access </a>(DCR)   617-626-1294; 413-545-5758</li>
                                    <li><a href="https://everyoneoutdoors.blogspot.com/" target="_blank">Everyone Outdoors blog</a></li>
                <!--  LINK IS DEAD  <li><a href="https://www.vermontadaptive.org/" target="_blank">Vermont Adaptive Ski &amp; Sports  </a>802-786-4991</li>  -->
                                    <li>Therapeutic Riding
                                      <ul>
                                        <li><a href="https://www.windrushfarm.org/" target="_blank">Windrush Farm</a></li>
                                      </ul>
                                    </li>
                                  </ul>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>

                      <!--Item 11-->
                      <div class="item col-xs-12 col-md-6 col-lg-4">
                        <div class="thumbnail">
                          <a href="#" data-toggle="modal" data-target="#resourceModal11">
                            <img class="group list-group-image" src="<?php bloginfo('url'); ?>/media/resource-list-image11.png" alt="Transportation"/>
                            <div class="caption-grid caption-words">
                              <h4 class="group inner list-group-item-heading">Transportation</h4><span class="links-number hide">17 <i class="fa fa-link" aria-hidden="true"></i></span>
                            </div>
                          </a>
                        </div>
                      </div>
                      <!--MODAL#11 BEGINS-->
                      <div class="modal fade" id="resourceModal11" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title" id="myModalLabel">Transportation</h4>
                            </div>
                            <div class="modal-body">
                              <ul>
                                <li>Paratransit Services
                                  <ul>
                                    <li><a href="" target="_blank">Read more...</a> includes:
                                      <ul>
                                        <li>How do I know if I qualify?</li>
                                        <li>How does Paratransit Service work?</li>
                                        <li>Who pays for the Paratransit Service?</li>
                                        <li>Who offers Paratransit Services?
                                          <ul>
                                            <li><a href="https://www.mbta.com/redirect/riding_the_t/accessible_services?id=7108" target="_blank">Massachusetts</a></li>
                                            <li><a href="https://www.ripta.com/" target="_blank">Rhode Island</a></li>
                                            <li><a href="https://www.ripta.com/reduced-fare-bus-pass-program-for-low-income-seniors-and-people-with-disabilities" target="_blank">Connecticut </a></li>
                                            <li><a href="http://www.apta.com/resources/links/unitedstates/Pages/NewHampshireTransitLinks.aspx" target="_blank">New Hampshire</a></li>
                                            <li><a href="http://www.apta.com/resources/links/unitedstates/Pages/VermontTransitLinks.aspx" target="_blank">Vermont</a></li>
                                            <li><a href="http://www.exploremaine.org/newsite/bus/adaservices.shtml#8" target="_blank">Maine</a></li>
                                          </ul>
                                        </li>
                                      </ul>
                                    </li>
                                  </ul>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>

                      <!--Item 12-->
                      <div class="item col-xs-12 col-md-6 col-lg-4">
                        <div class="thumbnail">
                          <a href="#" data-toggle="modal" data-target="#resourceModal12">
                            <img class="group list-group-image" src="<?php bloginfo('url'); ?>/media/resource-list-image12.png" alt="Housing"/>
                            <div class="caption-grid caption-words">
                              <h4 class="group inner list-group-item-heading">Housing</h4><span class="links-number hide">4 <i class="fa fa-link" aria-hidden="true"></i></span>
                            </div>
                          </a>
                        </div>
                      </div>
                      <!--MODAL#12 BEGINS-->
                      <div class="modal fade" id="resourceModal12" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title" id="myModalLabel">Housing</h4>
                            </div>
                            <div class="modal-body">
                              <ul>
                                <li>Housing and Home Modifications through <a href="http://www.ilru.org/projects/cil-net/cil-center-and-association-directory" target="_blank">Independent Living Centers nationwide</a> ? <a href="http://www.mass.gov/eohhs/consumer/disability-services/living-supports/independent-living/independent-centers.html" target="_blank">MA</a>, <a href="http://www.ilru.org/projects/cil-net/cil-center-and-association-directory-results/CT" target="_blank">CT</a></li>
                                <li><a href="https://www.gaylord.org/Patient-Info/Frequently-Asked-Questions" target="_blank">Gaylord Specialty Hospital - Long Term Care FAQs</a></li>
                                </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!--Item 13-->
                      <div class="item col-xs-12 col-md-6 col-lg-4">
                        <div class="thumbnail">
                          <a href="#" data-toggle="modal" data-target="#resourceModal13">
                            <img class="group list-group-image" src="<?php bloginfo('url'); ?>/media/resource-list-image13.png" alt="Employment"/>
                            <div class="caption-grid caption-words">
                              <h4 class="group inner list-group-item-heading">Employment</h4><span class="links-number hide">13 <i class="fa fa-link" aria-hidden="true"></i></span>
                            </div>
                          </a>
                        </div>
                      </div>
                      <!--MODAL#13 BEGINS-->
                      <div class="modal fade" id="resourceModal13" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title" id="myModalLabel">Employment</h4>
                            </div>
                            <div class="modal-body">
                              <ul>
                                <li><a href="http://www.msktc.org/lib/docs/SCI_employment_final.pdf" target="_blank">Employment after Spinal Cord Injury</a></li>
                                <li><a href="https://choosework.ssa.gov/" target="_blank">Ticket to Work Helpline- Working while on Social Security</a></li>
                                <li>Vocational Rehabilitation
                                  <ul>
                                    <li><a href="http://www.mass.gov/eohhs/consumer/disability-services/vocational-rehab/vr-process.html" target="_blank">Outline of Vocational Rehabilitation Process</a></li>
                                    <li><a href="http://www.ors.ri.gov/VR.html" target="_blank">Q&amp;A for Voc Rehab</a> includes:
                                      <ul>
                                        <li>What is the Vocational Rehabilitation (VR) Program?</li>
                                        <li>Who is eligible?</li>
                                        <li>How do you obtain services? </li>
                                        <li>Developing an Employment Plan</li>
                                        <li>Services available for the Employment Plan</li>
                                        <li>What services are available after you meet your employment goal</li>
                                      </ul>
                                    </li>
                                    <li>Each state has its own office that handles Vocational Rehabilitation services:<a href="https://www.disability.gov/disability-govs-quick-links/?warn_link=d50502a7ed76423691e9d4ba85e772cc" target="_blank">State Vocational Rehabilitation Agencies</a>
                                      <ul>
                                        <li><a href="http://www.mass.gov/eohhs/gov/departments/mrc/" target="_blank">Massachusetts</a>
                                          <ul>
                                            <li><a href="http://www.mass.gov/eohhs/docs/mrc/consumer-handbook-2015.pdf" target="_blank">MRC Vocational Rehabilitation Services Consumer Handbook</a></li>
                                          </ul>
                                        </li>
                                        <li><a href="https://www.education.nh.gov/career/vocational/" target="_blank">New Hampshire</a>
                                          <ul>
                                            <li><a href="https://www.education.nh.gov/career/vocational/cust_guide.htm" target="_blank">Customer Guide to NH Voc Rehab Services </a></li>
                                          </ul>
                                        </li>
                                        <li><a href="http://portal.ct.gov/dss" target="_blank">Connecticut</a></li>
                                        <li><a href="http://www.ors.ri.gov/VR.html" target="_blank">Rhode Island</a></li>
                                        <li><a href="http://vocrehab.vermont.gov/" target="_blank">Vermont</a></li>
                                        <li><a href="http://www.maine.gov/rehab/dvr/" target="_blank">Maine</a></li>
                                      </ul>
                                    </li>
                                  </ul>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!--Item 14-->
                      <div class="item col-xs-12 col-md-6 col-lg-4">
                        <div class="thumbnail">
                          <a href="#" data-toggle="modal" data-target="#resourceModal14">
                            <img class="group list-group-image" src="<?php bloginfo('url'); ?>/media/resource-list-image14.png" alt="Caring for the Caregiver"/>
                            <div class="caption-grid caption-words">
                              <h4 class="group inner list-group-item-heading">Caring for the Caregiver</h4><span class="links-number hide">10 <i class="fa fa-link" aria-hidden="true"></i></span>
                            </div>
                          </a>
                        </div>
                      </div>
                      <!--MODAL#14 BEGINS-->
                      <div class="modal fade" id="resourceModal14" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title" id="myModalLabel">Caring for the Caregiver</h4>
                            </div>
                            <div class="modal-body">
                              <ul>
                                <li><a href="http://www.naric.com/?q=en/rif/Caring%20for%20a%20Family%20Member%20with%20Spinal%20Cord%20Injury%20Can%20Be%20Both%20Challenging%20and%20Rewarding" target="_blank">Caring for a Family Member with Spinal Cord Injury Can Be Both Challenging and Rewarding</a></li>
                                <li><a href="http://images.main.uab.edu/spinalcord/SCI%20Infosheets%20in%20PDF/Caring%20for%20Caregivers.pdf" target="_blank">Caring for Caregivers</a></li>
                                <li><a href="https://facingdisability.com/spinal-cord-injury-videos/caregiving" target="_blank">FacingDisability Short Videos on SCI Caregiving</a></li>
                                <li><a href="https://www.secondopinion-tv.org/episode/caregiver-burnout" target="_blank">Caregiver Burnout</a> - video with transcript</li>
                                <li><a href="http://www.pva.org/find-support/caregiver-support" target="_blank">Caregiver Resources from the PVA</a> ? for vets and non-vets alike to get support and tips</li>
                                <li><a href="https://www.caregiver.va.gov/toolbox/index.asp" target="_blank">Caregiver Toolbox</a></li>
                                <li><a href="https://www.unitedspinal.org/resource-center/askus/?pg=kb.book&id=9" target="_blank">Caregivers Knowledge Book</a> from United Spinal</li>
                                <li><a href="https://www.christopherreeve.org/search?q=caregiver" target="_blank">Caregiver Resources</a> from Christopher &amp; Dana Reeve Foundation</li>
                                 <li>Local Resources:
                                   <ul>
                                     <li>See <a href="http://sciboston.com/peer-mentoring/support-group/" target="_blank">Greater Boston Chapter Support Groups </a>for family support groups</li>
                                     <li><a href="http://www.sciact.org/ptc.aspx" target="_blank">Powerful Tools for Caregivers Program </a>offered by the CT Chapter</li>
                                   </ul>
                                 </li>
                               </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!--Item 15-->
                      <div class="item col-xs-12 col-md-6 col-lg-4">
                        <div class="thumbnail">
                          <a href="#" data-toggle="modal" data-target="#resourceModal15">
                            <img class="group list-group-image" src="<?php bloginfo('url'); ?>/media/resource-list-image15.png" alt="Cure Research"/>
                            <div class="caption-grid caption-words">
                              <h4 class="group inner list-group-item-heading">Cure Research</h4><span class="links-number hide">4 <i class="fa fa-link" aria-hidden="true"></i></span>
                            </div>
                          </a>
                        </div>
                      </div>
                      <!--MODAL#15 BEGINS-->
                      <div class="modal fade" id="resourceModal15" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title" id="myModalLabel">Cure Research</h4>
                            </div>
                            <div class="modal-body">
                              <ul>
                                <li>Fact Sheet: Searching for a Cure in SCI  <a href="http://sh-sci.org/index.php?option=com_docman&task=doc_download&gid=117&Itemid=31" target="_blank">Download</a></li>
                                <li><a href="https://clinicaltrials.gov/ct2/results?term=spinal+cord+injury" target="_blank">Clinical Trials in SCI</a> (including cure research)</li>
                                <li><a href="http://www.themiamiproject.org/research/what-are-clinical-trials/clinical-trials/" target="_blank">Miami Project to Cure Paralysis ? Clinical Trials</a></li>
                                <li><a href="http://sci.rutgers.edu/forum/forumdisplay.php?7-Research-Forums" target="_blank">CareCure Community - Research</a></li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!--Item 16-->
                      <div class="item col-xs-12 col-md-6 col-lg-4">
                        <div class="thumbnail">
                          <a href="#" data-toggle="modal" data-target="#resourceModal16">
                            <img class="group list-group-image" src="<?php bloginfo('url'); ?>/media/resource-list-image16.png" alt="Spaulding Rehabilitation Hospital Resources"/>
                            <div class="caption-grid caption-words">
                              <h4 class="group inner list-group-item-heading">Spaulding Rehabilitation Hospital Resources</h4><span class="links-number hide">20 <i class="fa fa-link" aria-hidden="true"></i></span>
                            </div>
                          </a>
                        </div>
                      </div>
                      <!--MODAL#16 BEGINS-->
                      <div class="modal fade" id="resourceModal16" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title" id="myModalLabel">Spaulding Rehabilitation Hospital Resources</h4>
                            </div>
                            <div class="modal-body">
                              <ul>
                                <li>Inpatient Resources
                                  <ul>
                                    <li><a href="http://spauldingrehab.org/conditions-and-treatments/assistive-technology-services" target="_blank">Assistive Technology </a></li>
                                    <li><a href="http://spauldingrehab.org/patient-and-visitor-info/case-management" target="_blank">Case Management and Discharge Planning </a></li>
                                    <li><a href="http://spauldingrehab.org/conditions-and-treatments/pastoral-care-services" target="_blank">Chapel and Pastoral Care </a></li>
                                    <li><a href="http://spauldingrehab.org/research-and-clinical-trials/exercise-persons-disabilities" target="_blank">Exercise for Persons with Disabilities Program </a></li>
                                    <li><a href="http://sciboston.com/peer-mentoring/about-peer-family-support/" target="_blank">Peer Visitor Program through the NSCIA Greater Boston Chapter</a></li>
                                    <li><a href="http://spauldingrehab.org/conditions-and-treatments/psychology-services" target="_blank">Psychiatric/Psychological Counseling </a></li>
                                    <li><a href="http://spauldingrehab.org/conditions-and-treatments/wheelchair-rehabilitation" target="_blank">Pressure Mapping, Wheelchair Clinic </a></li>
                                    <li><a href="http://spauldingrehab.org/conditions-and-treatments/speech-language-pathology" target="_blank">Inpatient Speech-Language Pathology</a></li>
                                    <li><a href="http://spauldingrehab.org/locations/rehabilitation-nursing-facilities" target="_blank">Rehabilitation Nursing</a></li>
                                    <li><a href="http://spauldingrehab.org/patient-and-visitor-info/inpatient-support" target="_blank">Support Groups</a></li>
                                    <li><a href="http://spauldingrehab.org/patient-and-visitor-info/insurance-billing-information" target="_blank">Specialized SCI resource (financial) management</a></li>
                                    <li><a href="http://spauldingrehab.org/conditions-and-treatments/therapeutic-recreation-services" target="_blank">Therapeutic Recreation</a></li>
                                    <li><a href="http://spauldingrehab.org/conditions-and-treatments/ventilator-rehabilitation" target="_blank">Ventilator weaning and use of Diaphragm Pacing</a></li>
                                  </ul>
                                </li>
                                <li>Outpatient Resources
                                  <ul>
                                    <li><a href="http://spauldingrehab.org/conditions-and-treatments/spinal-cord-injury-rehabilitation" target="_blank">Boston Outpatient Center</a></li>
                                    <li><a href="http://spauldingrehab.org/research-and-clinical-trials/assistive-technology-center" target="_blank">Boston Assistive Technology Center</a></li>
                                    <li><a href="http://spauldingrehab.org/conditions-and-treatments/adaptive-sports" target="_blank">Dr. Charles H. Weingarten Adaptive Sports and Recreation Program</a></li>
                                    <li><a href="http://spauldingrehab.org/research-and-clinical-trials/exercise-persons-disabilities" target="_blank">Exercise for Person with Disabilities Program</a></li>
                                    <li><a href="http://spauldingrehab.org/patient-and-visitor-info/support-groups" target="_blank">SCI Support Group at Spaulding</a></li>
                                    <li><a href="http://spauldingrehab.org/conditions-and-treatments/pain-management" target="_blank">Pain Management Program</a></li>
                                    <li><a href="http://spauldingrehab.org/conditions-and-treatments/wheelchair-rehabilitation" target="_blank">Wheelchair Clinic</a></li>
                                  </ul>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!--Item 17-->
                      <div class="item col-xs-12 col-md-6 col-lg-4">
                        <div class="thumbnail">
                          <a href="#" data-toggle="modal" data-target="#resourceModal17">
                            <img class="group list-group-image" src="<?php bloginfo('url'); ?>/media/resource-list-image17.png" alt="Gaylord Specialty Healthcare Resources"/>
                            <div class="caption-grid caption-words">
                              <h4 class="group inner list-group-item-heading">Gaylord Specialty Healthcare Resources</h4><span class="links-number hide">35 <i class="fa fa-link" aria-hidden="true"></i></span>
                            </div>
                          </a>
                        </div>
                      </div>
                      <!--MODAL#17 BEGINS-->
                      <div class="modal fade" id="resourceModal17" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title" id="myModalLabel">Gaylord Specialty Healthcare Resources</h4>
                            </div>
                            <div class="modal-body">
                              <ul>
                                <li>General
                                  <ul>
                                    <li><a href="https://www.gaylord.org/Contact-Us/Locations" target="_blank">Contact Gaylord </a></li>
                                    <li><a href="https://www.gaylord.org/About-Gaylord/Tour-of-Gaylord" target="_blank">Refer a Patient</a></li>
                                    <li><a href="https://www.gaylord.org/About-Gaylord/Tour-of-Gaylord" target="_blank">Pain Management Program</a></li>
                                    <li><a href="https://www.gaylord.org/Support-Gaylord/Contributions-Newsletter" target="_blank">Newsletters</a></li>
                                  </ul>
                                </li>
                                <li>Inpatient
                                  <ul>
                                    <li><a href="https://www.gaylord.org/Patient-Info/Admissions" target="_blank">Admissions</a></li>
                                    <li><a href="https://www.gaylord.org/Patient-Info/Guide-to-Your-Stay" target="_blank">Guide to Your Stay</a></li>
                                    <li><a href="https://www.gaylord.org/Patient-Info/Your-Care-Team" target="_blank">Your Care Team</a></li>
                                    <li><a href="https://www.gaylord.org/Our-Programs/Complex-Medical-Care/Complex-Wound-Program" target="_blank">Complex Wound Care</a></li>
                                    <li><a href="https://www.gaylord.org/Our-Programs/Pulmonary/Inpatient-Pulmonary-Care" target="_blank">Inpatient Pulmonary Care</a></li>
                                    <li><a href="https://www.gaylord.org/Our-Programs/Pulmonary/Ventilator-Weaning-Program" target="_blank">Vent Weaning Process</a></li>
                                    <li><a href="https://www.gaylord.org/Support-Gaylord/Facility-Dog" target="_blank">Facility Dog</a></li>
                                  </ul>
                                </li>
                                <li>Outpatient - SCI PROGRAM SPECIFIC
                                  <ul>
                                    <li><a href="https://www.gaylord.org/Our-Programs/Spinal-Cord/Ekso-Bionic-Eksoskeleton" target="_blank">Ekso Bionic Exoskeleton</a></li>
                                    <li><a href="https://www.gaylord.org/ReWalk" target="_blank">ReWalk Exoskeleton</a></li>
                                    <li><a href="https://www.gaylord.org/Our-Programs/Spinal-Cord/Outpatient-Physiatry-Services" target="_blank">Outpatient Physiatry Services</a></li>
                                    <li><a href="https://www.gaylord.org/Our-Programs/Spinal-Cord/Spinal-Cord-Injury-Adolescent-Program" target="_blank">SCI Adolescent Program</a></li>
                                    <li><a href="https://www.gaylord.org/Our-Programs/Spinal-Cord/Spinal-Cord-Injury-Resources" target="_blank">SCI Resources</a></li>
                                    <li><a href="https://www.gaylord.org/Our-Programs/Spinal-Cord/Spinal-Cord-Injury-Technology" target="_blank">SCI Technology</a></li>
                                    <li><a href="https://www.gaylord.org/Our-Programs/Spinal-Cord/Wheelchair-Assessment-Services" target="_blank">Wheelchair Assessment Services</a></li>
                                    <li><a href="https://www.gaylord.org/Our-Programs/Additional-Programs-Services/ThinkFirst-Injury-Prevention-Program" target="_blank">ThinkFirst Program</a></li>
                                    <li><a href="https://www.gaylord.org/Patient-Info/Support-Groups" target="_blank">Support Groups</a></li>
                                  </ul>
                                </li>
                                <li>All Outpatient Services
                                  <ul>
                                    <li><a href="https://www.gaylord.org/Patient-Info/Appointments" target="_blank">Outpatient Appointments</a></li>
                                    <li><a href="https://www.gaylord.org/Contact-Us/Request-an-Appointment" target="_blank">Request Appointment</a></li>
                                    <li><a href="https://www.gaylord.org/About-Gaylord/Sports-Association" target="_blank">Sports Association</a></li>
                                    <li><a href="https://www.gaylord.org/Our-Programs/Additional-Programs-Services/Therapeutic-Recreation" target="_blank">Therapeutic Recreation</a></li>
                                    <li><a href="https://www.gaylord.org/Our-Programs/Physical-Therapy-Orthopedics-and-Sports-Medicine/Aquatic-Therapy" target="_blank">Aquatic Therapy</a></li>
                                    <li><a href="https://www.gaylord.org/Our-Programs/Additional-Programs-Services/Aquacize-Classes" target="_blank">Aquacize Classes</a></li>
                                    <li><a href="https://www.gaylord.org/Our-Programs/Physical-Therapy-Orthopedics-and-Sports-Medicine/Sports-Injury-Rehab" target="_blank">Sports Injury Rehab</a></li>
                                    <li><a href="https://www.gaylord.org/Our-Programs/Physical-Therapy-Orthopedics-and-Sports-Medicine/Treatments-and-Therapies" target="_blank">Therapies and Treatments</a></li>
                                    <li><a href="https://www.gaylord.org/Our-Programs/Physical-Therapy-Orthopedics-and-Sports-Medicine/Visiting-Our-Centers" target="_blank">Visiting Our Centers</a></li>
                                    <li><a href="https://www.gaylord.org/Our-Programs/Pulmonary/Pulmonary-Program/Pulmonary-Technology" target="_blank">Pulmonary Technology</a></li>
                                    <li><a href="https://www.gaylord.org/Our-Programs/Additional-Programs-Services/Hearing-Center" target="_blank">Hearing Assessment</a></li>
                                    <li><a href="https://www.gaylord.org/Our-Programs/Additional-Programs-Services/Neuropsychology-Program" target="_blank">Neuropsychology Program</a></li>
                                    <li><a href="https://www.gaylord.org/Our-Programs/Additional-Programs-Services/Nutrition-Program" target="_blank">Nutrition Services</a></li>
                                    <li><a href="https://www.gaylord.org/Patient-Info/Library-Resources" target="_blank">Library Resources</a></li>
                                    <li><a href="https://www.gaylord.org/Patient-Info/Patient-Relations" target="_blank">Patient Relations</a></li>
                                  </ul>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>

                    </div><!--End of Products div-->

                    <div class="loader"><a href="#" id="loadMoreResources">Load More</a></div>

                  </div>
                </div>
                </div>
              </div>
            </div>
            <!--End Tab 3-->


            <!--Start Tab 4-->
            <div id="tabs-4">
              <div class="tab-pane" id="resources-news">
                <div class="resource-snerscic-content">
                  <div class="row">
                  </div>
                </div>
              </div>
            </div>
            <!--End Tab 4-->

          </div>
        </div>
      </div>
    </div>
  </div>
  </section>
</div><!--End of Tab Container-->
</div><!--End of Tabs-->

<!-- Mid Container Ends Here -->
