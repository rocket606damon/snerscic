<!-- Banner Starts Here -->
<section class="inner-banner" style="background-image: url(<?php bloginfo('url'); ?>/media/contact-banner.jpg)">
  <div class="tbl">
    <div class="tbl-cell-bottom">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 col-sm-12">
            <h1>Contact Us</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Banner Ends Here -->

<!-- Mid Container Starts Here -->
<section class="mid-container">
  <div class="container">
    <div class="row">
      <div id="contact-content" class="contact-content">
        <?php the_field('contact_top', 'option'); ?>
        <div class="col-sm-6 col-md-5 contact-form">
          <?php echo do_shortcode('[gravityform id="1" title="true" description="false" ajax="true"]'); ?>
        </div>
        <div class="col-sm-6 col-md-7 contact-info">
          <div class="address">
            <?php the_field('contact_address', 'option'); ?>
          </div>
          <div class="extra">
            <p class="standard resize"><span class="resize">Phone:</span> <?php the_field('contact_phone', 'option'); ?></p>
            <p class="standard resize"><span class="resize">Email:</span> <a href="mailto:<?php the_field('contact_email', 'option'); ?>"><?php the_field('contact_email', 'option'); ?></a></p>
          </div>
          <div class="google-map">
            <iframe src="<?php the_field('google_map', 'option'); ?>" width="500" height="450" frameborder="0" style="border:0" allowfullscreen=""></iframe>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Mid Container Ends Here -->
