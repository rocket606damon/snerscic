<!-- Banner Starts Here -->
<section class="inner-banner" style="background-image: url('<?php bloginfo('url'); ?>/media/education-banner.png')">
  <div class="tbl">
    <div class="tbl-cell-bottom">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 col-sm-12">
            <h1>SCI EDUCATION</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Banner Ends Here -->

<!-- Mid Content Starts Here -->
<section class="mid-content mid-content-blue">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
        <h2>SNERSCIC offers a variety of education opportunities because we recognize the importance of education to help empower each individual on their journey, as well as family members, friends, caregivers, and healthcare professionals.</h2>
      </div>
    </div>
  </div>
</section>
<!-- Mid Content Ends Here -->

<!-- Mid Page Links Starts Here -->
<div id="tabs" class="education-page">
<section class="page-links">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
        <ul class="links-listing">
          <li class="education-tabs-1" class="li-clicker">
            <a href="#tabs-1" class="tab-clicker" title="KiM LECTURE WITH LIVE WEBCASTS">
              <div class="tbl">
                <div class="tbl-cell">
                  <i class="fa fa-film" aria-hidden="true"></i>
                  <span>KiM LECTURE WITH LIVE WEBCASTS</span>
                </div>
              </div>
            </a>
          </li>
          <li class="education-tabs-2" class="li-clicker">
            <a href="#tabs-2" class="tab-clicker" title="Register for Next KiM Lecture/Webcast">
              <div class="tbl">
                <div class="tbl-cell">
                  <i class="fa fa-user-plus" aria-hidden="true"></i>
                  <span>REGISTER FOR NEXT KiM LECTURE/WEBCAST</span>
                </div>
              </div>
            </a>
          </li>
          <li class="education-tabs-3" class="li-clicker">
            <a href="#tabs-3" class="tab-clicker" title="Consumer Fact Sheets">
              <div class="tbl">
                <div class="tbl-cell">
                  <i class="fa fa-paperclip" aria-hidden="true"></i>
                  <span>CONSUMER FACT SHEETS</span>
                </div>
              </div>
            </a>
          </li>
          <li class="education-tabs-4" class="li-clicker">
            <a href="#tabs-4" class="tab-clicker" title="Knowledge in Motion Online Videos">
              <div class="tbl">
                <div class="tbl-cell">
                  <i class="fa fa-video-camera" aria-hidden="true"></i>
                  <span>KiM ONLINE VIDEOS</span>
                </div>
              </div>
            </a>
          </li>
          <li class="education-tabs-5" class="li-clicker">
            <a href="#tabs-5" class="tab-clicker" title="New to SCI?">
              <div class="tbl">
                <div class="tbl-cell">
                  <i class="fa fa-question" aria-hidden="true"></i>
                  <span>NEW TO SCI?</span>
                </div>
              </div>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>
<!-- Mid Page Links Ends Here -->

<!-- Mid Container Starts Here -->
<div id="tab-container" class="tab-container education-page">
  <section>
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="tab-content">

            <!--Start Tab 1-->
            <div id="tabs-1">
              <div class="tab-pane" id="kim-lecture-with-live-webcast">
                <div class="education-snerscic-content">
                  <div class="row">

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <div class="currently-recruiting-content">
                        <div class="text-center">
                          <img src="<?php bloginfo('url'); ?>/media/education-upcoming1.png"/>
                        </div>
                        <p><strong>The Big Picture:</strong></p>
                        <p>Knowledge in Motion (KiM) Lecture and Webcast Series is a dynamic lecture with live webcast hosted by SNERSCIC every 3 months
                          for individuals living with SCI, their friends and family members, and health care professionals. The KiM program allows the SCI
                          community both locally and worldwide to interact directly with leaders in the field of SCI for cutting edge research, rehabilitation,
                          treatment, and quality of life, in terms easy-to-understand.
                        </p>
                        <p>This pioneering program began in 2006 (previously called Stepping Forward- Staying Informed), at which time there was also an
                          annual community SCI research conference.
                        </p>
                        <p><strong>The Details:</strong></p>
                        <p>The KiM lecture takes place at Spaulding Rehabilitation Hospital in Boston, MA.  Each lecture is also available as a free online
                          webcast with the ability to submit questions.Each lecture/live webcast features one speaker discussing a topic of high importance
                          to spinal cord injury.
                        </p>
                        <p>This 90-minute session begins with the guest speaker presenting their work for 45 minutes, followed by a 45-minute open
                          discussion where audience members are able to speak at length with the presenter. </p>
                        <p>A light dinner is served before each lecture.</p>
                        <div class="text-center">
                          <button class="btn past-btn"><a href="<?php echo home_url(); ?>/sci-education/kim-online-videos/" target="_blank">Past recordings of <br>Education Programs</a></button>
                          <button class="btn upcoming-btn"><a href="<?php echo home_url(); ?>/sci-education/register-for-next-kim-lecturewebcast/" target="_blank">Upcoming <br>Education Programs</a></button>
                          <p><a class="mailing-list" href="#" data-toggle="modal" data-target="#mailinglist-signup">Join our Mailing List</a> and get notifications and info sent directly to your inbox.</p>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
            <!--End Tab 1-->

            <!--Start Tab 2-->
            <div id="tabs-2">
              <div class="tab-pane" id="register-for-next-kim-lecturewebcast">
                <div class="education-snerscic-content">
                  <div class="row">

                      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="text-center">
                          <img src="<?php bloginfo('url'); ?>/media/education-upcoming1.png"/>
                        </div>
                      </div>

                      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="text-center">
                          <p>We will post the next lecture/webcast as soon as it is available, we promise!
                            <br>In the meantime, for more information or to receive updates on the Knowledge in Motion SCI Education Program, join our mailing list or contact:
                          </p>
                          <p><strong>Jenny Min</strong>
                            <br>SNERSCIC Research Assistant
                            <br>617-952-6173
                            <br><a href="mailto:jmin5@partners.org">jmin5@partners.org</a>
                          </p>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>
            <!--End Tab 2-->

            <!--Start Tab 3-->
            <div id="tabs-3">
              <div class="tab-pane" id="education-factsheets">
                <div class="education-snerscic-content">
                  <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                      <div class="mid-banner-referral-content">
                        <p>The consumer materials listed below were produced through collaboration between the MSKTC and the SCI Model Systems.  This health information is based on research evidence and/or professional consensus and has been reviewed and approved by an editorial team of experts from the SCI Model Systems.</p>
                      </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                      <div class="education-content">

                        <!--Block 1-->
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                          <div class="factsheet-box">
                            <img class="factsheet-img" src="<?php bloginfo('url'); ?>/media/factsheet1.jpg" alt="Patient/Family/Caregiver Education">
                            <div class="internal-factsheet">
                              <h3>Wheelchair Information</h3>
                              <p class="factsheet-text">These 3 factsheets include important information to help you find a wheelchair that truly meets your needs.</p>
                              <a href="http://www.msktc.org/lib/docs/SCI_wheelchairs1_final.pdf" class="english"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>English</a>
                              <a href="http://www.msktc.org/lib/docs/SCI-getting_the_rigth_chair-Span_BZEdits.pdf" class="spanish"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>Spanish</a>
                            </div>
                          </div>
                        </div>

                        <!--Block 2-->
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                          <div class="factsheet-box">
                            <img class="factsheet-img" src="<?php bloginfo('url'); ?>/media/factsheet2.jpg" alt="Autonomic Dysreflexia">
                            <div class="internal-factsheet">
                              <h3>Depression &amp; SCI</h3>
                              <p class="factsheet-text">Depression affects millions of Americans, including those with SCI. Learn more about resources and treatment options.</p>
                              <a href="http://www.msktc.org/lib/docs/Factsheets/SCI_Depression_and_SCI.pdf" class="english"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>English</a>
                              <a href="http://www.msktc.org/lib/docs/Factsheets/Spanish_Factsheets/SCI_Depression_and_SCI_Sp.pdf" class="spanish"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>Spanish</a>
                            </div>
                          </div>
                        </div>

                        <!--Block 3-->
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                          <div class="factsheet-box">
                            <img class="factsheet-img" src="<?php bloginfo('url'); ?>/media/factsheet3.jpg" alt="">
                            <div class="internal-factsheet">
                              <h3>Respiratory Health</h3>
                              <p class="factsheet-text">The respiratory system changes after spinal cord injury. Learn how those changes may impact health, and how you can better manage your health.</p>
                              <a href="http://www.msktc.org/lib/docs/Factsheets/SCI_RespHealth.pdf" class="english"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>English</a>
                              <a href="http://www.msktc.org/lib/docs/Factsheets/Spanish_Factsheets/SCI_Respiratory_Health_Sp.pdf" class="spanish"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>Spanish</a>
                            </div>
                          </div>
                        </div>

                        <!--Block 4-->
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                          <div class="factsheet-box">
                            <img class="factsheet-img" src="<?php bloginfo('url'); ?>/media/factsheet4.jpg" alt="">
                            <div class="internal-factsheet">
                              <h3>Safe Transfer Techniques</h3>
                              <p class="factsheet-text">Transferring in and out of your wheelchair puts high stress on your arms and shoulders. Learn the correct way to transfer.</p>
                              <a href="http://www.msktc.org/lib/docs/Factsheets/SCI_Safe_Transfer_Techniques.pdf" class="english"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>English</a>
                              <a href="http://www.msktc.org/lib/docs/Factsheets/Spanish_Factsheets/SCI_Safe_Transfer_Techniques_Sp.pdf" class="spanish"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>Spanish</a>
                            </div>
                          </div>
                        </div>

                        <!--Block 5-->
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                          <div class="factsheet-box">
                            <img class="factsheet-img" src="<?php bloginfo('url'); ?>/media/factsheet5.jpg" alt="">
                            <div class="internal-factsheet">
                              <h3>Pregnancy</h3>
                              <p class="factsheet-text">Learn how to prepare for pregnancy, labor and delivery after a spinal cord injury.</p>
                              <a href="http://www.msktc.org/lib/docs/Factsheets/SCI_and_Pregnancy_.pdf" class="english"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>English</a>
                              <a href="http://www.msktc.org/lib/docs/Factsheets/Spanish_Factsheets/SCI_Pregnancy_Sp.pdf" class="spanish"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>Spanish</a>
                            </div>
                          </div>
                        </div>

                        <!--Block 6-->
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                          <div class="factsheet-box">
                            <img class="factsheet-img" src="<?php bloginfo('url'); ?>/media/factsheet6.jpg" alt="">
                            <div class="internal-factsheet">
                              <h3>Bladder Health</h3>
                              <p class="factsheet-text">Learn about the problems caused by your injury and the strategies used to help you manage bladder problems.</p>
                              <a href="http://www.msktc.org/lib/docs/Factsheets/SCI_Bladder_Health.pdf" class="english"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>English</a>
                              <a href="http://www.msktc.org/lib/docs/Factsheets/Spanish_Factsheets/SCI_Bladder_Management_Sp.pdf" class="spanish"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>Spanish</a>
                            </div>
                          </div>
                        </div>

                        <!--Block 7-->
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                          <div class="factsheet-box">
                            <img class="factsheet-img" src="<?php bloginfo('url'); ?>/media/factsheet7.jpg" alt="">
                            <div class="internal-factsheet">
                              <h3>Sexuality</h3>
                              <p class="factsheet-text">After SCI, Loss of muscle movement, sense of touch, and sexual reflexes. Learn about sexual functioning after SCI.</p>
                              <a href="http://www.msktc.org/lib/docs/Factsheets/SCI_Sexuality.pdf" class="english"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>English</a>
                              <a href="http://www.msktc.org/lib/docs/Factsheets/Spanish_Factsheets/SCI_Sex__Sex_Function_Sp.pdf" class="spanish"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>Spanish</a>
                            </div>
                          </div>
                        </div>

                        <!--Block 8-->
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                          <div class="factsheet-box">
                            <img class="factsheet-img" src="<?php bloginfo('url'); ?>/media/factsheet8.jpg" alt="">
                            <div class="internal-factsheet">
                              <h3>Pain after Spinal Cord Injury</h3>
                              <p class="factsheet-text">Pain is a serious problem for many people with spinal cord injuries. Learn more about options for pain management.</p>
                              <a href="http://www.msktc.org/lib/docs/Factsheets/SCI_Pain_after_SCI.pdf" class="english"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>English</a>
                              <a href="http://www.msktc.org/lib/docs/Factsheets/Spanish_Factsheets/SCI_Pain_Spanish.pdf" class="spanish"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>Spanish</a>
                            </div>
                          </div>
                        </div>

                        <!--Block 9-->
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                          <div class="factsheet-box">
                            <img class="factsheet-img" src="<?php bloginfo('url'); ?>/media/factsheet9.jpg" alt="">
                            <div class="internal-factsheet">
                              <h3>Spasticity &amp; Spinal Cord Injury</h3>
                              <p class="factsheet-text">Learn more about spasticity causes, management, and treatment options.</p>
                              <a href="http://www.msktc.org/lib/docs/Factsheets/SCI_Spasticity_and_SCI.pdf" class="english"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>English</a>
                              <a href="http://www.msktc.org/lib/docs/Factsheets/Spanish_Factsheets/SCI_Spasticity_and_SCI_Sp.pdf" class="spanish"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>Spanish</a>
                            </div>
                          </div>
                        </div>
                        <!-- <div class="loader">
                          <a href="#">Load More</a>
                        </div> -->
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
            <!--End Tab 3-->

<script>

var currentOffset = 2;
var videosCount = 0;
var segmentLength = 2;
jQuery(document).ready(function () {
	console.log('loadMoreEducation1 clicked');
    videosCount = 22;
    jQuery('#loadMoreEducation1').click(function (e) {
		console.log('loadMoreEducation1 clicked');
        if (currentOffset + segmentLength <= videosCount){
          var start = currentOffset;
          var finish = currentOffset + segmentLength;
          if (finish > videosCount) finish = videosCount;
          for(var i = start; i <= finish; i++){
            jQuery('.video-'+currentOffset).slideDown();
            currentOffset++;
          }
        }
		e.preventDefault();
    });
});
</script>

            <!--Start Tab 4-->
            <div id="tabs-4">
              <div class="tab-pane" id="kim-online-videos">
                <div class="education-snerscic-content">
                  <div class="row videos">

                    <div class="col-sm-12" style="display: block;">
                      <h2>The following lectures are available to view online at no charge.
                        Please click on any presentation you'd like to view.
                        To help us continue to offer the most effective education programs,
                        please take a moment to complete the brief online survey after the video.
                        Thank you!
                      </h2>
                    </div>

                    <!--Video 1-->
                    <div class="col-sm-6 video video-1" style="display: block;">
                      <div class="col-sm-12">
                        <div class="embed-responsive embed-responsive-16by9">
                          <iframe class="embed-responsive-item lazy" src="//www.youtube.com/embed/DJFr0OSajVs" allowfullscreen=""></iframe>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="video-info" data-mh="info">
                        <h3><a href="https://youtu.be/DJFr0OSajVs" target="_blank">
                          Complementary &amp; Integrative Healthcare (CIH) for People with SCI</a></h3>
                        <p class="video-description" data-mh="description">
                          <strong>Jennifer Coker, MPH</strong>
                          <br>Rocky Mountain Regional SCI Center Model System
                          <br>Craig Hospital, Denver, CO
                          <br>June 2017 SNERSCIC's Knowledge in Motion SCI Education Series, Boston, MA
                        </p>
                        <p>After viewing the webcast, please complete this brief <a href="https://www.surveymonkey.com/r/Jun2017SCIwebcast-CIH">online survey</a></p>
                        </div>
                      </div>
                    </div>

                    <!--Video 2-->
                    <div class="col-sm-6 video video-2" style="display: block;">
                      <div class= "col-sm-12">
                        <div class="embed-responsive embed-responsive-16by9">
                          <iframe class="embed-responsive-item lazy" src="//www.youtube.com/embed/TMi6V3KOzqw" allowfullscreen=""></iframe>
                        </div>
                      </div>
                      <div class= "col-sm-12">
                        <div class="video-info" data-mh="info">
                        <h3><a href="https://youtu.be/TMi6V3KOzqw" target="_blank">
                          The Future is Here: What can Robotics do for People with Spinal Cord Injury</a>
                        </h3>
                        <p class="video-description" data-mh="description">
                          <strong>Paulo Bonato PhD,</strong> Spaulding Rehabilitation Hospital, Harvard Medical School
                          <br>panel Participants:<strong> Tim Morris,</strong> End User;<strong> Erick Larson,</strong> End User
                          <br><strong>Katie Schramm DPT; Ann O’Brien PT; Catherine Adans-Dester RA</strong>
                          <br>April 2017 SNERSCIC Knowledge in Motion SCI Education Series, Boston, MA
                        </p>
                        <p>After viewing the webcast, please complete this brief <a href="https://www.surveymonkey.com/r/Apr2017SCIwebcast-Robotics">online survey</a></p>
                        </div>
                      </div>
                    </div>

                    <!--Video 3-->
                    <div class="col-sm-6 video video-3">
                      <div class= "col-sm-12">
                        <div class="embed-responsive embed-responsive-16by9">
                          <iframe class="embed-responsive-item lazy" src="//www.youtube.com/embed/ypxQZt0ni_E" allowfullscreen=""></iframe>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="video-info" data-mh="info">
                        <h3><a href="https://youtu.be/ypxQZt0ni_E" target="_blank">
                          Living Healthy with Spinal Cord Injury</a>
                        </h3>
                        <p class="video-description" data-mh="description">
                          <strong>Cheri Blauwet MD,</strong> Spaulding New England Regional Spinal Cord Injury Center Model System, Spaulding Rehabilitation Hospital, Harvard Medical School
                          <br><strong>J. Andrew Taylor PhD,</strong> Spaulding New England Regional Spinal Cord Injury Center Model System, Spaulding Hospital Cambridge, Harvard Medical School
                          <br><strong>Stanley Ducharme PhD,</strong> Boston Medical Center, Boston University School of Medicine
                          <br><strong>Margaret Vasquez MS, RD, LDN,</strong> , Spaulding Rehabilitation Hospital
                          <br>January 2017 SNERSCIC Knowledge in Motion SCI Education Series, Boston, MA
                        </p>
                        <p>After viewing the webcast, please complete this brief <a href="https://www.surveymonkey.com/r/Jan2017SCIwebcast-topics">online survey</a></p>
                        </div>
                      </div>
                    </div>

                    <!--Video 4-->
                    <div class="col-sm-6 video video-4">
                      <div class= "col-sm-12">
                        <div class="embed-responsive embed-responsive-16by9">
                          <iframe class="embed-responsive-item lazy" src="//www.youtube.com/embed/dyk1uifwz_Q" allowfullscreen=""></iframe>
                        </div>
                      </div>
                      <div class= "col-sm-12">
                        <div class="video-info" data-mh="info">
                        <h3><a href="https://youtu.be/dyk1uifwz_Q" target="_blank">
                          Sexuality after Spinal Cord Injury</a>
                        </h3>
                        <p class="video-description" data-mh="description">
                          <strong>Marcalee Alexander, MD</strong>
                          <br>Spaulding Rehabilitation Hospital- Telemedicine, University of Alabama School of Medicine at Birmingham, PM&amp;R, Harvard School of Medicine, PM&amp;R
                          <br>February 2015 NERSCIC &amp; S-HSCIMS Knowledge in Motion SCI Education Series, Boston, MA
                        </p>
                        <p>After viewing the webcast, please complete this brief <a href="https://www.surveymonkey.com/s/Feb2015sciwebcast">online survey</a></p>
                        </div>
                      </div>
                    </div>

                    <!--Video 5-->
                    <div class="col-sm-6 video video-5">
                      <div class= "col-sm-12">
                        <div class="embed-responsive embed-responsive-16by9">
                          <iframe class="embed-responsive-item lazy" src="//www.youtube.com/embed/KL7O7rbN7YA" allowfullscreen=""></iframe>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="video-info" data-mh="info">
                        <h3><a href="https://youtu.be/KL7O7rbN7YA" target="_blank">
                          Integrated Primary Care for People with SCI</a>
                        </h3>
                        <p class="video-description" data-mh="description">
                          <strong>Michael D.Stillman, MD,</strong> University of Louisville School of Medicine
                          <br><strong>Steve R. Williams, MD,</strong> University of Louisville School of Medicine, Frazier Rehab and Neuroscience Center SCIMS
                          <br>November 2014 NERSCIC &amp; S-HSCIMS Knowledge in Motion SCI Education Series, Boston, MA
                        </p>
                        <p>After viewing the webcast, please complete this brief <a href="https://www.surveymonkey.com/s/nov2014SCIwebcast">online survey</a></p>
                        </div>
                      </div>
                    </div>

                    <!--Video 6-->
                    <div class="col-sm-6 video video-6">
                      <div class= "col-sm-12">
                        <div class="embed-responsive embed-responsive-16by9">
                          <iframe class="embed-responsive-item lazy" src="//www.youtube.com/embed/bh9LXA8vVrI" allowfullscreen=""></iframe>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="video-info" data-mh="info">
                        <h3><a href="https://youtu.be/bh9LXA8vVrI" target="_blank">
                          Eating Well to Prevent and Manage Secondary Conditions in SCI</a>
                        </h3>
                        <p class="video-description" data-mh="description">
                          <strong>Joanne Smith B.A., BRT Dip, CNP</strong>
                          <br><strong>Kylie James B.Sc. (OT), CNP</strong>
                          <br>June 2014 NERSCIC &amp; S-HSCIMS Knowledge in Motion SCI Education Series, Boston, MA
                        </p>
                        <p>After viewing the webcast, please complete this brief <a href="https://www.surveymonkey.com/s/june2014SCIWebcast">online survey</a></p>
                        </div>
                      </div>
                    </div>

                    <!--Video 7-->
                    <div class="col-sm-6 video video-7">
                      <div class= "col-sm-12">
                        <div class="embed-responsive embed-responsive-16by9">
                          <iframe class="embed-responsive-item lazy" src="//www.youtube.com/embed/FiYVtiPPstY" allowfullscreen=""></iframe>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="video-info" data-mh="info">
                        <h3 style="font-size:16px;"><a href="https://youtu.be/FiYVtiPPstY" target="_blank">
                          Neuroregenerative Properties of Dental Pulp Stem Cells and Their Future Potential to Improve Neurological Outcomes in SCI</a>
                        </h3>
                        <p class="video-description" data-mh="description">
                          <strong>Ricardo Battaglino, PhD,</strong> Harvard School of Dental Medicine, The Forsyth Institute
                          <br><strong>Alpdogan Kantarci, DDS, PhD,</strong> The Forsyth Institute
                          <br><strong>Hatice Hasturk, DDS, PhD,</strong> The Forsyth Institute
                          <br><strong>Leslie Morse, DO,</strong> Spaulding-Harvard Spinal Cord Injury Model System
                          <br>January 2014 NERSCIC &amp; S-HSCIMS Knowledge in Motion SCI Education Series, Boston, MA
                        </p>
                        <p>After viewing the webcast, please complete this brief <a href="https://www.surveymonkey.com/s/january2014Webcast">online survey</a></p>
                        </div>
                      </div>
                    </div>

                    <!--Video 8-->
                    <div class="col-sm-6 video video-8">
                      <div class= "col-sm-12">
                        <div class="embed-responsive embed-responsive-16by9">
                          <iframe class="embed-responsive-item lazy" src="//www.youtube.com/embed/0xigSzApitQ" allowfullscreen=""></iframe>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="video-info" data-mh="info">
                        <h3><a href="https://youtu.be/0xigSzApitQ" target="_blank">
                          New Advances in Research for Recovery after SCI</a>
                        </h3>
                        <p class="video-description" data-mh="description">
                          <strong>Jeffrey Macklis, MD,</strong> Harvard Stem Cell Institute, Harvard University, Harvard Medical School
                          <br><strong>Lisa McKerracher, PhD,</strong> BioAxone Biosciences, Inc.
                          <br><strong>Angus McQuilken,</strong> Massachusetts Life Sciences Center
                          <br>June 2013 NERSCIC &amp; S-HSCIMS Knowledge in Motion SCI Education Series, Boston, MA
                        </p>
                        <p>After viewing the webcast, please complete this brief <a href="https://www.surveymonkey.com/s/June2013Webcast">online survey</a></p>
                        </div>
                      </div>
                    </div>

                    <!--Video 9-->
                    <div class="col-sm-6 video video-9">
                      <div class= "col-sm-12">
                        <div class="embed-responsive embed-responsive-16by9">
                          <iframe class="embed-responsive-item lazy" src="//www.youtube.com/embed/SNmxQIjhYD8" allowfullscreen=""></iframe>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="video-info" data-mh="info">
                        <h3><a href="https://youtu.be/SNmxQIjhYD8" target="_blank">
                          Developing a Personal Emergency Preparedness Plan for Individuals with SCI</a>
                        </h3>
                        <p class="video-description" data-mh="description">
                          <strong>Jeffrey Dougan,</strong> Assistant Director of Community Services, MA Office on Disability (MOD)
                          <br>March 2013 NERSCIC &amp; S-HSCIMS Knowledge in Motion SCI Education Series, Boston, MA
                        </p>
                        <p>After viewing the webcast, please complete this brief <a href="https://www.surveymonkey.com/s/march2013webcast">online survey</a></p>
                      </div>
                      </div>
                    </div>

                    <!--Video 10-->
                    <div class="col-sm-6 video video-10">
                      <div class= "col-sm-12">
                        <div class="embed-responsive embed-responsive-16by9">
                          <iframe class="embed-responsive-item lazy" src="//www.youtube.com/embed/qVTC3XrvoGk" allowfullscreen=""></iframe>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="video-info" data-mh="info">
                        <h3><a href="https://youtu.be/qVTC3XrvoGk" target="_blank">
                          Staying Well after SCI: What You Can Do and How We Can Help</a>
                        </h3>
                        <p class="video-description" data-mh="description">
                          <strong>Dr. Susan Bergman,</strong> Commonwealth Community Care (BCMG)
                          <br>November 2012 NERSCIC &amp; SHSCIMS Knowledge in Motion SCI Education Series, Boston, MA
                        </p>
                        <p>After viewing the webcast, please complete this brief <a href="https://www.surveymonkey.com/s/Nov2012webcast">online survey</a></p>
                        </div>
                      </div>
                    </div>

                    <!--Video 11-->
                    <div class="col-sm-6 video video-11">
                      <div class= "col-sm-12">
                        <div class="embed-responsive embed-responsive-16by9">
                          <iframe class="embed-responsive-item lazy" src="//www.youtube.com/embed/4c7tq4qGkFU" allowfullscreen=""></iframe>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="video-info" data-mh="info">
                        <h3><a href="https://youtu.be/4c7tq4qGkFU" target="_blank">
                          Adaptive Sports and Recreation for Individuals with SCI </a>
                        </h3>
                        <p class="video-description" data-mh="description">
                          <strong>Chandler Bullard, David Lee,</strong> Northeast Passage, University of New Hampshire
                          <br>May 2012 NERSCIC Lecture, Boston Medical Center, Boston, MA
                        </p>
                        </div>
                      </div>
                    </div>

                    <!--Video 12-->
                    <div class="col-sm-6 video video-12">
                      <div class= "col-sm-12">
                        <div class="embed-responsive embed-responsive-16by9">
                          <iframe class="embed-responsive-item lazy" src="//www.youtube.com/embed/xFFs5S0tRJI" allowfullscreen=""></iframe>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="video-info" data-mh="info">
                        <h3><a href="https://youtu.be/xFFs5S0tRJI" target="_blank">
                          A Continuum of Strategies Targeted at Neuroplasticity for Recovery After Neurologic Injury </a>
                        </h3>
                        <p class="video-description" data-mh="description">
                          <strong>Susan Harkema, PhD,</strong> Rehabilitation Research Director, Kentucky Spinal Cord Injury Research Center, Frazier Rehabilitation Institute
                          <br>March 2012 NERSCIC Lecture, Boston Medical Center, Boston, MA
                        </p>
                        </div>
                      </div>
                    </div>

                    <!--Video 13-->
                    <div class="col-sm-6 video video-13">
                      <div class= "col-sm-12">
                        <div class="embed-responsive embed-responsive-16by9">
                          <iframe class="embed-responsive-item lazy" src="//www.youtube.com/embed/Yy4_HSE3H1k" allowfullscreen=""></iframe>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="video-info" data-mh="info">
                        <h3><a href="https://youtu.be/Yy4_HSE3H1k" target="_blank">
                          How to Live With a Spinal Cord Injury and Osteoporosis</a>
                        </h3>
                        <p class="video-description" data-mh="description">
                          <strong>Andrei Krassioukov MD, PhD, FRCP,</strong> ICORD, University of British Columbia, GF Strong Rehabilitation Centre Vancouver, BC, Canada
                          <br>September 2011 NERSCIC Lecture, Boston Medical Center, Boston, MA
                        </p>
                        </div>
                      </div>
                    </div>

                    <!--Video 14-->
                    <div class="col-sm-6 video video-14">
                      <div class= "col-sm-12">
                        <div class="embed-responsive embed-responsive-16by9">
                          <iframe class="embed-responsive-item lazy" src="//www.youtube.com/embed/l1EcOgDqOs0" allowfullscreen=""></iframe>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="video-info" data-mh="info">
                        <h3><a href="https://youtu.be/l1EcOgDqOs0" target="_blank">
                          The Skin You’re In: An Overview of Maintaining Skin Integrity for Individuals with Spinal Cord Injury</a>
                        </h3>
                        <p class="video-description" data-mh="description">
                          <strong>Lauren Harney, RN, BSN, CWON,</strong> Boston Medical Center
                          <br>May 2011 NERSCIC Lecture, Boston Medical Center, Boston, MA
                        </p>
                        </div>
                      </div>
                    </div>

                    <!--Video 15-->
                    <div class="col-sm-6 video video-15">
                      <div class= "col-sm-12">
                        <div class="embed-responsive embed-responsive-16by9">
                          <iframe class="embed-responsive-item lazy" src="//www.youtube.com/embed/YqLPYNzt49Y" allowfullscreen=""></iframe>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="video-info" data-mh="info">
                        <h3><a href="https://youtu.be/YqLPYNzt49Y" target="_blank">
                          From Then to Now to the Future</a>
                        </h3>
                        <p class="video-description" data-mh="description">
                          <strong>Susan P. Howley,</strong> Executive Vice President, Research Christopher &amp; Dana Reeve Foundation
                          <br>October 2010 NERSCIC Annual Consumer Research Conference, Boston, MA
                        </p>
                        </div>
                      </div>
                    </div>

                    <!--Video 16-->
                    <div class="col-sm-6 video video-16">
                      <div class= "col-sm-12">
                        <div class="embed-responsive embed-responsive-16by9">
                          <iframe class="embed-responsive-item lazy" src="//www.youtube.com/embed/1qmkAfa4_zM" allowfullscreen=""></iframe>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="video-info" data-mh="info">
                        <h3><a href="https://youtu.be/1qmkAfa4_zM" target="_blank">
                          Cardiometabolic Disorders after Spinal Cord Injury; Causes, Consequences, and Effective Interventions</a>
                        </h3>
                        <p class="video-description" data-mh="description">
                          <strong>Mark S. Nash, PhD., FACSM,</strong> Professor of Neurological Surgery and Rehabilitation Medicine, Miller School of Medicine of the University of Miami, Director of Applied Physiology Research, Miami Project to Cure Paralysis, Miami, FL
                          <br>October 2010 NERSCIC Annual Consumer Research Conference, Boston, MA
                        </p>
                        </div>
                      </div>
                    </div>

                    <!--Video 17-->
                    <div class="col-sm-6 video video-17">
                      <div class= "col-sm-12">
                        <div class="embed-responsive embed-responsive-16by9">
                          <iframe class="embed-responsive-item lazy" src="//www.youtube.com/embed/dx3DWc6ZgLk" allowfullscreen=""></iframe>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="video-info" data-mh="info">
                        <h3><a href="https://youtu.be/dx3DWc6ZgLk" target="_blank">
                          Aging with Spinal Cord Injury: Implications for the Future</a>
                        </h3>
                        <p class="video-description" data-mh="description">
                          <strong>Suzanne Groah, MD, MSPH,</strong> Director, Spinal Cord Injury Research, National Rehabilitation Hospital, Washington, DC
                          <br>October 2010 NERSCIC Annual Consumer Research Conference, Boston, MA
                        </p>
                        </div>
                      </div>
                    </div>

                    <!--Video 18-->
                    <div class="col-sm-6 video video-18">
                      <div class= "col-sm-12">
                        <div class="embed-responsive embed-responsive-16by9">
                          <iframe class="embed-responsive-item lazy" src="//www.youtube.com/embed/Dl1MFLhrl7k" allowfullscreen=""></iframe>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="video-info" data-mh="info">
                        <h3><a href="https://youtu.be/Dl1MFLhrl7k" target="_blank">
                          Healthy Now, Ready for the Future: Being Prepared for Clinical Trials</a>
                        </h3>
                        <p class="video-description" data-mh="description">
                          <strong>Lindsay Huisman,</strong> Restorative Therapies
                          <br>October 2010 NERSCIC Lecture, Boston Medical Center, Boston, MA
                        </p>
                        </div>
                      </div>
                    </div>

                    <!--Video 19-->
                    <div class="col-sm-6 video video-19">
                      <div class= "col-sm-12">
                        <div class="embed-responsive embed-responsive-16by9">
                          <iframe class="embed-responsive-item lazy" src="//www.youtube.com/embed/UDPcABt9aTk" allowfullscreen=""></iframe>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="video-info" data-mh="info">
                        <h3><a href="https://youtu.be/UDPcABt9aTk" target="_blank">
                          Spinal Cord Injury: From the Moment of Injury to Leading a Full and Healthy Life</a>
                        </h3>
                        <p class="video-description" data-mh="description">
                          <strong>Dr. Steve Williams,</strong> New England Regional Spinal Cord Injury Center at Boston Medical Center
                          <br>March 2010 NERSCIC Lecture, Boston Medical Center, Boston, MA
                        </p>
                        </div>
                      </div>
                    </div>

                    <!--Video 20-->
                    <div class="col-sm-6 video video-20">
                      <div class= "col-sm-12">
                        <div class="embed-responsive embed-responsive-16by9">
                          <iframe class="embed-responsive-item lazy" src="//www.youtube.com/embed/fRzZ-keF6qk" allowfullscreen=""></iframe>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="video-info" data-mh="info">
                        <h3><a href="https://youtu.be/fRzZ-keF6qk" target="_blank">
                          What is the Massachusetts Life Sciences Center and How Does it Impact Spinal Cord Injury Research?</a>
                        </h3>
                        <p class="video-description" data-mh="description">
                          <strong>Dr. Susan Windham-Bannister,</strong> Massachusetts Life Sciences Center
                          <br>January 2010 NERSCIC Lecture, Boston Medical Center, Boston, MA
                        </p>
                        </div>
                      </div>
                    </div>

                    <!--Video 21-->
                    <div class="col-sm-6 video video-21">
                      <div class= "col-sm-12">
                        <div class="embed-responsive embed-responsive-16by9">
                          <iframe class="embed-responsive-item lazy" src="//www.youtube.com/embed/dw78qvs4Ris" allowfullscreen=""></iframe>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="video-info" data-mh="info">
                        <h3><a href="https://youtu.be/dw78qvs4Ris" target="_blank">
                          The Benefits of Exercise for Those with Spinal Cord Injury</a>
                        </h3>
                        <p class="video-description" data-mh="description">
                          <strong>Brett Fechter, Mike Rollins,</strong> Journey Forward, Canton, MA
                          <br>November 2009 NERSCIC Lecture, Boston Medical Center, Boston, MA
                        </p>
                        </div>
                      </div>
                    </div>

                    <!--Video 22-->
                    <div class="col-sm-6 video video-22">
                      <div class= "col-sm-12">
                        <div class="embed-responsive embed-responsive-16by9">
                          <iframe class="embed-responsive-item lazy" src="//www.youtube.com/embed/Hqh15fj31Ds" allowfullscreen=""></iframe>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="video-info" data-mh="info">
                        <h3><a href="https://youtu.be/Hqh15fj31Ds" target="_blank">
                          The Impact of Obesity in Spinal Cord Injury</a>
                        </h3>
                        <p class="video-description" data-mh="description">
                          <strong>Dr. Rocco Chiappini,</strong> Crotched Mountain, Greenfield, NH
                          <br>November 2008 NERSCIC Lecture, Boston Medical Center, Boston, MA
                        </p>
                        </div>
                      </div>
                    </div>

                    <div class="load-more col-sm-12 text-center">
                      <a href="#" id="loadMoreEducation1" class="load-more-videos blue-btn btn">Load More Videos</a>
                    </div>

                  </div>
                </div>
              </div>
            </div>

            <!--Start Tab 5-->
            <div id="tabs-5">
              <div class="tab-pane" id="new-to-sci">
                <div class="education-snerscic-content">
                  <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <div class="currently-recruiting-content">
                        <p>When a person is first injured – and for awhile after – things can feel very overwhelming for everyone involved.   Here are a few resources to help you when you need it most.</p>
                        <br><p>Please be sure to <a href="<?php echo home_url(); ?>/contact-us/" title="Contact Us">Contact Us</a> if we can help you in any way whatsoever, even if you’re not sure how we could help – contact us anyway! </p>
                        <p><strong>Basic Resources for Anyone Affected by A New Injury</strong></p>

                        <ul>
                          <li>What does my level of injury mean?  (SCI Terms and SC Anatomy)
                            <ul>
                              <li>See UAB’s <a href="http://www.uab.edu/medicine/sci/information-for-the-newly-injured" target="_blank">SCI Info for the Newly Injured</a></li>
                            </ul>
                          </li>
                          <li>Who can I talk to who understands and can help?
                            <ul>
                              <li>Support Groups – in-person and online
                                <ul>
                                  <li><a href="http://sciboston.com/peer-mentoring/support-group/" target="_blank">Greater Boston Chapter Support Groups</a> includes:
                                    <ul>
                                      <li>Boston Area  – Spinal Rap @ Spaulding</li>
                                      <li>Family Circle @ Spaulding</li>
                                      <li>Rhode Island</li>
                                      <li>South Shore/Cape Cod</li>
                                      <li>Springfield, MA</li>
                                      <li>Berkshire/Pittsfield, MA</li>
                                    </ul>
                                  </li>
                                  <li><a href="http://www.sciact.org/support_grp.aspx" target="_blank">CT Chapter Support Groups</a> includes:
                                    <ul>
                                      <li>Wallingford, CT @ Gaylord</li>
                                      <li>New Britain, CT</li>
                                      <li>Hartford, CT</li>
                                      <li>Bristol, CT</li>
                                    </ul>
                                  </li>
                                  <li><a href="http://www.spinalcord.org/chapters/directory/chapter-directory/directory-category/new-hampshire/" target="_blank">NH Chapter</a></li>
                                  <li>Online Support Groups
                                    <ul>
                                      <li><a href="http://www.apparelyzed.com/" target="_blank">Apparelyzed SCI Peer Support</a></li>
                                      <li><a href="http://sci.rutgers.edu/" target="_blank">CareCure Community</a></li>
                                      <li><a href="https://facingdisability.com/" target="_blank">Facing Disability</a></li>
                                      <li><a href="http://www.wheel-life.org/about/" target="_blank">Wheel:Life</a></li>
                                    </ul>
                                  </li>
                                  <li><a href="http://www.spinalcord.org/chapters/directory/" target="_blank">United Spinal Chapters for Peer Support</a></li>
                                  <li>Christopher &amp; Dana Reeve Foundation – <a href="https://www.christopherreeve.org/get-support/get-a-peer-mentor" target="_blank">Request a peer mentor</a></li>
                                </ul>
                              </li>
                              <li>Resource and Support Call-in Centers
                                <ul>
                                  <li><a href="https://unitedspinal.org/ask-us/" target="_blank">United Spinal – Ask Us</a></li>
                                  <li><a href="https://craighospital.org/resources/nurse-advice-line" target="_blank">Craig Hospital Nurse Advice Line</a></li>
                                  <li><a href="https://www.christopherreeve.org/get-support/ask-us-anything" target="_blank">Christopher &amp; Dana Reeve Foundation – Ask Us Anything</a></li>
                                  <li><a href="http://www.pva.org/find-support/veterans-assistance" target="_blank">Paralyzed Veterans of America – Find Support</a></li>
                                </ul>
                              </li>
                            </ul>
                          </li>
                          <li>Where can I find info about…  (New Injury Resources)
                            <ul>
                              <li><a href="https://www.christopherreeve.org/living-with-paralysis/newly-paralyzed" target="_blank">Newly Paralyzed FAQs</a> - Christopher and Dana Reeve Foundation</li>
                              <li><a href="http://www.spinalcord.org/" target="_blank">United Spinal Association Resource Center</a></li>
                              <li>Yes, You Can!
                                <ul>
                                  <li>Part 1 - How SCI Affects Your Body p1-31<a href="http://sh-sci.org/index.php?option=com_docman&task=doc_download&gid=78&Itemid=31" target="_blank"> Download</a></li>
                                  <li>Part 2 - How SCI Affects Your Body/ Maximizing Your Function p32-72<a href="http://sh-sci.org/index.php?option=com_docman&task=doc_download&gid=79&Itemid=31" target="_blank"> Download</a></li>
                                  <li>Part 3 - Maximizing Your Function p73-91<a href="http://sh-sci.org/index.php?option=com_docman&task=doc_download&gid=80&Itemid=31" target="_blank"> Download</a></li>
                                  <li>Part 4 - Maximizing Your Function p92-109<a href="http://sh-sci.org/index.php?option=com_docman&task=doc_download&gid=81&Itemid=31" target="_blank"> Download</a></li>
                                  <li>Part 5 - Maximizing Your Function p110-118<a href="http://sh-sci.org/index.php?option=com_docman&task=doc_download&gid=82&Itemid=31" target="_blank"> Download</a></li>
                                  <li>Part 6 - Coping and Living with SCI p119-132<a href="http://sh-sci.org/index.php?option=com_docman&task=doc_download&gid=83&Itemid=31" target="_blank"> Download</a></li>
                                  <li>Part 7 - Coping and Living with SCI p133-141<a href="http://sh-sci.org/index.php?option=com_docman&task=doc_download&gid=84&Itemid=31" target="_blank"> Download</a></li>
                                  <li>Part 8 - Coping and Living with SCI p142-148<a href="http://sh-sci.org/index.php?option=com_docman&task=doc_download&gid=85&Itemid=31" target="_blank"> Download</a></li>
                                  <li>Part 9 - Coping and Living with SCI/ Staying Healthy after SCI p149-192<a href="http://sh-sci.org/index.php?option=com_docman&task=doc_download&gid=86&Itemid=31" target="_blank"> Download</a></li>
                                </ul>
                              </li>
                              <li><a href="<?php echo home_url(); ?>/resources/new-england-sci-toolkit-nescit/">SNERSCIC’s New England Regional SCI Toolkit</a> – empowering you with info to receive quality care wherever you are treated for SCI</li>
                              <li>MSKTC Fact Sheets
                                <ul>
                                  <li><a href="http://www.msktc.org/lib/docs/Factsheets/SCI_Understand_Spin_Crd_Inj_Prt1.pdf" target="_blank">Understanding Spinal Cord Injury: Part 1— The Body Before and After Injury</a></li>
                                  <li><a href="http://www.msktc.org/lib/docs/Factsheets/SCI_Understand_Spin_Crd_Inj_Prt2.pdf" target="_blank">Understanding Spinal Cord Injury, Part 2— Recovery and Rehabilitation</a></li>
                                </ul>
                              </li>
                              <li><a href="http://sci-u.ca/take-a-course/" target="_blank">SCI-U</a></li>
                              <li><a href="https://facingdisability.com/" target="_blank">Facing Disability</a></li>
                              <li><a href="https://ablethrive.com/" target="_blank">AbleThrive</a></li>
                            </ul>
                          </li>
                        </ul>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!--End Tab 5-->

          </div>
        </div>
      </div>
    </div>
  </div>
  </section>
</div><!--End of Tab Container-->
</div><!--End of Tabs-->
<!-- Mid Container Ends Here -->
